<?php

return [
    'api_keys' => [
        'public_key' => env('STRIPE_KEY', null),
        'secret_key' => env('STRIPE_SECRET_KEY', null)
    ]
];
