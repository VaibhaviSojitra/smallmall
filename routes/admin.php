<?php

use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\UserController;
use App\Models\Product;
use App\Models\Order;
use App\Models\UserSubscription;
use App\Models\VendorQuestionnaire;
use App\Models\SubscribeSeller;
use App\Models\SubscriptionPlan;

Route::prefix('admin')->group(function () {

    Route::get('/login', function () {
        return view('admin.login');
    });
    Route::post('/login', [AuthController::class, 'loginUser']);

    // Admin specific role based permission on routes
    Route::middleware('auth:sanctum', 'role:admin')->group(function () {
        Route::get('/sample', function () {
            return view('admin.sample');
        });
        Route::get('/category', function () {
            return view('admin.category.list');
        });
        Route::get('/users', function () {
            return view('admin.user.list');
        });
        Route::get('/vendors', function () {
            return view('admin.vendor.list');
        });
    });


    // Seller specific role based permission on routes
    Route::middleware('auth:sanctum', 'role:seller')->group(function () {
        Route::get('/sample', function () {
            return view('admin.sample');
        });

        Route::get('/questionnaire', function () {
            return view('admin.vendor.add_questionnaire');
        });
        Route::post('/subscribe', [UserController::class, 'vendorSubscribe']);
    });


    // Admin & Seller both roles permission on routes
    Route::middleware('auth:sanctum', 'role:seller|admin')->group(function () {
        Route::get('/', function (){
            return redirect('/admin/dashboard');
        });
        Route::get('/dashboard', App\Http\Controllers\Admin\DashboardController::class);

        Route::get('/edit-profile', function(){
            $profile = auth()->user();
            return view('admin.edit_profile', compact('profile'));
        })->name('edit-profile');

        Route::get('/products', function () {

            $user = auth()->user();
            $is_seller = 0;
            $getVendorQus = [];
            $is_ques = 0;
            if( $user->hasRole('seller') )
            {
                $is_seller = 1;
                $getVendorQus = VendorQuestionnaire::where('user_id',$user->id)->first();
                if($getVendorQus){
                    $is_ques = 1;
                }else{
                    $is_ques = 0;
                }
            }

            $subscription = UserSubscription::where('user_id',Auth::user()->id)->first();
            if($subscription){
                $isSubscription = 1;
            }else{
                $isSubscription = 0;
            }
            return view('admin.product.list',compact('getVendorQus','is_ques','isSubscription','is_seller'));
        });
        Route::get('/products/create', function () {
            $user = auth()->user();
            $is_seller = 0;
            $getVendorQus = [];
            $is_ques = 0;
            if( $user->hasRole('seller') )
            {
                $is_seller = 1;
                $getVendorQus = VendorQuestionnaire::where('user_id',$user->id)->first();
                if($getVendorQus){
                    $is_ques = 1;
                }else{
                    $is_ques = 0;
                }
            }
            $subscription = UserSubscription::where('user_id',Auth::user()->id)->first();
            if($subscription){
                $isSubscription = 1;
            }else{
                $isSubscription = 0;
            }
            return view('admin.product.add',compact('getVendorQus','is_ques','isSubscription','is_seller'));
        });
        Route::get('/vendors/products/{id}', function ($id) {
            $user = auth()->user();
            $is_seller = 0;
            $getVendorQus = [];
            $is_ques = 0;
            if( $user->hasRole('seller') )
            {
                $is_seller = 1;
                $getVendorQus = VendorQuestionnaire::where('user_id',$user->id)->first();
                if($getVendorQus){
                    $is_ques = 1;
                }else{
                    $is_ques = 0;
                }
            }
            $subscription = UserSubscription::where('user_id',Auth::user()->id)->first();
            if($subscription){
                $isSubscription = 1;
            }else{
                $isSubscription = 0;
            }
            return view('admin.vendor.product', compact('id','getVendorQus','is_ques','isSubscription','is_seller'));
        });
        Route::get('/products/{product}/edit', function (Product $product) {
            $user = auth()->user();
            $is_seller = 0;
            $getVendorQus = [];
            $is_ques = 0;
            if( $user->hasRole('seller') )
            {
                $is_seller = 1;
                $getVendorQus = VendorQuestionnaire::where('user_id',$user->id)->first();
                if($getVendorQus){
                    $is_ques = 1;
                }else{
                    $is_ques = 0;
                }
            }
            $subscription = UserSubscription::where('user_id',Auth::user()->id)->first();
            if($subscription){
                $isSubscription = 1;
            }else{
                $isSubscription = 0;
            }
            return view('admin.product.edit', compact('product','getVendorQus','is_ques','isSubscription','is_seller'));
        });

        Route::get('/orders', function () {
            return view('admin.order.list');
        });
        Route::get('/orders/{order}/edit', [OrderController::class,'editOrder']);
        Route::get('/orders/{order}/details', [OrderController::class,'orderDetail']);

        Route::get('/subscription', function () {
            $subscription = UserSubscription::with('user','plan')->where('user_id',Auth::user()->id)->first();
            $plans = SubscriptionPlan::get();

            $user = auth()->user();
            $is_seller = 0;
            $getVendorQus = [];
            $is_ques = 0;
            if( $user->hasRole('seller') )
            {
                $is_seller = 1;
                $getVendorQus = VendorQuestionnaire::where('user_id',$user->id)->first();
                if($getVendorQus){
                    $is_ques = 1;
                }else{
                    $is_ques = 0;
                }
            }
            return view('admin.subscription.list', compact('getVendorQus','is_ques','subscription','plans'));
        });

        Route::get('/subscriber', function () {
            return view('admin.subscriber.list');
        });


        Route::get('/my-profile', function () {
            return view('admin.my-profile');
        });


        Route::get('/top-products', function () {
            return view('admin.top_product.list');
        });

        Route::get('/brand-subscribers', function () {
            return view('admin.brand_subscribers.list');
        });

        Route::get('/vendor/questionnaire', function () {
            return view('admin.vendor.list_questionnaire');
        });
        Route::get('/vendor/approved-questionnaire', function () {
            return view('admin.vendor.approved_list_questionnaire');
        });
        Route::get('/vendor/questionnaire/{id}', function ($id) {
            $questionnaire = VendorQuestionnaire::where('id',$id)->first();
            return view('admin.vendor.view_questionnaire',compact('questionnaire'));
        });

    });

});
