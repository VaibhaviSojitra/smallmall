<?php

use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\ProductController as AdminProductController;
use App\Http\Controllers\Admin\ProfileController as AdminProfileController;
use App\Http\Controllers\Admin\OrderController as AdminOrderController;
use App\Http\Controllers\Admin\ProductMediaController;
use App\Http\Controllers\Admin\ProductReviewController;
use App\Http\Controllers\Admin\UserController as AdminUserController;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\HomeController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\CartController;
use App\Http\Controllers\Api\WishlistController;
use App\Http\Controllers\Api\OrderController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::name('api.')->group(function(){
    Auth::routes(['register'=>false,'login'=>false]);
});
Route::post('/auth/register', [AuthController::class, 'createUser']);
Route::post('/auth/login', [AuthController::class, 'loginUser']);
Route::post('/forgot/password', [AuthController::class, 'forgotPassword']);
Route::post('/reset/password', [AuthController::class, 'resetPassword']);

Route::middleware('auth:sanctum')->group(function () {

    Route::prefix('/user')->group(function () {
        Route::get('/view-invoice/{id}', [OrderController::class, 'viewInvoice']);
        Route::get('/', [UserController::class, 'index']);
        Route::post('/update', [UserController::class, 'update']);
        Route::post('/update/profile', [UserController::class, 'updateProfile']);
        Route::post('/update/mobile', [UserController::class, 'updateUserMobile']);
        Route::post('/verify-otp', [UserController::class, 'verifyOTP']);
        Route::post('/change/password', [UserController::class, 'changePassword']);
        Route::prefix('/address')->group(function () {
            Route::get('/{id?}', [UserController::class, 'getUserAddress']);
            Route::post('/add', [UserController::class, 'addUserAddress']);
            Route::post('/update', [UserController::class, 'updateUserAddress']);
            Route::post('/delete', [UserController::class, 'deleteUserAddress']);
        });
        Route::post('/device/token', [UserController::class, 'deviceToken']);
        Route::get('/notification', [UserController::class, 'getNotification']);
    });

    Route::prefix('order')->group(function () {
        Route::get('/', [OrderController::class, 'index']);
        Route::get('/{order}', [OrderController::class, 'show']);
        Route::post('/place', [OrderController::class, 'store']);
        Route::post('/{order}/payment', [OrderController::class, 'payment']);
        Route::get('/{order}/cancel', [OrderController::class, 'cancel']);
        Route::get('invoice/{order_item}', [OrderController::class, 'invoice']);
    });

    Route::prefix('/products')->group(function () {
        Route::post('/{product}/reviews/add', [ProductController::class, 'reviewsAdd']);
        Route::post('/{product}/reviews/{review}/update', [ProductController::class, 'reviewsUpdate']);
        Route::get('/{product}/reviews/{review}/delete', [ProductController::class, 'reviewsDelete']);
    });

});
Route::middleware('optionalauth')->group(function () {

    Route::prefix('home')->group(function () {
        Route::post('/', [HomeController::class, 'index']);
    });

    Route::prefix('cart')->group(function () {
        Route::post('/', [CartController::class, 'index']);
        Route::post('/add', [CartController::class, 'store']);
        Route::post('/update', [CartController::class, 'update']);
        Route::post('/delete', [CartController::class, 'delete']);
    });

    Route::prefix('wishlist')->group(function () {
        Route::post('/', [WishlistController::class, 'index']);
        Route::post('/add', [WishlistController::class, 'store']);
        Route::post('/delete', [WishlistController::class, 'delete']);
    });

    Route::prefix('/products')->group(function () {
        Route::post('/', [ProductController::class, 'index']);
        Route::post('/{product}', [ProductController::class, 'show']);
        Route::get('/{product}/reviews', [ProductController::class, 'reviews']);
        Route::get('/{product}/review-summary', [ProductController::class, 'reviewSummary']);
        Route::post('/search/list', [ProductController::class, 'search']);
        Route::get('/{product}/category/{category}/related-products', [ProductController::class, 'relatedProductsByCategory']);
    });

    Route::prefix('category')->group(function () {
        Route::get('/', [ProductController::class, 'categories']);
        Route::get('/{slug}/slug', [ProductController::class, 'categoryBySlug']);
        Route::post('/{category}/products', [ProductController::class, 'productsByCategory']);
    });

    Route::prefix('seller')->group(function () {
        Route::get('/', [ProductController::class, 'sellers']);
        Route::post('/{seller}/products', [ProductController::class, 'productsBySeller']);
        Route::get('/{seller}/toggle-subscription', [ProductController::class, 'toggleBrandSubscription']);
        Route::get('/{seller}/subscriber', [ProductController::class, 'sellerSubscriber']);
    });

});

Route::get('/session_id', [CartController::class, 'getSessionId']);


Route::prefix('admin')->middleware('auth:sanctum')->group(function () {
    Route::apiResource('categories', CategoryController::class);
    Route::apiResource('products', AdminProductController::class);


    Route::apiResource('orders', AdminOrderController::class);
    Route::post('orders/change_status', [AdminOrderController::class, 'changeOrderStatus']);

    Route::apiResource('products/{product}/medias', ProductMediaController::class)->except("update")->scoped();
    Route::put('products/{product}/delete', [AdminProductController::class, 'destroy']);
    Route::put('products/{product}/approve', [AdminProductController::class, 'approve']);
    Route::put('products/{product}/reject', [AdminProductController::class, 'reject']);
    Route::put('products/{product}/popular', [AdminProductController::class, 'changePopular']);
    Route::put('products/{product}/featured', [AdminProductController::class, 'changeFeatured']);
    Route::get('top-products', [AdminProductController::class, 'topProducts']);
    Route::apiResource('users', AdminUserController::class)->except("store", "destroy", "update");
    Route::post('users/{user}/block', [AdminUserController::class, 'block']);
    Route::post('users/{user}/unblock', [AdminUserController::class, 'unblock']);
    Route::get('subscribed-users', [AdminUserController::class, 'subscribedUsers']);

    Route::get('vendors', [AdminProductController::class, 'vendorProducts']);
    Route::get('vendor/questionnaire', [AdminUserController::class, 'vendorQuestionnaire']);
    Route::get('vendor/all/questionnaire', [AdminUserController::class, 'vendorAllQuestionnaire']);
    Route::get('vendor/approved/questionnaire', [AdminUserController::class, 'approvedVendorQuestionnaire']);
    Route::post('vendor/add-questionnaire', [AdminUserController::class, 'vendorAddQuestionnaire']);
    Route::post('vendor/questionnaire/change_status', [AdminUserController::class, 'changeVendorQuestionnaireStatus']);

    Route::get('vendors/subscriber', [AdminUserController::class, 'vendorSubscriber']);

    Route::apiResource('products/{product}/reviews', ProductReviewController::class)->only("index", "destroy")->scoped();
    Route::put('profile', [AdminProfileController::class, 'update']);
});

