<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\SellerController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::name('web.')->group(function(){
    Auth::routes();
});
Route::get('user/verify/{token}', [UserController::class, 'verifyEmail'])->name('user/verify');

include __DIR__ . "/admin.php";


Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/about', [HomeController::class, 'about'])->name('about');
Route::get('/contact', [HomeController::class, 'contact'])->name('contact');
Route::get('/privacy-policy', [HomeController::class, 'privacypolicy'])->name('privacy-policy');
Route::get('/terms', [HomeController::class, 'terms'])->name('terms');
Route::get('/vendors', [SellerController::class, 'vendorsList'])->name('vendors');

Route::prefix('/product')->group(function () {
    Route::get('/{product}', [ProductController::class, 'index']);
    Route::get('/category/{slug}', [ProductController::class, 'productByCategory']);
    Route::get('/vendor/{id}', [ProductController::class, 'productBySeller']);
});
Route::get('/shop', [ProductController::class, 'shop'])->name('shop');

Route::post('addUserAddress', [UserController::class, 'addUserAddress'])->middleware('auth:sanctum');
Route::get('/my-account', [UserController::class, 'myaccount'])->name('my-account')->middleware('auth:sanctum');
Route::get('forgot-password', [UserController::class, 'showForgotPassword'])->name('forgot-password');
Route::get('send-forgot-otp', [UserController::class, 'sendForgotOtp'])->name('send-forgot-otp');
Route::get('verify-forgot-otp', [UserController::class, 'verifyForgotOtp'])->name('verify-forgot-otp');
Route::post('change-forgot-password', [UserController::class, 'changeForgotPassword'])->name('change-forgot-password');
Route::get('logout', [UserController::class, 'logout'])->name('logout');

Route::get('/cart', [OrderController::class, 'cart'])->name('cart');
Route::get('/wishlist', [OrderController::class, 'wishlist'])->name('wishlist');
Route::get('/checkout', [OrderController::class, 'checkout'])->name('checkout')->middleware('auth:sanctum');

Route::get('/secret', [OrderController::class, 'getClientSecret'])->name('secret')->middleware('auth:sanctum');
Route::get('get-cards', [OrderController::class, 'getCards']);
Route::post('placeOrder', [OrderController::class, 'placeOrder'])->middleware('auth:sanctum');
Route::post('payment', [OrderController::class, 'payment'])->name('payment');
Route::get('/order/{id}/details', [OrderController::class, 'viewOrderDetails'])->name('viewOrderDetails')->middleware('auth:sanctum');
Route::get('/view-invoice/{id}', [OrderController::class, 'viewInvoice'])->name('view-invoice');

Route::name('seller.')->prefix('/seller')->group(function () {
    Route::get('/register', [SellerController::class, 'register'])->name('register');
    Route::post('/doregister', [SellerController::class, 'doregister'])->name('doregister');
});

Route::get('/show-invoice/{id}', [OrderController::class, 'viewSignedInvoice'])->name('show-invoice');
