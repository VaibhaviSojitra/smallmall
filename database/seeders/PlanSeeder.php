<?php

namespace Database\Seeders;

use App\Models\SubscriptionPlan;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class PlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $stripe = new \Stripe\StripeClient(
            config('stripe.api_keys.secret_key')
        );
        $createPlan = $stripe->plans->create([
            'amount' => 100,
            'currency' => 'aud',
            'interval' => 'month',
            'product' => [
                'name' => 'Small Mall'
            ],
        ]);

        SubscriptionPlan::updateOrCreate([
            'plan_id' => $createPlan->id
        ], [
            'plan_id' => $createPlan->id,
            'title' => 'Small Mall',
            'price' => $createPlan->amount
        ]);
        
    }
}
