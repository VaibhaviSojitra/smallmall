<?php

namespace Database\Seeders;

use App\Models\Address;
use App\Models\Media;
use App\Models\Product;
use App\Models\ProductReview;
use App\Models\ProductSeller;
use App\Models\ProductVariant;
use App\Models\User;
use Database\Factories\ProductReviewFactory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class FactoryDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Dummy users
        
        User::factory(1)
            ->create([
                'email' => 'fiveonedigital@gmail.com'
            ])
            ->each(function ($user) {
                $user->assignRole('admin');
            });

        User::factory(10)
            ->has(Address::factory()->count(1))
            ->create()
            ->each(function ($user) {
                $user->assignRole('customer');
            });
        
        User::factory(5)
            ->create()
            ->each(function ($user) {
                $user->assignRole('seller');
            });



        Product::factory(20)
            ->has(ProductReview::factory()->count(3), 'reviews')
            ->has(Media::factory()->count(2), 'medias')
            ->count(50)
            ->create();
        
    }
}
