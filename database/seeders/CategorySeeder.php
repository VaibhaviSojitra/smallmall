<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            "Beauty and personal care",
            "Health",
            "Household",
            "Kitchen",
            "Pantry and food",
            "Pet supplies",
            "Footwear",
            "Sports, Fitness and Outdoors",
            "Stationery and Office Products",
            "Toys and Games",
            "Fashion",
            "Baby and Kids"
        ];

        foreach ($data as $categoryText){
            Category::updateOrCreate([
                'name' => $categoryText
            ], [
                'name' => $categoryText,
                'slug' => Str::slug($categoryText)
            ]);
        }
        
    }
}
