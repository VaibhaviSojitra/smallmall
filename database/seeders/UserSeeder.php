<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'email' => 'admin@gmail.com',
            'name' => 'Michael Rudman',
            'password' => bcrypt('Admin@123'),
            'email_verified_at' => now()
        ]);

        $user = User::where('email','admin@gmail.com')->first();
        $user->assignRole('admin');
    }
}
