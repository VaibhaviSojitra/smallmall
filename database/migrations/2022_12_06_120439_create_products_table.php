<?php

use App\Models\Category;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('subtitle');
            $table->string('slug');
            $table->float('selling_price');
            $table->float('display_price');
            $table->foreignIdFor(Category::class)->constrained();
            $table->text('description');
            $table->string('short_description')->nullable();
            $table->json('specification')->comment('key-value pair');
            $table->boolean('is_featured')->default(false);
            $table->boolean('is_popular')->default(false);
            $table->foreignIdFor(User::class, 'seller_id')->constrained('users');
            $table->boolean('is_approved')->default('1');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
};
