<?php

use App\Models\Product;
use App\Models\ProductSeller;
use App\Models\ProductVariant;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->id();
            $table->text('order_id');
            $table->foreignIdFor(User::class)->nullable();
            $table->foreignIdFor(Product::class)->constrained();
            $table->unsignedInteger('quantity');
            $table->DateTime('deliver_at')->nullable();
            $table->DateTime('cancelled_at')->nullable();
            $table->DateTime('assigned_at')->nullable();
            $table->DateTime('completed_at')->nullable();
            $table->DateTime('confirmed_at')->nullable();
            $table->DateTime('shipped_date')->nullable();
            $table->tinyInteger('status')->default('0')->comment('0 for ordered/ 1 for confirmed/ 2 for processing/ 3 for shipped/ 4 for on hold/ 5 for completed/ 6 for cancelled');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
    }
};
