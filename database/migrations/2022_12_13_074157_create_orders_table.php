<?php

use App\Models\Address;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->text('order_id');
            $table->foreignIdFor(User::class)->constrained();
            $table->foreignIdFor(Address::class, 'billing_address_id')->constrained('addresses');
            $table->foreignIdFor(Address::class, 'shipping_address_id')->constrained('addresses');
            $table->float('total_amount');
            $table->string('coupon_discount')->nullable();
            $table->string('shipping_charge')->nullable();
            $table->tinyInteger('payment_type')->default('0')->comment('1 for cod, 2 for card, 3 for paypal');
            $table->string('transaction_id')->nullable();
            $table->DateTime('ordered_at');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
};
