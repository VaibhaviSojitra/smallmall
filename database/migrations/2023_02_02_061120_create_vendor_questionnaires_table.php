<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\User;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_questionnaires', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(User::class)->constrained();
            $table->string('business_location')->nullable();
            $table->string('business_type')->nullable();
            $table->string('business_name')->nullable();
            $table->string('comapany_register_number')->nullable();
            $table->string('business_add_1')->nullable();
            $table->string('business_add_2')->nullable();
            $table->string('business_city')->nullable();
            $table->string('business_state')->nullable();
            $table->string('business_zip')->nullable();
            $table->string('contact_number')->nullable();
            $table->string('firstname')->nullable();
            $table->string('middlename')->nullable();
            $table->string('lastname')->nullable();
            $table->string('country_citizenship')->nullable();
            $table->string('dob')->nullable();
            $table->string('add_1')->nullable();
            $table->string('add_2')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('zip')->nullable();
            $table->tinyInteger('is_primary_contact')->default(0)->comment('0-none, 1-Is a beneficial owner of the business, 2-Is a legal representative of the business');
            $table->string('bank_acc_holder_name')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('bank_acc_number')->nullable();
            $table->string('bank_ifsc')->nullable();
            $table->string('brand_name')->nullable();
            $table->string('sell_products_type')->nullable();
            $table->string('product_produce')->nullable();
            $table->string('product_sustainably')->nullable();
            $table->string('raw_materials_sourced')->nullable();
            $table->string('trademark')->nullable();
            $table->string('identity_card')->nullable();
            $table->string('address_proof')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_questionnaires');
    }
};
