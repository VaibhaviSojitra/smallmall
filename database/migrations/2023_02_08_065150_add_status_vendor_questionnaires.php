<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendor_questionnaires', function (Blueprint $table) {
            $table->tinyInteger('status')->after('address_proof')->default(0)->comment('0-none, 1-Approved , 2 Decline');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendor_questionnaires', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
};
