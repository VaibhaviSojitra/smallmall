<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('used_material')->after('specification')->nullable();
            $table->string('manufactured_in')->after('used_material')->nullable();
            $table->string('manufactured_by')->after('manufactured_in')->nullable();
            $table->string('sustainability_statement')->after('manufactured_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('used_material');
            $table->dropColumn('manufactured_in');
            $table->dropColumn('manufactured_by');
            $table->dropColumn('sustainability_statement');
        });
    }
};
