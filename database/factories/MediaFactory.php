<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use Bilions\FakerImages\FakerImageProvider;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Media>
 */
class MediaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {   
        $this->faker->addProvider(new FakerImageProvider($this->faker));
        $image = basename($this->faker->image(storage_path('app/public/products/fake')));
        return [
            'type' => 0,
            'path' => "fake/".$image,
            'ownerable_type' => 'App\Models\Product',
            'ownerable_id' => Product::factory(),
            'created_at' => now(),
            'updated_at' => now()
        ];
    }
}
