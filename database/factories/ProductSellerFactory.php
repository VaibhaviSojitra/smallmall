<?php

namespace Database\Factories;

use App\Models\ProductVariant;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ProductSeller>
 */
class ProductSellerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $productVariant = ProductVariant::inRandomOrder()->first();
        return [
            'product_id' => $productVariant->product_id,
            'product_variant_id' => $productVariant->id,
            'seller_id' => User::role('seller')->inRandomOrder()->first()->id,
        ];
    }
}
