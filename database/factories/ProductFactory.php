<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $title = fake()->company();
        return [
            'title' => $title,
            'subtitle' => fake()->sentence(),
            'slug' => Str::slug($title),
            'category_id' => Category::inRandomOrder()->first()->id,
            'description' => fake()->paragraph(),
            'display_price' => fake()->randomNumber(3),
            'selling_price' => fake()->randomNumber(3),
            'seller_id' => User::role('seller')->inRandomOrder()->first()->id,
            'is_featured' => (bool)random_int(0, 1),
            'is_popular' => (bool)random_int(0, 1),
            'specification' => json_encode([
                [
                    "key" => "Color",
                    "value" => fake()->colorName(),
                ],
                [
                    "key" => "Width",
                    "value" => fake()->randomNumber(3). ' mm',
                ],
                [
                    "key" => "Height",
                    "value" => fake()->randomNumber(3). ' mm',
                ]
            ])
        ];
    }
}
