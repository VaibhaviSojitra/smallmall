<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Address>
 */
class AddressFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $address = explode(",", fake()->address(), 2);
        return [
            'user_id' => User::factory(),
            'full_name' => fake()->name(),
            'add_1' => $address[0],
            'add_2' => $address[1],
            'created_at' => now(),
            'updated_at' => now()
        ];
    }
}
