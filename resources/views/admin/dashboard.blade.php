@extends('layouts.admin')
@section('page-content')
<section class="content-main">


    <div class="content-header">
        <div>
            <h2 class="content-title card-title">Dashboard</h2>
            <p>Whole data about your business here</p>
        </div>
    </div>
    <div class="row">

        <div class="col-lg-4">
            <div class="card card-body mb-4">
                <article class="icontext">
                    <span class="icon icon-sm rounded-circle bg-success-light"><i class="text-success material-icons md-local_shipping"></i></span>
                    <div class="text">
                        <h6 class="mb-1 card-title">Orders</h6>
                        <span>{{ number_format($totalOrders->total()) }}</span>
                        <span class="text-sm"> Including orders in transit </span>
                    </div>
                </article>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card card-body mb-4">
                <article class="icontext">
                    <span class="icon icon-sm rounded-circle bg-warning-light"><i class="text-warning material-icons md-qr_code"></i></span>
                    <div class="text">
                        <h6 class="mb-1 card-title">Products</h6>
                        <span>{{ number_format($totalProducts) }}</span>
                        <span class="text-sm"> In {{ $categories }} Categories </span>
                    </div>
                </article>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card card-body mb-4">
                <article class="icontext">
                    <span class="icon icon-sm rounded-circle bg-info-light"><i class="text-info material-icons md-shopping_basket"></i></span>
                    <div class="text">
                        <h6 class="mb-1 card-title">Monthly Earning</h6>
                        <span>${{ number_format($totalOrders->where('created_at', '>=', \Carbon\Carbon::now()->subMonth())->sum('total_amount')) }}</span>
                        <span class="text-sm"> Based in your local time. </span>
                    </div>
                </article>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-6 col-lg-6">
            <div class="card mb-4">
                <article class="card-body">
                    <h5 class="card-title">Daily sale statistics</h5>
                    <canvas id="dailySalesChart" height="100"></canvas>
                </article>
            </div>
        </div>
        <div class="col-xl-6 col-lg-6">
            <div class="card mb-4">
                <article class="card-body">
                    <h5 class="card-title">Daily product statistics</h5>
                    <canvas id="dailyProductsChart" height="100"></canvas>
                </article>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-6 col-lg-6">
            <div class="card mb-4">
                <article class="card-body">
                    <h5 class="card-title">Weekly sale statistics</h5>
                    <canvas id="weeklySalesChart" height="100"></canvas>
                </article>
            </div>
        </div>
        <div class="col-xl-6 col-lg-6">
            <div class="card mb-4">
                <article class="card-body">
                    <h5 class="card-title">Weekly product statistics</h5>
                    <canvas id="weeklyProductsChart" height="100"></canvas>
                </article>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-6 col-lg-6">
            <div class="card mb-4">
                <article class="card-body">
                    <h5 class="card-title">Monthly sale statistics</h5>
                    <canvas id="monthlySalesChart" height="100"></canvas>
                </article>
            </div>
        </div>
        <div class="col-xl-6 col-lg-6">
            <div class="card mb-4">
                <article class="card-body">
                    <h5 class="card-title">Monthly Product statistics</h5>
                    <canvas id="monthlyProductsChart" height="100"></canvas>
                </article>
            </div>
        </div>
    </div>
    <div class="card mb-4">
        <header class="card-header">
            <h4 class="card-title">Latest orders</h4>
        </header>
        <div class="card-body">
            <div class="table-responsive">
                <div class="table-responsive">
                    <table class="table align-middle table-nowrap mb-0">
                        <thead class="table-light">
                            <tr>
                                <th class="align-middle" scope="col">Order ID</th>
                                <th class="align-middle" scope="col">Billing Name</th>
                                <th class="align-middle" scope="col">Date</th>
                                <th class="align-middle" scope="col">Total</th>
                                <th class="align-middle" scope="col">Payment Type</th>
                                <th class="align-middle" scope="col">Payment Id</th>
                                <th class="align-middle" scope="col">View Details</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($totalOrders as $order)
                                <tr>
                                    <td><a href="/admin/orders/{{$order->id}}/details" class="fw-bold">{{ $order->order_id }}</a></td>
                                    <td>{{ $order->user->name }}</td>
                                    <td>{{ $order->created_at->format('d M, Y') }}</td>
                                    <td>${{ $order->total_amount }}</td>
                                    <td>
                                        {{ $order->payment_type }}
                                    </td>
                                    <td>
                                        <span class="badge badge-pill badge-soft-success">{{ $order->transaction_id }}</span>
                                    </td>
                                    <td>
                                        <a href="/admin/orders/{{$order->id}}/details" class="btn btn-xs"> View details</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- table-responsive end// -->
        </div>
    </div>
    <div class="pagination-area mt-30 mb-50">
        {{ $totalOrders->links('pagination::bootstrap-4') }}
    </div>

</section>

@endsection

@push('scripts')

<script>

$(document).ready(function () {

    // DailySalesChart
    if ($('#dailySalesChart').length) {
        var ctx = document.getElementById('dailySalesChart').getContext('2d');
        var chart = new Chart(ctx, {
            type: 'line',

            data: {
                labels: {!! $dailySalesGraph->pluck('dayname') !!},
                datasets: [{
                        label: 'Sales',
                        tension: 0.4,
                        fill: true,
                        backgroundColor: 'rgba(44, 120, 220, 0.2)',
                        borderColor: 'rgba(44, 120, 220)',
                        data: {{ $dailySalesGraph->pluck('total_amount') }}
                    }
                ]
            },
            options: {
                plugins: {
                legend: {
                    labels: {
                    usePointStyle: true,
                    },
                }
                }
            }
        });
    } //End if
    // dailyProductChart
    if ($('#dailyProductsChart').length) {
        var ctx = document.getElementById('dailyProductsChart').getContext('2d');
        var chart = new Chart(ctx, {
            type: 'line',

            data: {
                labels: {!! $dailyProductsGraph->pluck('dayname') !!},
                datasets: [
                    {
                        label: 'Products',
                        tension: 0.3,
                        fill: true,
                        backgroundColor: 'rgba(380, 200, 230, 0.2)',
                        borderColor: 'rgb(380, 200, 230)',
                        data: {{ $dailyProductsGraph->pluck('total') }}
                    }
                ]
            },
            options: {
                plugins: {
                legend: {
                    labels: {
                    usePointStyle: true,
                    },
                }
                }
            }
        });
    } //End if


    // WeeklySalesChart
    if ($('#weeklySalesChart').length) {
        var ctx = document.getElementById('weeklySalesChart').getContext('2d');
        var chart = new Chart(ctx, {
            type: 'line',

            data: {
                labels: {!! $weeklySalesGraph->pluck('weekname') !!},
                datasets: [{
                        label: 'Sales',
                        tension: 0.4,
                        fill: true,
                        backgroundColor: 'rgba(44, 120, 220, 0.2)',
                        borderColor: 'rgba(44, 120, 220)',
                        data: {{ $weeklySalesGraph->pluck('total_amount') }}
                    }
                ]
            },
            options: {
                plugins: {
                legend: {
                    labels: {
                    usePointStyle: true,
                    },
                }
                }
            }
        });
    } //End if
    // dailyProductChart
    if ($('#weeklyProductsChart').length) {
        var ctx = document.getElementById('weeklyProductsChart').getContext('2d');
        var chart = new Chart(ctx, {
            type: 'line',

            data: {
                labels: {!! $weeklyProductsGraph->pluck('weekname') !!},
                datasets: [
                    {
                        label: 'Products',
                        tension: 0.3,
                        fill: true,
                        backgroundColor: 'rgba(380, 200, 230, 0.2)',
                        borderColor: 'rgb(380, 200, 230)',
                        data: {{ $dailyProductsGraph->pluck('total') }}
                    }
                ]
            },
            options: {
                plugins: {
                legend: {
                    labels: {
                    usePointStyle: true,
                    },
                }
                }
            }
        });
    } //End if


    // MonthlySalesChart
    if ($('#monthlySalesChart').length) {
        var ctx = document.getElementById('monthlySalesChart').getContext('2d');
        var chart = new Chart(ctx, {
            type: 'line',

            data: {
                labels: {!! $monthlySalesGraph->pluck('monthname') !!},
                datasets: [{
                        label: 'Sales',
                        tension: 0.4,
                        fill: true,
                        backgroundColor: 'rgba(44, 120, 220, 0.2)',
                        borderColor: 'rgba(44, 120, 220)',
                        data: {{ $monthlySalesGraph->pluck('total_amount') }}
                    }
                ]
            },
            options: {
                plugins: {
                legend: {
                    labels: {
                    usePointStyle: true,
                    },
                }
                }
            }
        });
    } //End if
    // dailyProductChart
    if ($('#monthlyProductsChart').length) {
        var ctx = document.getElementById('monthlyProductsChart').getContext('2d');
        var chart = new Chart(ctx, {
            type: 'line',

            data: {
                labels: {!! $monthlyProductsGraph->pluck('monthname') !!},
                datasets: [
                    {
                        label: 'Products',
                        tension: 0.3,
                        fill: true,
                        backgroundColor: 'rgba(380, 200, 230, 0.2)',
                        borderColor: 'rgb(380, 200, 230)',
                        data: {{ $monthlyProductsGraph->pluck('total') }}
                    }
                ]
            },
            options: {
                plugins: {
                legend: {
                    labels: {
                    usePointStyle: true,
                    },
                }
                }
            }
        });
    } //End if
});

</script>

@endpush
