@extends('layouts.admin')
@section('page-content')

    <div class="content-header">
        <h2 class="content-title">My Profile</h2>

    </div>

    <edit-profile :profile="{{ $profile }}"/>

@endsection
