@extends('layouts.admin')
@section('page-content')
<section class="content-main">
    @if($getVendorQus && $getVendorQus->status == 2)
        <h4> Your account is not approved By Admin, Please update questionnaire.</h4>
        <a href="/admin/vendor/questionnaire" class="btn btn-primary">Update Questionnaire</a>
    @elseif($getVendorQus && $getVendorQus->status == 0)
        <h4> Your account is still not approved, If you want to update questionnaire change from here.</h4>
        <a href="/admin/vendor/questionnaire" class="btn btn-primary">Update Questionnaire</a>
    @elseif($getVendorQus && $getVendorQus->status == 3)
        <h4> To complete your approval please add questionnaire</h4>
        <a href="/admin/vendor/questionnaire" class="btn btn-primary">Add Questionnaire</a>
    @elseif($subscription)
        <subscription-list :subscription="{{ $subscription }}"></subscription-list>
    @else
        <div class="card mb-4">
            <div class="card-body">
            <h4>You dont have any subscription. Please Subscribe from here</h4>

                <div class="mt-3 col-md-6">
                    <form method="POST" id="payment-form" data-cc-on-file="false" data-stripe-publishable-key="{{ config('stripe.api_keys.public_key') }}" class="row" action="{{ url('/admin/subscribe') }}">
                    @csrf    
                        <div class="form-group">
                            <label for="category_name" class="form-label">Select Plan</label>
                            <select name="plan_id" class="form-select form-control" id="plan_id" required>
                                @foreach($plans as $value)
                                <option value="{{$value->id}}">{{$value->title}} - {{config('services.stripe.currency')}} {{$value->price/100}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div id="card-element"></div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="row mt-2">
                            <div class="col-md-12">
                                <span class="payment-errors" id="card-errors" style="color: red;margin-top:30px;"></span>
                            </div>
                        </div>
                        <input type="hidden" class="stripeToken" name="stripeToken">
                        <div class="mt-3 d-grid gap-2 d-md-block">
                            <button type="submit" class="btn btn-primary">
                                <span>Subscribe</span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endif
    <!-- card .// -->
</section>
@endsection
@push('scripts')

<script>
        window.ParsleyConfig = {
            errorsWrapper: '<div></div>',
            errorTemplate: '<div class="alert alert-danger parsley" role="alert"></div>',
            errorClass: 'has-error',
            successClass: 'has-success'
        };
    </script>
    
    <script src="http://parsleyjs.org/dist/parsley.js"></script>
    
    <script src="https://js.stripe.com/v3/"></script>
    <script>
        $(document).ready(function(){

        var style = {
            base: {
                color: '#32325d',
                lineHeight: '18px',
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSmoothing: 'antialiased',
                fontSize: '16px',
                '::placeholder': {
                    color: '#aab7c4'
                }
            },
            invalid: {
                color: '#fa755a',
                iconColor: '#fa755a'
            }
        };

        var stripe = Stripe($("#payment-form").attr('data-stripe-publishable-key'));

        const elements = stripe.elements(); // Create an instance of Elements.
        const card = elements.create('card', { hidePostalCode: true, style: style }); // Create an instance of the card Element.

        card.mount('#card-element'); // Add an instance of the card Element into the `card-element` <div>.

        card.on('change', function(event) {
            var displayError = document.getElementById('card-errors');
            if (event.error) {
                displayError.textContent = event.error.message;
            } else {
                displayError.textContent = '';
            }
        });

        var form = document.getElementById('payment-form');
        $(document).on('submit', '#payment-form', function(e) {
                e.preventDefault();
                console.log(card);
                stripe.createToken(card).then(function(result) {
                    if (result.error) {
                    // Inform the user if there was an error.
                    var errorElement = document.getElementById('card-errors');
                    errorElement.textContent = result.error.message;
                    } else {
                    // Send the token to your server.
                        stripeTokenHandler(result.token);
                    }
                });
            });

            // Submit the form with the token ID.
            function stripeTokenHandler(token) {
                // Insert the token ID into the form so it gets submitted to the server
                // var form = document.getElementById('payment-form');
                // var hiddenInput = document.createElement('input');
                // hiddenInput.setAttribute('type', 'text');
                // hiddenInput.setAttribute('name', 'stripeToken');
                // hiddenInput.setAttribute('value', token.id);
                // form.appendChild(hiddenInput);
                $(".stripeToken").val(token.id);
                // Submit the form
                form.submit();
            }
        });

    </script>

@endpush
