@extends('layouts.admin')
@section('page-content')
    <order-detail :order="{{ $order }}"/>
@endsection
@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.5.0-alpha1/html2canvas.js">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.min.js"></script>

    <script src="/web-assets/js/invoice/invoice.js"></script>

@endpush