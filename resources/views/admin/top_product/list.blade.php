@extends('layouts.admin')
@section('page-content')
<section class="content-main">

    <top-product-widget :is_admin="@if(auth()->user()->hasRole('admin')){{1}}@else{{0}}@endif"></top-product-widget>
    <!-- card .// -->
</section>
@endsection
