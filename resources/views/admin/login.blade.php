@extends('layouts.admin')
@section('page-content')
<section class="content-main mt-80 mb-80">
    <div class="card mx-auto card-login">
        <div class="card-body">
            <div class="text-center px-5">
                <img src="/web-assets/imgs/theme/logo.webp" class="logo" alt="SmallMall Dashboard" />
            </div>
            <h4 class="card-title mb-4">Sign in</h4>
            @if ($errors->has('error'))
                    <p class="text-danger">{{ $errors->first('error') }}</p>
                    @endif
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                    @endif
            <form action="" method="POST">
                @csrf
                <div class="mb-3">
                    <input class="form-control" placeholder="Username or email" type="text" name="email" value="{{request()->old('email')}}"/>
              
                </div>
                <!-- form-group// -->
                <div class="mb-3">
                    <!-- <input class="form-control" placeholder="Password" type="password" name="password" />
                    <span class="input-group-text cursor-pointer"><i class="icon material-icons md-remove_red_eye"></i></span> -->
                    <div class="input-group input-group-merge form-password-toggle">
                    <input
                        type="password"
                        class="form-control form-control-merge"
                        id="login-password"
                        name="password"
                        tabindex="2"
                        placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                        aria-describedby="login-password"
                    />
                    <span class="input-group-text cursor-pointer"><i class="icon material-icons md-remove_red_eye"></i></span>
                    </div>

                </div>
                <!-- form-group// -->
                <div class="mb-3">
                    <a href="#" class="float-end font-sm text-muted">Forgot password?</a>
                    <label class="form-check">
                        <input type="checkbox" class="form-check-input" checked="" name="remember" />
                        <span class="form-check-label">Remember</span>
                    </label>
                </div>
                <!-- form-group form-check .// -->
                <div class="mb-4">
                    <button type="submit" class="btn btn-primary w-100">Login</button>
                </div>
                <!-- form-group// -->
            </form>
        </div>
    </div>
</section>
@endsection

@push('scripts')
<script>
        $(document).ready(function(){

            $('.form-password-toggle .input-group-text').on('click', function (e) {
                e.preventDefault();
                var $this = $(this),
                    inputGroupText = $this.closest('.form-password-toggle'),
                    formPasswordToggleIcon = $this,
                    formPasswordToggleInput = inputGroupText.find('input');

                if (formPasswordToggleInput.attr('type') === 'text') {
                    formPasswordToggleInput.attr('type', 'password');
                    formPasswordToggleIcon.find('i').removeClass('md-remove_red_eye');
                    formPasswordToggleIcon.find('i').addClass('md-remove_red_eye');
                } else if (formPasswordToggleInput.attr('type') === 'password') {
                    formPasswordToggleInput.attr('type', 'text');
                    formPasswordToggleIcon.find('i').addClass('md-remove');
                    formPasswordToggleIcon.find('i').removeClass('md-remove_red_eye');
                }
            });
        });

</script>

@endpush
