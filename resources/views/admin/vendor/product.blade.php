@extends('layouts.admin')
@section('page-content')
<section class="content-main">

    <vendor-product-list :user-id="'{{ $id }}'"></vendor-product-list>
    <!-- card .// -->
</section>
@endsection
