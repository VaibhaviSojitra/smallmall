@extends('layouts.admin')
@section('page-content')
 
<section class="content-main">
        <div class="content-header">
            <h2 class="content-title"> Questionnaire</h2>
        </div>
        <div class="card mb-4">
            <div class="card-body">
                <div class="mt-2">
                    <div class="row">
                        <div class="mb-4 col-md-4">
                            <label for="business_name" class="form-label">Business Name</label>
                            <input value="{{$questionnaire->business_name}}" type="text" class="form-control" id="business_name" readonly/>
                        </div>
                        <div class="mb-4 col-md-4">
                            <label for="business_location" class="form-label">Business Location</label>
                            <input value="{{$questionnaire->business_location}}" type="text" class="form-control" id="business_location" readonly/>
                        </div>
                        <div class="mb-4 col-md-4">
                            <label for="business_type" class="form-label">Business Type</label>
                            <select class="form-select" id="business_type" readonly>
                                <option value="Sole Trader" @if($questionnaire->business_type == 'Sole Trader'){{'selected'}}@endif>Sole Trader</option>
                                <option value="Partnership" @if($questionnaire->business_type == 'Partnership'){{'selected'}}@endif>Partnership</option>
                                <option value="Company" @if($questionnaire->business_type == 'Company'){{'selected'}}@endif>Company</option>
                                <option value="Charity" @if($questionnaire->business_type == 'Charity'){{'selected'}}@endif>Charity</option>
                                <option value="Not-for-profit Organisation" @if($questionnaire->business_type == 'Not-for-profit Organisation'){{'selected'}}@endif>Not-for-profit Organisation</option>
                            </select>
                        </div>
                 
                        <div class="mb-4 col-md-4">
                            <label for="comapany_register_number" class="form-label">Company Registration Number</label>
                            <input value="{{$questionnaire->comapany_register_number}}" type="text" class="form-control" id="comapany_register_number" readonly/>
                        </div>
                        <hr>
                        <h4 class="mb-3">Registered Business Address</h4>
                        <hr>
                        <div class="mb-4 col-md-4">
                            <label for="business_add_1" class="form-label">Address Line 1</label>
                            <input value="{{$questionnaire->business_add_1}}" type="text" class="form-control" id="business_add_1" readonly/>
                        </div>
                        <div class="mb-4 col-md-4">
                            <label for="business_add_2" class="form-label">Address Line 2</label>
                            <input value="{{$questionnaire->business_add_2}}" type="text" class="form-control" id="business_add_2" readonly/>
                        </div>
                        <div class="mb-4 col-md-4">
                            <label for="business_city" class="form-label">City</label>
                            <input value="{{$questionnaire->business_city}}" type="text" class="form-control" id="business_city" readonly/>
                        </div>
                        <div class="mb-4 col-md-4">
                            <label for="business_state" class="form-label">State</label>
                            <input value="{{$questionnaire->business_state}}" type="text" class="form-control" id="business_state" readonly/>
                        </div>
                        <div class="mb-4 col-md-4">
                            <label for="business_zip" class="form-label">Zip</label>
                            <input value="{{$questionnaire->business_zip}}" type="text" class="form-control" id="business_zip" readonly/>
                        </div>
                        <hr>
                        <h4 class="mb-3">Primary Contact Person Information </h4>
                        <hr>
                        <div class="mb-4 col-md-4">
                            <label for="firstname" class="form-label">First Name</label>
                            <input value="{{$questionnaire->firstname}}" type="text" class="form-control" id="firstname" readonly/>
                        </div>
                        <div class="mb-4 col-md-4">
                            <label for="middlename" class="form-label">Middle Name</label>
                            <input value="{{$questionnaire->middlename}}" type="text" class="form-control" id="middlename" readonly/>
                        </div>
                        <div class="mb-4 col-md-4">
                            <label for="lastname" class="form-label">Last Name</label>
                            <input value="{{$questionnaire->lastname}}" type="text" class="form-control" id="lastname" readonly/>
                        </div>
                        <div class="mb-4 col-md-4">
                            <label for="contact_number" class="form-label">Contact Number</label>
                            <input value="{{$questionnaire->contact_number}}" type="number" class="form-control" id="contact_number" readonly/>
                        </div>
                        <div class="mb-4 col-md-4">
                            <label for="country_citizenship" class="form-label">Country Citizenship</label>
                            <input value="{{$questionnaire->country_citizenship}}" type="text" class="form-control" id="country_citizenship" readonly/>
                        </div>
                        <div class="mb-4 col-md-4">
                            <label for="dob" cclass="form-label">DOB</label>
                            <input class="form-control" id="dob" type="date" value="{{$questionnaire->dob}}" readonly/>
                        </div>
                        <div class="mb-4 col-md-6">
                            <label for="add_1" class="form-label">Residential Address Line 1</label>
                            <input value="{{$questionnaire->add_1}}" type="text" class="form-control" id="add_1" readonly/>
                        </div>
                        <div class="mb-4 col-md-6">
                            <label for="add_2" class="form-label">Residential Address Line 2</label>
                            <input value="{{$questionnaire->add_2}}" type="text" class="form-control" id="add_2" readonly/>
                        </div>
                        <div class="mb-4 col-md-4">
                            <label for="city" class="form-label">Residential City</label>
                            <input value="{{$questionnaire->city}}" type="text" class="form-control" id="city" readonly/>
                        </div>
                        <div class="mb-4 col-md-4">
                            <label for="state" class="form-label">Residential State</label>
                            <input value="{{$questionnaire->state}}" type="text" class="form-control" id="state" readonly/>
                        </div>
                        <div class="mb-4 col-md-4">
                            <label for="zip" class="form-label">Residential Zip</label>
                            <input value="{{$questionnaire->zip}}" type="text" class="form-control" id="zip" readonly/>
                        </div>
                        <div class="mb-4 col-md-6">
                            <label for="is_primary_contact" class="form-label">Confirm if the primary contact person is</label>
                            <select class="form-select" id="is_primary_contact" readonly>
                                <option value="1" @if($questionnaire->is_primary_contact == 1){{'selecte'}}@endif>Is a beneficial owner of the business</option>
                                <option value="2" @if($questionnaire->is_primary_contact == 2){{'selecte'}}@endif>Is a legal representative of the business</option>
                            </select>
                        </div>
                        <hr>
                        <h4 class="mb-3">Payment Information</h4>
                        <hr>
                        <div class="mb-4 col-md-6">
                            <label for="bank_acc_holder_name" class="form-label">Bank Account Holder Name</label>
                            <input value="{{$questionnaire->bank_acc_holder_name}}" type="text" class="form-control" id="bank_acc_holder_name" readonly/>
                        </div>
                        <div class="mb-4 col-md-6">
                            <label for="bank_name" class="form-label">Bank Name</label>
                            <input value="{{$questionnaire->bank_name}}" type="text" class="form-control" id="bank_name" readonly/>
                        </div>
                        <div class="mb-4 col-md-6">
                            <label for="bank_acc_number" class="form-label">Bank Account Number</label>
                            <input value="{{$questionnaire->bank_acc_number}}" type="text" class="form-control" id="bank_acc_number" readonly/>
                        </div>
                        <div class="mb-4 col-md-6">
                            <label for="bank_ifsc" class="form-label">Bank IFSC Code</label>
                            <input value="{{$questionnaire->bank_ifsc}}" type="text" class="form-control" id="bank_ifsc" readonly/>
                        </div>
                        <hr>
                        <div class="mb-4 col-md-6">
                            <label for="brand_name" class="form-label">Brand Name</label>
                            <input value="{{$questionnaire->brand_name}}" type="text" class="form-control" id="brand_name" readonly/>
                        </div>

                        <div class="mb-4 col-md-6">
                            <label for="sell_products_type" class="form-label">Are you the manufacturer or brand owner (or agent or representative of the brand) for any of the products you want to sell on Amazon?  </label>
                            <textarea class="form-control" id="sell_products_type">{{$questionnaire->sell_products_type}}</textarea>
                        </div>
                        <div class="mb-4 col-md-6">
                            <label for="product_produce" class="form-label">Where were the goods produced/manufactured? </label>
                            <textarea class="form-control" id="product_produce">{{$questionnaire->product_produce}}</textarea>
                        </div>
                        <div class="mb-4 col-md-6">
                            <label for="product_sustainably" class="form-label">Are all your products/items produced sustainably? </label>
                            <textarea class="form-control" id="product_sustainably">{{$questionnaire->product_sustainably}}</textarea>
                        </div>
                        <div class="mb-4 col-md-6">
                            <label for="raw_materials_sourced" class="form-label">Are all your raw materials sourced sustainably? </label>
                            <textarea class="form-control" id="raw_materials_sourced">{{$questionnaire->raw_materials_sourced}}</textarea>
                        </div>
                        <div class="mb-4 col-md-6">
                            <label for="trademark" class="form-label">Do you own government-registered trademark for the branded products you want to sell?  </label>
                            <textarea class="form-control" id="trademark">{{$questionnaire->trademark}}</textarea>
                        </div>
                        
                        <div class="mb-4 col-md-6">
                            <label for="formFile" class="form-label">Proof of Identity</label>
                            <br>
                            <a href="@if($questionnaire->identity_card){{Storage::disk('vendor')->url($questionnaire->identity_card)}}@endif" download><image width="200" height="200" src="@if($questionnaire->identity_card){{Storage::disk('vendor')->url($questionnaire->identity_card)}}@endif"></a>
                        </div>
                        <div class="mb-4 col-md-6">
                            <label for="formFile2" class="form-label">Proof of Address</label>
                            <br>
                            <a href="@if($questionnaire->identity_card){{Storage::disk('vendor')->url($questionnaire->identity_card)}}@endif" download><image width="200" height="200" src="@if($questionnaire->address_proof){{Storage::disk('vendor')->url($questionnaire->address_proof)}}@endif"></a>

                        </div>
                </div>
            </div>
        </div>
    </section>
@endsection
