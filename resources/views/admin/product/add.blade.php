@extends('layouts.admin')
@section('page-content')
    <add-product :is_admin="@if(auth()->user()->hasRole('admin')){{1}}@else{{0}}@endif"/>
@endsection
