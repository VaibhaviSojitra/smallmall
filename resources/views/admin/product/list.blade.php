@extends('layouts.admin')
@section('page-content')
<section class="content-main">
    @if($is_seller == 1)
        @if($is_ques == 1)
            @if($getVendorQus && $getVendorQus->status == 2)
                <h4> Your account is not approved By Admin, Please update questionnaire.</h4>
                <a href="/admin/questionnaire" class="btn btn-primary">Update Questionnaire</a>
            @elseif($getVendorQus && $getVendorQus->status == 0)
                <h4> Your account is still not approved, If you want to update questionnaire change from here.</h4>
                <a href="/admin/questionnaire" class="btn btn-primary">Update Questionnaire</a>
            @elseif($getVendorQus && $getVendorQus->status == 3)
                <h4> To complete your approval please add questionnaire</h4>
                <a href="/admin/questionnaire" class="btn btn-primary">Add Questionnaire</a>
            @elseif($isSubscription == 0)
                <h4> To complete your approval please Subscribe</h4>
                <a href="/admin/subscription" class="btn btn-primary">Subscription</a>
            @else
                <product-widget :is_admin="@if(auth()->user()->hasRole('admin')){{1}}@else{{0}}@endif"></product-widget>
            @endif
        @elseif($is_ques == 0)
            <h4> To complete your approval please add questionnaire</h4>
            <a href="/admin/questionnaire" class="btn btn-primary">Add Questionnaire</a>
        @elseif($isSubscription == 0)
            <h4> To complete your approval please Subscribe</h4>
            <a href="/admin/subscription" class="btn btn-primary">Subscription</a>
        @else
            <product-widget :is_admin="@if(auth()->user()->hasRole('admin')){{1}}@else{{0}}@endif"></product-widget>
        @endif
    @else
        <product-widget :is_admin="@if(auth()->user()->hasRole('admin')){{1}}@else{{0}}@endif"></product-widget>
    @endif
</section>
@endsection
