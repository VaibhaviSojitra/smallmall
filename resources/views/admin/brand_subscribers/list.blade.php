@extends('layouts.admin')
@section('page-content')
<section class="content-main">

    <brand-subscribers-widget :is_admin="@if(auth()->user()->hasRole('seller')){{1}}@else{{0}}@endif"></brand-subscribers-widget>
    <!-- card .// -->
</section>
@endsection
