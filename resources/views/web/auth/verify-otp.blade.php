﻿@extends('web.layouts.web')

 @section('main')
    <main class="main pages">
        <div class="page-content pt-100 pb-150">
            <div class="container">
                <div class="row">
                    <div class="col-xl-4 col-lg-6 col-md-12 m-auto">
                        <div class="login_wrap widget-taber-content background-white">
                            <div class="padding_eight_all bg-white">
                                <div class="heading_s1">
                                    <img class="border-radius-15" src="assets/imgs/page/forgot_password.svg" alt="">
                                    <h2 class="mb-15 mt-15">Forgot your password?</h2>
                                    <p class="mb-30">Not to worry, we got you! Let’s get you a new password. Please enter your email address or your Username.</p>
                                </div>

                                @if(session()->has('message'))
                                    <div class="alert alert-success">
                                        {{ session()->get('message') }}
                                    </div>
                                @endif

                                <form method="get" action="{{ route('verify-forgot-otp') }}" id="forgotPasswordForm">
                                    @csrf
                                    <div class="form-group">
                                        <input type="text" name="email" id="email" class="@error('email') is-invalid @enderror" value="{{ $email }}" readonly placeholder="Username or Email *">
                                    </div>

                                    <div class="login_footer form-group">
                                        <div class="chek-form w-100">
                                            <input type="text" maxlength="6" name="security_code" id="security_code" class="@error('security_code') is-invalid @enderror" value="{{ old('security_code') }}" placeholder="Security Code *">
                                            @error('security_code')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-heading btn-block hover-up">Verify code</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection



