﻿@extends('web.layouts.web')

 @section('main')
    <main class="main pages">
        <div class="page-content pt-150 pb-150">
            <div class="container">
                <div class="row">
                    <div class="col-xl-4 col-lg-6 col-md-12 m-auto">
                        <div class="login_wrap widget-taber-content background-white">
                            <div class="padding_eight_all bg-white">
                                <div class="heading_s1">
                                    <img class="border-radius-15" src="assets/imgs/page/forgot_password.svg" alt="">
                                    <h2 class="mb-15 mt-15">Forgot your password?</h2>
                                    <p class="mb-30">Not to worry, we got you! Let’s get you a new password. Please enter your email address or your Username.</p>
                                </div>
                                <form method="post"  id="forgotPasswordForm">
                                    @csrf
                                    <div class="form-group">
                                        <input type="text" name="email" id="email" placeholder="Username or Email *">
                                        <span class="text-danger error-text email_err"></span>
                                    </div>

                                    {{-- Step 2 --}}
                                    <div class="login_footer form-group">
                                        <div class="chek-form w-100">
                                            <input type="text" maxlength="6" name="security_code" readonly id="security_code" placeholder="Security code *">
                                            <span class="text-danger error-text security_code_err"></span>
                                        </div>
                                    </div>

                                    {{-- Step 3 --}}
                                    <div class="form-group">
                                        <input type="password" class="d-none" name="password" id="password" placeholder="Password *">
                                        <span class="text-danger error-text password_err"></span>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="d-none" name="confirm_password" id="confirm_password" placeholder="Re-enter password *">
                                        <span class="text-danger error-text confirm_password_err"></span>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-heading btn-block hover-up forgotPasswordSubmit" id="forgotPasswordSubmit">Reset password</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('page-scripts')
    {{-- SEND FORGOT PASSWORD OTP  --}}
    <script>

        $( document ).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).on("click",".forgotPasswordSubmit",function(e) {
                e.preventDefault();
                $("#forgotPasswordSubmit").prop('disabled', true);
                // var formdata = new FormData(this);
                var email = $('#email').val();

                $.ajax({
                    url: "{{ route('send-forgot-otp') }}",
                    type: 'POST',
                    data: {
                        email: email
                    },
                    dataType: "json",
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function() {},
                    success: function(data) {
                        $("#forgotPasswordSubmit").prop('disabled', false);
                        if (!data.error && !data.error2) {
                            alert(data.success);
                            $("#forgotPasswordSubmit").addClass('submitOtp');
                            $("#forgotPasswordSubmit").text('Verify OTP');
                            $("#email").prop('readonly', true);
                            $("#security_code").prop('readonly', false);
                        }
                        else
                        {
                            if (data.error2)
                            {
                                alert(data.error2);
                            }
                            else
                            {
                                resetErrors();
                                printErrMsg(data.error);
                            }
                        }
                    },
                    error: function(error) {
                        $("#forgotPasswordSubmit").prop('disabled', false);
                        alert("something went wrong");
                    },
                });

                function resetErrors() {
                    var form = document.getElementById('forgotPasswordForm');
                    var data = new FormData(form);
                    for (var [key, value] of data)
                    {
                        $('.' + key + '_err').text('');
                        $('#' + key).removeClass('is-invalid');
                        $('#' + key).addClass('is-valid');
                    }
                }

                function printErrMsg(msg) {
                    $.each(msg, function(key, value)
                    {
                        $('.' + key + '_err').text(value);
                        $('#' + key).addClass('is-invalid');
                        $('#' + key).removeClass('is-valid');
                    });
                }
            });


            // STEP 2: Verifying otp
            $(document).on("click",".submitOtp",function(e) {
                e.preventDefault();

                $("#forgotPasswordSubmit").prop('disabled', true);
                // var security_code = $('#security_code').val();
                // var email = $("#email").val();
                var formdata = new FormData('#forgotPasswordForm');

                $.ajax({
                    url: "{{ route('verify-forgot-otp') }}",
                    type: 'POST',
                    data: formdata,
                    dataType: "json",
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function() {},
                    success: function(data) {
                        $("#forgotPasswordSubmit").prop('disabled', false);
                        if (!data.error && !data.error2) {
                            alert(data.success);
                            $("#forgotPasswordSubmit").removeClass('submitOtp');
                            $("#forgotPasswordSubmit").addClass('changePassword');
                            $("#forgotPasswordSubmit").text('Reset Password');
                            $("#email").prop('disabled', true);
                            $("#security_code").prop('readonly', false);
                            $("#password").removeClass('d-none');
                            $("#confirm_password").removeClass('d-none');
                        }
                        else
                        {
                            if (data.error2)
                            {
                                alert(data.error2);
                            }
                            else
                            {
                                resetErrors();
                                printErrMsg(data.error);
                            }
                        }
                    },
                    error: function(error) {
                        $("#forgotPasswordSubmit").prop('disabled', false);
                        alert("something went wrong");
                    },
                });

                function resetErrors() {
                    var form = document.getElementById('forgotPasswordForm');
                    var data = new FormData(form);
                    for (var [key, value] of data)
                    {
                        $('.' + key + '_err').text('');
                        $('#' + key).removeClass('is-invalid');
                        $('#' + key).addClass('is-valid');
                    }
                }

                function printErrMsg(msg) {
                    $.each(msg, function(key, value)
                    {
                        $('.' + key + '_err').text(value);
                        $('#' + key).addClass('is-invalid');
                        $('#' + key).removeClass('is-valid');
                    });
                }
            });


        });

    </script>
@endsection


