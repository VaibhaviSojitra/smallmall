@extends('web.layouts.web')
  
 @section('main')
    <main class="main pages">
        <div class="page-content pt-150 pb-150">
            <div class="container">
                <div class="row">
                    <div class="col-xl-8 col-lg-10 col-md-12 m-auto">
                    <div class="heading_s1">
                    </div>
                        <div class="row">
                            <div class="m-auto col-lg-6 col-md-8">
                                <div class="login_wrap widget-taber-content background-white">
                                    <div class="padding_eight_all bg-white">
                                        <div class="heading_s1">
                                            <h1 class="mb-5">Create an Account</h1>
                                        </div>
                                        @if ($errors->has('error'))
                                        <p class="text-danger">{{ $errors->first('error') }}</p>
                                        @endif
                                        <form method="POST" id="payment-form" data-cc-on-file="false" data-stripe-publishable-key="{{ config('stripe.api_keys.public_key') }}" action="{{ url('/seller/doregister') }}">
                                            @csrf
                                            <div class="form-group">
                                                <input type="text" required="" name="name" placeholder="Name" />
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                            </div>
                                            <div class="form-group">
                                                <input type="text" required="" name="email" placeholder="Email" />
                                                @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                            </div>
                                            <div class="form-group">
                                                <input required="" type="password" name="password" placeholder="Password" />
                                                @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                            </div>
                                            <div class="form-group">
                                                <input required="" type="password" name="password_confirmation" placeholder="Confirm password" />
                                            </div>
                                            <div class="form-group">
                                                <input required class="form-control" name="mobile" type="number" placeholder="Mobile Number" />
                                            </div>
                                            <!-- <div class="form-group">
                                               <select name="plan_id" class="form-select form-control" required>
                                                    <option>--Select--</option>
                                                    @foreach($plans as $value)
                                                    <option value="{{$value->id}}">{{$value->title}} - {{config('services.stripe.currency')}} {{$value->price/100}}</option>
                                                    @endforeach
                                               </select>
                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div id="card-element"></div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <span class="payment-errors" id="card-errors" style="color: red;margin-top:30px;"></span>
                                                </div>
                                            </div> -->

                                            <div class="form-group mb-30 mt-4">
                                                <!-- <input type="hidden" class="stripeToken" name="stripeToken"> -->
                                                <button type="submit" class="btn btn-fill-out btn-block hover-up font-weight-bold">  {{ __('Register') }}</button>
                                            </div>

                                            <p class="font-xs text-muted"><strong>Note:</strong>Your personal data will be used to support your experience throughout this website, to manage access to your account, and for other purposes described in our privacy policy</p>
                                        </form>

                                    </div>
                                </div>
                            </div>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
  
