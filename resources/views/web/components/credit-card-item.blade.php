<script type="x-tmpl" id="creditCardItem">
<div class="col-12 col-sm-12">
    <div class="form-check icon-check">
        <input class="form-check-input payment-source" type="radio" name="payment-source" value="${this.id}" id="${this.id}">
        <label class="form-check-label" for="${this.id}" id="saved-card">**** **** **** ${this.card.last4}</label>
        <i class="icon-check-1 far fa-circle color-gray-dark font-16"></i>
        <i class="icon-check-2 far fa-check-circle font-16 color-highlight"></i>
    </div>
</div>
</script>
<script type="x-tmpl" id="newCreditCardItem">
   <div class="col-12 col-sm-12">
       <div class="form-check icon-check">
           <label class="form-check-label" for="new-card-radio" id="saved-card">New Card</label>
           <input class="form-check-input payment-source" type="radio" name="payment-source" value="new-card" id="new-card-radio">
           <i class="icon-check-1 far fa-circle color-gray-dark font-16"></i>
           <i class="icon-check-2 far fa-check-circle font-16 color-highlight"></i>
       </div>
   </div>
</script>
