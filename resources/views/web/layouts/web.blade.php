<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8" />
    <title>Small Mall - Marketplace on your click</title>
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta property="og:title" content="" />
    <meta property="og:type" content="" />
    <meta property="og:url" content="" />
    <meta property="og:image" content="" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="/web-assets/imgs/theme/favicon.svg" />
    <!-- Template CSS -->
    {{-- <link rel="stylesheet" href="/web-assets/css/plugins/animate.min.css" /> --}}
    <link rel="stylesheet" href="/web-assets/css/main.css?v=5.6" />
    <link rel="stylesheet" href="/web-assets/css/custom.css" />
    @vite(['resources/js/app.js','resources/sass/main.scss'])

</head>

<body >
   <div id="app">
    @if(!Route::is('seller.register'))
    @include('web.layouts.header')
    @endif
   @yield('main')

   @if(!Route::is('web.login') && !Route::is('web.register') && !Route::is('seller.register'))
    @include('web.layouts.footer')
    @endif
</div>
    <!-- Preloader Start -->
    <div id="preloader-active">
        <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
                <div class="text-center">
                    <img src="/web-assets/imgs/theme/loading.gif" alt="" />
                </div>
            </div>
        </div>
    </div>

    @include('web.layouts.common-scripts')

    @yield('page-scripts')
    <!-- Template  JS -->
    <script src="/web-assets/js/main.js?v=5.6"></script>
    <script src="/web-assets/js/shop.js?v=5.6"></script>
    <input type="hidden" id="auth_id" value="{{ Auth::id() }}">
</body>

</html>
