<header class="header-area header-style-1 header-height-2">
    <div class="mobile-promotion">
        <span>Welcome to <strong>SmallMall</strong> choose your favorite products from wide range of categories.</span>
    </div>
    <div class="header-middle header-middle-ptb-1 d-none d-lg-block">
        <div class="container">
            <div class="header-wrap">
                <div class="logo logo-width-1">
                    <a href="/"><img src="{{asset('web-assets/imgs/theme/logo.webp')}}" alt="logo" /></a>
                </div>
                <div class="header-right">
                    <div class="search-style-2">
                        <search-product-form></search-product-form>

                    </div>
                    <div class="header-action-right">
                        <div class="header-action-2">
                            
                            <div class="header-action-icon-2">
                            @if(Auth::id())
                            <wishlist-widget></wishlist-widget>
                            @endif
                            </div>
                            <div class="header-action-icon-2">
                                <cart-widget :user_id="@if(Auth::user()){{Auth::user()->id}}@else{{0}}@endif"></cart-widget>
                            </div>
                            <div class="header-action-icon-2">
                                <a href="/my-account">
                                    <img class="svgInject" alt="SmallMall" src="{{asset('web-assets/imgs/theme/icons/icon-user.svg')}}" />
                                </a>
                                <a href="#"><span class="lable ml-0">Account</span></a>
                                <div class="cart-dropdown-wrap cart-dropdown-hm2 account-dropdown">
                                    <ul>
                                        <li>
                                            <a href="/my-account"><i class="fi fi-rs-user mr-10"></i>My Account</a>
                                        </li>
                                        <li>
                                            <a href="/my-account"><i class="fi fi-rs-location-alt mr-10"></i>Order</a>
                                        </li>
                                        @if(Auth::id())
                                        <li>
                                            <a href="/wishlist"><i class="fi fi-rs-heart mr-10"></i>My Wishlist</a>
                                        </li>
                                     
                                        @endif

                                        @if(Auth::id())
                                        <li>
                                            <a href="{{ url('logout') }}"><i class="fi fi-rs-sign-out mr-10"></i>Sign out</a>
                                        </li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-bottom header-bottom-bg-color sticky-bar">
        <div class="container">
            <div class="header-wrap header-space-between position-relative">
                <div class="logo logo-width-1 d-block d-lg-none">
                    <a href="/"><img src="{{asset('web-assets/imgs/theme/logo.webp')}}" alt="logo" /></a>
                </div>
                <div class="header-nav d-none d-lg-flex">
                    <div class="main-categori-wrap d-none d-lg-block">
                        <a class="categories-button-active" href="#">
                            <span class="fi-rs-apps"></span> <span class="et">Browse</span> All Categories
                            <i class="fi-rs-angle-down"></i>
                        </a>
                        <div class="categories-dropdown-wrap categories-dropdown-active-large font-heading">
                            <header-category-list></header-category-list>
                            <!-- <div class="more_categories"><span class="icon"></span> <span class="heading-sm-1">Show more...</span></div> -->
                        </div>
                    </div>
                    <div class="main-menu main-menu-padding-1 main-menu-lh-2 d-none d-lg-block font-heading">
                        <nav>
                            <ul>
                                <li>
                                    <a class="active" href="/">Home</a>
                                </li>
                                <!-- <li>
                                    <a href="/about">About</a>
                                </li> -->
                                <li>
                                    <a href="/shop">Mall </a>
                                </li>
                                <li>
                                    <a href="/vendors">Brands</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="hotline d-none d-lg-flex">
                    <img src="{{asset('web-assets/imgs/theme/icons/icon-headphone.svg')}}" alt="hotline" />
                    <p>1900 - 888<span>24/7 Support Center</span></p>
                </div>
                <div class="header-action-icon-2 d-block d-lg-none">
                    <div class="burger-icon burger-icon-white">
                        <span class="burger-icon-top"></span>
                        <span class="burger-icon-mid"></span>
                        <span class="burger-icon-bottom"></span>
                    </div>
                </div>
                <div class="header-action-right d-block d-lg-none">
                    <div class="header-action-2">
                        <div class="header-action-icon-2">
                            @if(Auth::id())
                            <wishlist-widget></wishlist-widget>
                            @endif
                        </div>
                        <div class="header-action-icon-2">
                            <cart-widget :user_id="@if(Auth::user()){{Auth::user()->id}}@else{{0}}@endif"></cart-widget>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="mobile-header-active mobile-header-wrapper-style">
    <div class="mobile-header-wrapper-inner">
        <div class="mobile-header-top">
            <div class="mobile-header-logo">
                <a href="/"><img src="{{asset('web-assets/imgs/theme/logo.webp')}}" alt="logo" /></a>
            </div>
            <div class="mobile-menu-close close-style-wrap close-style-position-inherit">
                <button class="close-style search-close">
                    <i class="icon-top"></i>
                    <i class="icon-bottom"></i>
                </button>
            </div>
        </div>
        <div class="mobile-header-content-area">
           
            <div class="mobile-menu-wrap mobile-header-border">
                <!-- mobile menu start -->
                <nav>
                    <ul class="mobile-menu font-heading">
                        <li>
                            <a href="/">Home</a>
                        </li>
                        <li>
                            <a href="/shop">Mall</a>
                        </li>
                        <li >
                            <a href="/vendor-list">Brands</a>
                          
                        </li>
                       
                    </ul>
                </nav>
                <!-- mobile menu end -->
            </div>
            <div class="mobile-header-info-wrap">
                <div class="single-mobile-header-info">
                    <a href="/contact"><i class="fi-rs-marker"></i> Our location </a>
                </div>
                <div class="single-mobile-header-info">
                    <a href="/login"><i class="fi-rs-user"></i>Log In / Sign Up </a>
                </div>
                <div class="single-mobile-header-info">
                    <a href="#"><i class="fi-rs-headphones"></i>(+01) - 2345 - 6789 </a>
                </div>
            </div>
            <div class="mobile-social-icon mb-50">
                <h6 class="mb-15">Follow Us</h6>
                <a href="#"><img src="{{asset('web-assets/imgs/theme/icons/icon-facebook-white.svg')}}" alt="" /></a>
                <a href="#"><img src="{{asset('web-assets/imgs/theme/icons/icon-twitter-white.svg')}}" alt="" /></a>
                <a href="#"><img src="{{asset('web-assets/imgs/theme/icons/icon-instagram-white.svg')}}" alt="" /></a>
                <a href="#"><img src="{{asset('web-assets/imgs/theme/icons/icon-pinterest-white.svg')}}" alt="" /></a>
                <a href="#"><img src="{{asset('web-assets/imgs/theme/icons/icon-youtube-white.svg')}}" alt="" /></a>
            </div>
            <div class="site-copyright">Copyright 2023 © SmallMall. All rights reserved.</div>
        </div>
    </div>
</div>