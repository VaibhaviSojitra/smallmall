    <!-- Vendor JS-->
    <script src="/web-assets/js/vendor/modernizr-3.6.0.min.js"></script>
    <script src="/web-assets/js/vendor/jquery-3.6.0.min.js"></script>
    <script src="/web-assets/js/vendor/jquery-migrate-3.3.0.min.js"></script>
    <script src="/web-assets/js/vendor/bootstrap.bundle.min.js"></script>
    <script src="/web-assets/js/plugins/slick.js"></script>
    <script src="/web-assets/js/plugins/jquery.syotimer.min.js"></script>
    <script src="/web-assets/js/plugins/waypoints.js"></script>
    <script src="/web-assets/js/plugins/wow.js"></script>
    <script src="/web-assets/js/plugins/perfect-scrollbar.js"></script>
    <script src="/web-assets/js/plugins/magnific-popup.js"></script>
    <script src="/web-assets/js/plugins/select2.min.js"></script>
    <script src="/web-assets/js/plugins/counterup.js"></script>
    <script src="/web-assets/js/plugins/jquery.countdown.min.js"></script>
    <script src="/web-assets/js/plugins/images-loaded.js"></script>
    <script src="/web-assets/js/plugins/isotope.js"></script>
    <script src="/web-assets/js/plugins/scrollup.js"></script>
    <script src="/web-assets/js/plugins/jquery.vticker-min.js"></script>
    <script src="/web-assets/js/plugins/jquery.theia.sticky.js"></script>
    {{-- <script src="/web-assets/js/plugins/jquery.elevatezoom.js"></script> --}}

    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/63c9422c47425128790e8e35/1gn52kvi8';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
    <!--End of Tawk.to Script-->
