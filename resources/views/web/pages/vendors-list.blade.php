@extends('web.layouts.web')

@section('main')
    <main class="main pages mb-80">
        <div class="page-header breadcrumb-wrap">
            <div class="container">
                <div class="breadcrumb">
                    <a href="/" rel="nofollow"><i class="fi-rs-home mr-5"></i>Home</a>
                    <span></span> Brands
                </div>
            </div>
        </div>
        <div class="page-content pt-50">
            <div class="container">
                <div class="archive-header-2 text-center">
                    <h3 class="mb-50">Shop by Brands</h3>
                </div>

                <vendors-List :is_auth="{{ auth()->check() ? 1 : 0 }}"></vendors-List>

            </div>
        </div>
    </main>

    @endsection

  @section('page-scripts')

   @endsection
