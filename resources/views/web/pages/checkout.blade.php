@extends('web.layouts.web')
   
@section('main')

    <main class="main">
        <div class="page-header breadcrumb-wrap">
            <div class="container">
                <div class="breadcrumb">
                    <a href="/" rel="nofollow"><i class="fi-rs-home mr-5"></i>Home</a>
                    <span></span> Shop
                    <span></span> Checkout
                </div>
            </div>
        </div>
        <div class="container mb-80 mt-50">
            <checkout-title></checkout-title>
            @if(count($cart['cart']) > 0)
            <div class="row">
                <div class="col-lg-7">
                    @if(count($userAddress) > 0)
                    <div class="row mb-20">
                        <h4 class="mb-30">Select Address</h4>
                        <div id="userAddress">
                            @foreach($userAddress as $value)
                                @php
                                if($value['address_type'] == 1){
                                    $address_type = 'Home';
                                }else if($value['address_type'] == 2){
                                    $address_type = 'Office';
                                }else{
                                    $address_type = 'Other';
                                }
                                @endphp
                                <div class="form-check list-address-selected">
                                    <input class="form-check-input address_id" type="radio" name="address_id" id="inlineCheckbox{{$value->id}}" value="{{$value->id}}">
                                    <label class="form-check-label" for="inlineCheckbox{{$value->id}}"><strong>{{$value->full_name}}</strong> - {{$value->add_1}}, {{$value->add_2}}, {{$value->city}},{{$value->state}}, {{$value->zip}} - <strong>{{$value->mobile}}</strong>   <strong class="badge badge-pill" style="background-color: #111FA3;">{{$address_type}}</strong> </label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                   
                    @else
                    <div class="row mb-20">
                        <div id="userAddress">
                        </div>
                    </div>
                    @endif
                    <hr/>
                    <h6 class="mb-10">OR Add New Address </h6>
                    <div class="row">
                        <h4 class="mb-30">Billing Details</h4>
                        <form method="post" id="addUserAddressForm">
                            @csrf
                            <div class="row">
                                <div class="form-group col-lg-12">
                                    <input type="text" name="full_name" class="full_name" placeholder="Full name *">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-lg-6">
                                    <input type="text" name="add_1" placeholder="Address *">
                                </div>
                                <div class="form-group col-lg-6">
                                    <input type="text" name="add_2" placeholder="Address line2">
                                </div>
                            </div>
                            <div class="row shipping_calculator">
                                <div class="form-group col-lg-6">
                                    <input type="text" name="city" placeholder="City / Town *">
                                </div>
                                <div class="form-group col-lg-6">
                                    <input type="text" name="state" placeholder="State / County *">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-lg-6">
                                    <input type="text" name="zip" placeholder="Postcode / ZIP *">
                                </div>
                                <div class="form-group col-lg-6">
                                    <input type="text" name="mobile" placeholder="Phone *">
                                </div>
                            </div>
                            <div class="ship_detail">
                                <div class="form-group">
                                    <div class="chek-form">
                                        <div class="custome-checkbox">
                                            <input class="form-check-input" type="checkbox" name="is_shipping_address" id="is_shipping_address_chk">
                                            <label class="form-check-label label_info" data-bs-toggle="collapse" data-target="#collapseAddress" href="#collapseAddress" aria-controls="collapseAddress" for="is_shipping_address_chk"><span>Ship to a different address?</span></label>
                                        </div>
                                    </div>
                                </div>
                                <div id="collapseAddress" class="different_address collapse in">
                                    <div class="row">
                                        <div class="form-group col-lg-12">
                                            <input type="text" name="s_full_name" placeholder="Full name *">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-lg-6">
                                            <input type="text" name="s_add_1" placeholder="Address *">
                                        </div>
                                        <div class="form-group col-lg-6">
                                            <input type="text" name="s_add_2" placeholder="Address line2">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-lg-6">
                                            <input type="text" name="s_state" placeholder="State / County *">
                                        </div>
                                        <div class="form-group col-lg-6">
                                            <input type="text" name="s_city" placeholder="City / Town *">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-lg-6">
                                            <input type="text" name="s_zip" placeholder="Postcode / ZIP *">
                                        </div>
                                        <div class="form-group col-lg-6">
                                            <input type="text" name="s_mobile" placeholder="Phone *">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="user_id" :value="user_id" />
                            <button class="btn btn-fill-out btn-block mt-30">Add Address</button>
                        </form>
                    </div>
                </div>
        
                <div class="col-lg-5">
                    <checkout-cart-item></checkout-cart-item>
                    <div class="payment ml-30">
                        <h4 class="mb-30">Payment</h4>
                        <form method="post" id="placeOrderForm">
                        @csrf

                            <div class="payment_option">
                                <!-- <div class="custome-radio">
                                    <input class="form-check-input" type="radio" name="payment_type" id="exampleRadios4" checked="" value="1">
                                    <label class="form-check-label" for="exampleRadios4">Cash on delivery</label>
                                </div> -->
                                <div class="custome-radio">
                                    <input class="form-check-input" type="radio" name="payment_type" id="exampleRadios5" value="2" checked>
                                    <label class="form-check-label" for="exampleRadios5" >Online Getway</label>
                                </div>
                            </div>
                            <div class="payment-logo d-flex">
                                <img class="mr-15" src="web-assets/imgs/theme/icons/payment-visa.svg" alt="">
                                <img class="mr-15" src="web-assets/imgs/theme/icons/payment-master.svg" alt="">
                            </div>

                            <input type="hidden" id="billing_address_id" name="billing_address_id">
                            <input type="hidden" id="is_shipping_address" name="is_shipping_address" value="0">
                            <input type="hidden" id="shipping_address_id" name="shipping_address_id">
                            <p class="m-t-md error unselect_addess" style="display: none;">Please select a address</p>
                            <p class="m-t-md  mt-20 address_info">Please select a address or add new address after that you can place order</p>
                            <button class="btn btn-fill-out btn-block mt-20 place_order"  style="display: none;" disabled>Place an Order<i class="fi-rs-sign-out ml-15"></i></button>

                        </form>
                        <div class="cardDetails mt-4">
                        <div class='wallet-error' style="color:red;display:none"></div>

                            <form role="form" action="" method="post" class="stripe-payment" data-cc-on-file="false" data-stripe-publishable-key="{{ config('stripe.api_keys.public_key') }}" id="payment-form">
                                @csrf
                                <input type="hidden" id="total_amount" name="total_amount" value="{{$cart['total']}}">
                                <input type='hidden' id="user" value="@if (auth()->check()){{auth()->user()->name}}@endif" />
                                <input type='hidden' id="email" value="@if (auth()->check()){{auth()->user()->email}}@endif" />
                                <input type='hidden' id="mobile_number" value="@if (auth()->check()){{auth()->user()->mobile_number}}@endif" />
                                <input type='hidden' id="payment_preference" value="stripe" />
                                
                                <div class="card-body p-0">
                                    <div class="row mb-0" id="user_card_list">
                                
                                    </div>

                                    <div class="col-12 col-sm-12 input-style has-borders no-icon mb-4 input-style-active address-fields">

                                        <div id="card-element"></div>

                                        <div id="card-errors" role="alert" style="color: red;margin-top: 3%;"></div>
                                    
                                    </div>
                                    <div class="form-check icon-check">
                                        <label class="form-check-label" for="is_card_save">Save this card for future payments</label>
                                        <input class="form-check-input" name="is_card_save" type="checkbox" value="1" id="is_card_save">

                                        <i class="icon-check-1 fa fa-square color-gray-dark font-16"></i>
                                        <i class="icon-check-2 fa fa-check-square font-16 color-highlight"></i>
                                    </div>

                                </div>
                        
                            </form>
                            <p class="m-t-md  mt-20 pay_now_address_info">Please select a address or add new address after that you can place order</p>
                            <button class="btn btn-fill-out btn-block mt-20 pay_now" disabled>Place an Order<i class="fi-rs-sign-out ml-15"></i></button>

                        </div>

                    </div>
                </div>
            </div>
            @else
            <div class="row">
                <div class="col-md-4">
                    <a class="btn" href="/shop"><i class="fi-rs-arrow-left mr-10"></i>Continue Shopping</a>
                </div>
            </div>
            @endif
        </div>
    </main>
    @endsection
  
    @include('web.components.credit-card-item')


   @section('page-scripts')
   <script src="/web-assets/js/vendor/validation/jquery.validate.min.js"></script>
   <script src="/web-assets/js/vendor/validation/additional-methods.min.js"></script>
   <script src="https://js.stripe.com/v3/"></script>

   <script>
        $(document).ready(function(){

            var paymentObject = {
                total_amount:  $("#total_amount").val(),
                payment_type: 'stripe',
                is_shipping_address: $("#is_shipping_address").val(),
                billing_address_id: $("#billing_address_id").val(),
                shipping_address_id: $("#shipping_address_id").val(),
                order_id: null,
                use_saved_card: 1
            };
            
            $(document).on('change', 'input:radio[name="address_id"]', function() {
                if ($(this).is(':checked')) {
                    $(".place_order").removeAttr('disabled');
                    $(".pay_now").removeAttr('disabled');
                    validator.resetForm();
                    $('#addUserAddressForm')[0].reset();
                    $('#addUserAddressForm').find(".error").removeClass("error");
                    $("#billing_address_id").val($(this).val());
                    $("#is_shipping_address").val(0);
                    $("#shipping_address_id").val($(this).val());

                    paymentObject.is_shipping_address = $("#is_shipping_address").val();
                    paymentObject.billing_address_id = $("#billing_address_id").val();
                    paymentObject.shipping_address_id = $("#shipping_address_id").val();
                    // $(".address_info").hide()
                }else{
                    $(".place_order").attr('disabled','disabled');
                    $(".pay_now").attr('disabled','disabled');
                    $("#billing_address_id").val("");
                    $("#is_shipping_address").val(0);
                    $("#shipping_address_id").val("");
                }
            });
            $(document).on('change', 'input:checkbox[name="is_shipping_address"]', function() {
                if ($(this).is(':checked')) {
                   $("#is_shipping_address").val(1);
                }else{
                    $("#is_shipping_address").val(0);
                }
            });
            $(document).on('keyup', '.full_name', function() {
                $('input:radio[name="address_id"]').prop('checked', false);
                $(".place_order").attr('disabled','disabled');
            });
            var validator = $('#addUserAddressForm').validate({
                rules: {
                    'full_name': {
                        required: true
                    },
                    'add_1': {
                        required: true
                    },
                    'add_2': {
                        required: true
                    },
                    'city':{
                        required: true
                    },
                    'state':{
                        required: true
                    },
                    'zip':{
                        required: true
                    },
                    'mobile':{
                        required: true
                    },
                    's_full_name': {
                        required: function() {
                            if ($('input[name="is_shipping_address"]').is(':checked')) {
                                return true;
                            }
                            else {
                                return false;
                            }
                        },
                    },
                    's_add_1': {
                       required: function() {
                            if ($('input[name="is_shipping_address"]').is(':checked')) {
                                return true;
                            }
                            else {
                                return false;
                            }
                        },
                    },
                    's_add_2': {
                       required: function() {
                            if ($('input[name="is_shipping_address"]').is(':checked')) {
                                return true;
                            }
                            else {
                                return false;
                            }
                        },
                    },
                    's_city':{
                       required: function() {
                            if ($('input[name="is_shipping_address"]').is(':checked')) {
                                return true;
                            }
                            else {
                                return false;
                            }
                        },
                    },
                    's_state':{
                       required: function() {
                            if ($('input[name="is_shipping_address"]').is(':checked')) {
                                return true;
                            }
                            else {
                                return false;
                            }
                        },
                    },
                    's_zip':{
                       required: function() {
                            if ($('input[name="is_shipping_address"]').is(':checked')) {
                                return true;
                            }
                            else {
                                return false;
                            }
                        },
                    },
                    's_mobile':{
                       required: function() {
                            if ($('input[name="is_shipping_address"]').is(':checked')) {
                                return true;
                            }
                            else {
                                return false;
                            }
                        },
                    },
                },
                messages: {
                    'full_name': {
                        required: '*Please Enter Full Name'
                    },
                    'add_1': {
                        required: '*Please Enter Address 1'
                    },
                    'add_2': {
                        required: '*Please Enter Address 2'
                    }, 
                    'city': {
                        required: 'Please Enter City'
                    },
                    'state': {
                        required: 'Please Enter State'
                    },
                    'zip': {
                        required: 'Please Enter Zip'
                    },
                    'mobile': {
                        required: 'Please Enter Phone'
                    },
                    's_full_name': {
                        required: '*Please Enter Full Name'
                    },
                    's_add_1': {
                        required: '*Please Enter Address 1'
                    },
                    's_add_2': {
                        required: '*Please Enter Address 2'
                    }, 
                    's_city': {
                        required: 'Please Enter City'
                    },
                    's_state': {
                        required: 'Please Enter State'
                    },
                    's_zip': {
                        required: 'Please Enter Zip'
                    },
                    's_mobile': {
                        required: 'Please Enter Phone'
                    },
                }
            });
            $(document).on('submit', '#addUserAddressForm', function(e) {
                e.preventDefault();
                 var formdata = new FormData($("#addUserAddressForm")[0]);
                $.ajax({
                    type: "POST",
                    url: '{{url( "addUserAddress")}}',
                    data: formdata,
                    dataType: "json",
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function() {},
                    success: function(data) {
                        if(data.success == 1){
                            $(".place_order").removeAttr('disabled');
                            $(".pay_now").removeAttr('disabled');
                            var html = '';
                            if(data.data.billing_address){
                                $("#billing_address_id").val(data.data.billing_address.id);
                                if(data.data.shipping_address){
                                    $("#is_shipping_address").val(1);
                                    $("#shipping_address_id").val(data.data.shipping_address.id);
                                }else{
                                    $("#is_shipping_address").val(0);
                                    $("#shipping_address_id").val(data.data.billing_address.id);
                                }
                                validator.resetForm();
                                $('#addUserAddressForm')[0].reset();
                                $('#addUserAddressForm').find(".error").removeClass("error");
                                $('input:radio[name="address_id"]').prop('checked', false);
                                $(data.data.billing_address).each(function( i,value ) {
                                    var checked = 'checked';
                                    if(value.address_type == 1){
                                        var address_type = 'Home';
                                    }else if(value.address_type == 2){
                                        var address_type = 'Office';
                                    }else{
                                        var address_type = 'Other';
                                    }
                                    html += '<div class="form-check list-address-selected">     <input class="form-check-input address_id" type="radio" name="address_id" id="inlineCheckbox'+value.id+'" value="'+value.id+'" '+checked+'>  <label class="form-check-label" for="inlineCheckbox'+value.id+'"><strong>'+value.full_name+'</strong>  - '+value.add_1+', '+value.add_2+', '+value.city+','+value.state+', '+value.zip+' - <strong>'+value.mobile+'</strong>   <strong class="badge badge-pill" style="background-color: #111FA3;">'+address_type+'</strong> </label>   </div>';
                                });
                                $("#userAddress").append(html);
                                paymentObject.is_shipping_address = $("#is_shipping_address").val();
                                paymentObject.billing_address_id = $("#billing_address_id").val();
                                paymentObject.shipping_address_id = $("#shipping_address_id").val();
                            }
                        }else{
                            $(".place_order").addClass('disabled');
                            $(".pay_now").addClass('disabled');
                            $("#billing_address_id").val("");
                            $("#shipping_address_id").val("");
                        } 
                    }, 
                    complete: function() {}
                })
            });
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).on('change', 'input:radio[name="payment_type"]', function() {
                if ($(this).val() == 1) {
                    $(".place_order").removeAttr('disabled');
                    $(".cardDetails").hide();
                }else{
                    $(".place_order").hide();
                    $(".cardDetails").show();
                    $(".address_info").hide();
                }
                var billing_address_id = $('#billing_address_id').val();
                if(billing_address_id){
                    $(".place_order").removeAttr('disabled');
                }else{
                    $(".place_order").attr('disabled','disabled');
                }
            });

            var $form = $(".stripe-payment");
            var $payNowBtn = $('.pay_now');
            var $walletError = $('.wallet-error');
            // Create a Stripe client.
            if (document.getElementById('payment_preference').value == 'stripe') {
                var stripe = Stripe($form.data('stripe-publishable-key'));

                // Create an instance of Elements.
                var elements = stripe.elements();
                var card = elements.create('card', {
                    hidePostalCode: true,
                    style: {
                        base: {
                            iconColor: '#666EE8',
                            color: '#32325d',
                            lineHeight: '40px',
                            fontWeight: 300,
                            fontFamily: 'Helvetica Neue',
                            fontSize: '15px',

                            '::placeholder': {
                                color: '#aab7c4',
                            },
                        },
                    }
                });

                var customerCards = [];
                var cardContainer = document.getElementById('user_card_list');
                var tmpl = document.getElementById('creditCardItem').innerHTML;

                getCardList();

                card.mount('#card-element');
                card.addEventListener('change', function (event) {
                    var displayError = document.getElementById('card-errors');
                    displayError.textContent = '';
                    if (event.error) {
                        stripeError(event.error.message);
                        displayError.textContent = event.error.message;
                    }
                });
                card.addEventListener('focus', function (event) {
                    document.querySelector('#new-card-radio').checked = true;
                });

                function listCards(item, index, arr) {
                    const tmplParse = function (templateString, templateVars) {
                        return new Function("return `" + templateString + "`;").call(templateVars);
                    };
                    cardContainer.innerHTML += tmplParse(tmpl, item);
                }
            }

            function getCardList() {
                $.ajax({
                    type: 'GET',
                    url: "/get-cards",
                    success: function (data) {
                        if(data.data){
                            customerCards = data.data;
                            cardContainer.innerHTML = '';
                            customerCards.forEach(listCards);
                            const tmplParse = function (templateString, templateVars) {
                                return new Function("return `" + templateString + "`;").call(templateVars);
                            };
                            var newTmpl = document.getElementById('newCreditCardItem').innerHTML;
                            cardContainer.innerHTML += tmplParse(newTmpl);
                        }
                    
                    }
                });
            }

            function stripeError(msg) {
                var errorElement = document.getElementById('card-errors');
                errorElement.textContent = msg;
            }

            $(document).on('click', '.payment-source', function (e) {
                if (e.target.value != 'new-card') {
                    $('.address-fields').each(function () {
                        $(this).css('display', 'none');
                    });
                } else {
                    $('.address-fields').each(function () {
                        $(this).css('display', 'block');
                    });
                }
            });

            $(document).on('click', '.pay_now', function (e) {
                e.preventDefault();
                var form_type = $payNowBtn.data('form_type');
                paymentObject.total_amount = $('#total_amount').val();
                createStripePaymentMethod();
            });

            function createOrder() {
                $.ajax({
                    type: 'POST',
                    url: "/placeOrder",
                    data: {
                        total_amount: paymentObject.total_amount,
                        billing_address_id: paymentObject.billing_address_id,
                        is_shipping_address: paymentObject.is_shipping_address,
                        shipping_address_id: paymentObject.shipping_address_id,
                        payment_type: 2,
                        payment_method_id: paymentObject.payment_method_id,
                        save_card: paymentObject.save_card,
                        payment_id: paymentObject.payment_id,
                        use_saved_card : paymentObject.use_saved_card,
                    },
                    success: function (data) {
                        paymentObject.order_id = data.order_id;
                        stripeTokenHandler();
                    }
                });   
            }

            function createStripePaymentMethod(){
                paymentObject.save_card = 0;
                paymentObject.use_saved_card = 0;
                paymentObject.payment_method_id = null;
                paymentObject.order_id = null;

                var address_id =  $('#billing_address_id').val();
                if (!address_id) {
                    $walletError.css('display', 'block');
                    $walletError.html('Please select address');
                    $(".pay_now").attr('disabled','disabled');
                }else{

                    $(".pay_now").removeAttr('disabled');
                    var radioButton = document.querySelector('input[name="payment-source"]:checked');
                    if (radioButton == null) {
                        $walletError.css('display', 'block');
                        $walletError.html('Please select payment option');
                    } else {
                        $walletError.css('display', 'none');
                        $walletError.html('');

                        if (radioButton.value == 'new-card') {
                            if ($('#is_card_save').prop('checked') === true) {
                                paymentObject.save_card = 1;
                            }
                            stripe.createPaymentMethod({
                                type: 'card',
                                card: card,
                                billing_details: {
                                    name: $(".stripe-payment #user").val(),
                                },
                            }).then(function (result) {
                                console.log(result);
                                if (result.error) {
                                    stripeError(result.error.message);
                                } else {
                                    payment_method_id = result.paymentMethod.id;
                                    stripe.createToken(card).then(function (result) {
                                        if (result.error) {
                                            $payNowBtn.attr('disabled', false);
                                            stripeError(result.error.message);
                                        } else {
                                            paymentObject.payment_id = result.token.id;
                                            $payNowBtn.attr('disabled', true);
                                            $payNowBtn.html('Processing...');
                                            paymentObject.payment_id = result.token.id;
                                            paymentObject.purchase_preference = 'stripe';
                                            createOrder();
                                            
                                        }
                                    });
                                }
                            });
                        } else {
                            console.log(radioButton.value);
                            paymentObject.payment_method_id = radioButton.value;
                            paymentObject.use_saved_card = 1;
                            paymentObject.save_card = 0;
                            paymentObject.purchase_preference = 'stripe';
                            createOrder();
                        }
                    }
                }
            }
            function stripeTokenHandler() {
                $payNowBtn.html('Processing...');
                $.ajax({
                    url: '/payment',
                    type: 'post',
                    data: paymentObject,
                    success: function (response) {
                        $payNowBtn.html('Confirming Payment...');
                        $payNowBtn.attr('disabled', false);

                        if (response.success == true) {
                            $("#menu-cards").removeClass("menu-active");
                            $(".menu-hider").removeClass("menu-active");
                            getCardList();
                            card.clear();
                            $payNowBtn.html('Place an Order');
                            // card.elements.clear();
                            $form[0].reset();
                            window.location.href = "{{url('my-account')}}";

                        } else {
                            $(".error-text").css({
                                "display": "block",
                            });
                            $('.error-text').html("Error processing payment. Try again.!");
                        }

                    }
                });
            }

            $(document).on('submit', '#placeOrderForm', function(e) {
                e.preventDefault();
                var billing_address_id = $('#billing_address_id').val();
                var payment_type = $('input:radio[name="payment_type"]:checked').val();
                var formdata = new FormData($("#placeOrderForm")[0]); 
                $(".unselect_addess").hide();
                $(".unselect_addess").text('');
                $(".cardDetails").hide();

                if(billing_address_id){

                    $.ajax({
                            type: "POST",
                            url: '{{url("placeOrder")}}',
                            data: formdata,
                            dataType: "json",
                            contentType: false,
                            cache: false,
                            processData: false,
                            beforeSend: function() {},
                            success: function(data) {
                                if(1 == data.success){
                                    window.location.href = "{{url('my-account')}}";
                                }
                            }, 
                            complete: function() {}
                        })
                
                }else{
                    $(".unselect_addess").show();
                }
            });
        });

    </script>

    @endsection
