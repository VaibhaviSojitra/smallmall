@extends('web.layouts.web')
   
@section('main')
    <main class="main">
        <div class="page-header breadcrumb-wrap">
            <div class="container">
                <div class="breadcrumb">
                    <a href="/" rel="nofollow"><i class="fi-rs-home mr-5"></i>Home</a>
                    <span></span> <a href="/shop" rel="nofollow">Shop</a> <span></span> Wishlist
                </div>
            </div>
        </div>
        <div class="container mb-30 mt-50">
            <wishlist-list></wishlist-list>       
        </div>
    </main>
    @endsection
  
   @section('page-scripts')
 
    @endsection
