@extends('web.layouts.web')
   
@section('main')
    <main class="main pages">
        <div class="page-header breadcrumb-wrap">
            <div class="container">
                <div class="breadcrumb">
                    <a href="/" rel="nofollow"><i class="fi-rs-home mr-5"></i>Home</a>
                    <span></span> <a href="/my-account" rel="nofollow"> My Account</a>
                    <span></span> View Order 
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row invoice invoice-content invoice-4">
                <div class="col-lg-12">
                    <order-details :order="{{$order}}"></order-details>
                </div>
            </div>
        </div>
       
    </main>
@endsection
  
@section('page-scripts')

<script src="/web-assets/js/invoice/html2canvas.js">
</script>
<script src="/web-assets/js/invoice/jspdf.min.js"></script>

    <script src="/web-assets/js/invoice/invoice.js"></script>

@endsection
