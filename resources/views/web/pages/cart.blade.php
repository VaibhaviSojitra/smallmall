@extends('web.layouts.web')
   
@section('main')

    <main class="main">
        <div class="page-header breadcrumb-wrap">
            <div class="container">
                <div class="breadcrumb">
                    <a href="/" rel="nofollow"><i class="fi-rs-home mr-5"></i>Home</a>
                    <span></span> <a href="/shop" rel="nofollow">Shop</a>
                    <span></span> Cart
                </div>
            </div>
        </div>
        <div class="container mb-80 mt-50">
        <cart-list :user_id="@if(Auth::user()){{Auth::user()->id}}@else{{0}}@endif"></cart-list>

        </div>
    </main>
    @endsection
  
   @section('page-scripts')
 
    @endsection
