@extends('web.layouts.web')
   
@section('main')
<main class="main">
        <div class="page-header mt-30 mb-50">
            <div class="container">
                <div class="archive-header">
                    <div class="row align-items-center">
                        <div class="col-xl-3">
                        @if(isset($category))
                        <h1 class="mb-15">{{$category->name}}</h1>
                        <div class="breadcrumb">
                            <a href="/" rel="nofollow"><i class="fi-rs-home mr-5"></i>Home</a>
                            <span></span> <a href="/shop" rel="nofollow">Shop</a> <span></span> {{$category->name}}
                        </div>
                        @else   
                        <h1 class="mb-15">Shop</h1>
 
                        <div class="breadcrumb">
                            <a href="/" rel="nofollow"><i class="fi-rs-home mr-5"></i>Home</a>
                            <span></span> Shop 
                        </div>
                        @endif
                        
                        </div>
                        <!-- <div class="col-xl-9 text-end d-none d-xl-block">
                            <ul class="tags-list">
                                <li class="hover-up">
                                    <a href="#"><i class="fi-rs-cross mr-10"></i>Cabbage</a>
                                </li>
                                <li class="hover-up active">
                                    <a href="#"><i class="fi-rs-cross mr-10"></i>Broccoli</a>
                                </li>
                                <li class="hover-up">
                                    <a href="#"><i class="fi-rs-cross mr-10"></i>Artichoke</a>
                                </li>
                                <li class="hover-up">
                                    <a href="#"><i class="fi-rs-cross mr-10"></i>Celery</a>
                                </li>
                                <li class="hover-up mr-0">
                                    <a href="#"><i class="fi-rs-cross mr-10"></i>Spinach</a>
                                </li>
                            </ul>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="container mb-30">
            <div class="row">
                <div class="col-12">
                    
                    <products-List :category_id="@if(isset($category)){{$category->id}}@else{{0}}@endif" :seller_id="@if(isset($seller_id)){{$seller_id}}@else{{0}}@endif"></products-List>

                    <!--End Deals-->
                </div>
            </div>
        </div>
    </main>
    @endsection
  
   @section('page-scripts')
 
    @endsection
