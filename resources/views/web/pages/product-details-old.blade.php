@extends('web.layouts.web')
   
@section('main')
<main class="main">
        <div class="page-header breadcrumb-wrap">
            <div class="container">
                <div class="breadcrumb">
                    <a href="/" rel="nofollow"><i class="fi-rs-home mr-5"></i>Home</a>
                    <span></span> <a href="/shop">Vegetables & tubers</a> <span></span> Seeds of Change Organic
                </div>
            </div>
        </div>
        <div class="container mb-30">
            <div class="row">
                <div class="col-xl-10 col-lg-12 m-auto">
                    <div class="product-detail accordion-detail">
                        <product-details :product_id="{{$product->id}}"></product-details>
                        <div class="row mt-60">
                            <div class="col-12">
                                <h2 class="section-title style-1 mb-30">Related products</h2>
                            </div>
                            {{-- <related-products-list :product_id="{{$product->id}}" :category_id="{{$product->category_id}}"></related-products-list> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    @endsection
  
   @section('page-scripts')
 
    @endsection
