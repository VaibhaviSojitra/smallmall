<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8" />
    <title>Small Mall - Marketplace on your click</title>
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta property="og:title" content="" />
    <meta property="og:type" content="" />
    <meta property="og:url" content="" />
    <meta property="og:image" content="" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="/web-assets/imgs/theme/favicon.svg" />
    <!-- Template CSS -->
    {{-- <link rel="stylesheet" href="/web-assets/css/plugins/animate.min.css" /> --}}
    <link rel="stylesheet" href="/web-assets/css/main.css?v=5.6" />
    <link rel="stylesheet" href="/web-assets/css/custom.css" />
    @vite(['resources/js/app.js', 'resources/sass/main.scss'])

</head>

<body>


    <div class="invoice invoice-content invoice-3" style="min-width: 600px;">

        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="invoice-inner">
                        <div class="invoice-info" id="invoice_wrapper">
                            <div class="invoice-header">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="invoice-name">
                                            <div class="logo">
                                                <a href=""><img src="/web-assets/imgs/theme/logo.webp" style="max-width: 250px" alt="logo" crossorigin="anonymous"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6  text-end">
                                        <div class="invoice-numb">
                                            <h4 class="invoice-header-1 mb-10 mt-20">Order No: <span
                                                    class="text-heading">{{ $order->order_id }}</span></h4>
                                            <h6>Invoice Date: <span
                                                    class="text-heading">{{ \Carbon\Carbon::parse($order->ordered_at)->format('d F, Y') }}</span>
                                            </h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="invoice-top">
                                <div class="row">
                                    <div class="col-lg-4 col-md-6">
                                        <div class="invoice-number">
                                            <h4 class="invoice-title-1 mb-10">Invoice To</h4>
                                            <p class="invoice-addr-1">
                                                <strong>{{ ucfirst($order->order->billing_address->full_name) }}</strong> <br>
                                                {{$order->order->billing_address->add_1}},{{$order->order->billing_address->add_2}}<br>
                                                {{$order->order->billing_address->city}}, {{$order->order->billing_address->state}}, {{$order->order->billing_address->zip}}<br>
                                                <abbr title="Phone">Phone:</abbr> {{ $order->order->billing_address->mobile }}<br>
                                                <abbr title="Email">Email: </abbr> {{ $order->user->email }}<br>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6">
                                        <div class="invoice-number">
                                            <h4 class="invoice-title-1 mb-10">Ship To</h4>
                                            <p class="invoice-addr-1">
                                            <strong>{{ ucfirst($order->order->shipping_address->full_name) }}</strong> <br>
                                                {{$order->order->shipping_address->add_1}},{{$order->order->shipping_address->add_2}}<br>
                                                {{$order->order->shipping_address->city}}, {{$order->order->shipping_address->state}}, {{$order->order->shipping_address->zip}}<br>
                                                <abbr title="Phone">Phone:</abbr> {{ $order->order->shipping_address->mobile }}<br>
                                                <abbr title="Email">Email: </abbr> {{ $order->user->email }}<br>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6">
                                        <div class="invoice-number">
                                            <h4 class="invoice-title-1 mb-10">Seller</h4>
                                            <p class="invoice-addr-1">
                                                <strong>Name:</strong>
                                                {{ ucfirst($order->product->seller->name) }}<br>
                                                <strong>Email:</strong>
                                                {{ ucfirst($order->product->seller->email) }}<br>
                                                <strong>Phone:</strong>
                                                {{ ucfirst($order->product->seller->mobile) }}<br>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="invoice-center" style="page-break-before: always;">
                                <div class="table-responsive">
                                    <table class="table-striped invoice-table">
                                        <thead class="bg-active">
                                            <tr>
                                                <th>Item Name</th>
                                                <th class="text-center">Unit Price</th>
                                                <th class="text-right">Status</th>
                                                <th class="text-center">Quantity</th>
                                                <th class="text-right">Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $sum = 0;
                                            @endphp
                                            @php
                                                $sum = $sum + $order->product->selling_price * $order->quantity;
                                            @endphp
                                            <tr>
                                                <td>
                                                    <div class="item-desc-1">
                                                        <span>{{ $order->product->title }}</span>
                                                    </div>
                                                </td>
                                                <td class="text-center"> ₹{{ number_format($order->product->selling_price) }}</td>
                                                <td class="text-right">{{ $order->status_name }}</td>
                                                <td class="text-center">{{ $order->quantity }}</td>
                                                <td class="text-right">
                                                    ₹{{ number_format($order->product->selling_price * $order->quantity) }}
                                                </td>
                                            </tr>

                                            <tr>
                                                <td colspan="4" class="text-end f-w-600">SubTotal</td>
                                                <td class="text-right">₹{{ number_format($sum) }}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" class="text-end f-w-600">Delivery Charges</td>
                                                <td class="text-right">₹{{ number_format($order->shipping_charge) }}</td>
                                            </tr>
                                            @if ( $order->coupon_discount )
                                                <tr>
                                                    <td colspan="4" class="text-end f-w-600">Coupon Discount</td>
                                                    <td class="text-right">₹{{ number_format($order->coupon_discount) }}</td>
                                                </tr>
                                            @endif
                                            <tr>
                                                <td colspan="4" class="text-end f-w-600">Grand Total</td>
                                                <td class="text-right f-w-600">₹ {{ number_format( ($sum + $order->shipping_charge) - $order->coupon_discount) }} </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="invoice-bottom">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div>
                                            <h3 class="invoice-title-1">Important Note</h3>
                                            <ul class="important-notes-list-1">
                                                <li>All amounts shown on this invoice are in Indian currency</li>
                                                <li>finance charge of 1.5% will be made on unpaid balances after 30
                                                    days.</li>
                                                <li>Once order done, money can't refund</li>
                                                <li>Delivery might delay due to some external dependency</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-offsite">
                                        <div class="text-end">
                                            <p class="mb-0 text-13">Thank you for your business</p>
                                            <p><strong>Small Mall</strong></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="invoice-btn-section clearfix d-print-none">
                            <a href="javascript:window.print()" class="btn btn-lg btn-custom btn-print hover-up"> <img
                                    src="/web-assets/imgs/theme/icons/icon-print.svg" alt=""> Print </a>
                            <a id="invoice_download_btn" class="btn btn-lg btn-custom btn-download hover-up"> <img
                                    src="/web-assets/imgs/theme/icons/icon-download.svg" alt=""> Download </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('web.layouts.common-scripts')

    <script src="/web-assets/js/invoice/html2canvas.js">
</script>
<script src="/web-assets/js/invoice/jspdf.min.js"></script>

    <script src="/web-assets/js/invoice/invoice.js"></script>
</body>

</html>



















