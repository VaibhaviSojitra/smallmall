<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Admin Panel: {{ config('app.name') }}</title>
        <meta http-equiv="x-ua-compatible" content="ie=edge" />
        <meta name="description" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta property="og:title" content="" />
        <meta property="og:type" content="" />
        <meta property="og:url" content="" />
        <meta property="og:image" content="" />
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="/admin-assets/imgs/theme/favicon.svg" />
        <!-- Template CSS -->
        <link href="/admin-assets/css/main.css?v=1.1" rel="stylesheet" type="text/css" />
        @stack('css')
    </head>

    <body>
        <div class="screen-overlay"></div>
        <div id="app">
            @includeWhen(auth()->check(), 'layouts.admin.common.sidemenu')
            <main @class([
                    'main-wrap' => auth()->check()
                ])>
            @includeWhen(auth()->check(), 'layouts.admin.common.header')
                <section class="content-main">
                    @yield('page-content')
                    <!-- card end// -->
                </section>
                <footer class="main-footer font-xs">
                    <div class="row pb-30 pt-15">
                        <div class="col-sm-6">
                            {{ now()->format('Y') }}
                            &copy; Small Mall.
                        </div>
                        <div class="col-sm-6">
                            <div class="text-sm-end">All rights reserved</div>
                        </div>
                    </div>
                </footer>
            </main>
        </div>
        <script src="/admin-assets/js/vendors/jquery-3.6.0.min.js"></script>
        <script src="/admin-assets/js/vendors/bootstrap.bundle.min.js"></script>
        <script src="/admin-assets/js/vendors/select2.min.js"></script>
        <script src="/admin-assets/js/vendors/perfect-scrollbar.js"></script>
        <script src="/admin-assets/js/vendors/jquery.fullscreen.min.js"></script>
        <script src="/admin-assets/js/vendors/chart.js"></script>
        <!-- Main Script -->
        <script src="/admin-assets/js/main.js?v=1.1" type="text/javascript"></script>
        {{-- <script src="/admin-assets/js/custom-chart.js" type="text/javascript"></script> --}}
        @vite(['resources/js/admin.js'])
        @stack('scripts')

    </body>
</html>
