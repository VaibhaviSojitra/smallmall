<aside class="navbar-aside" id="offcanvas_aside">
    <div class="aside-top">
        <a href="{{url('/admin/dashboard')}}" class="brand-wrap">
            <img src="/web-assets/imgs/theme/logo.webp" class="logo" alt="SmallMall Dashboard" />
        </a>
        <div>
            <button class="btn btn-icon btn-aside-minimize"><i class="text-muted material-icons md-menu_open"></i></button>
        </div>
    </div>
    <nav>
        <ul class="menu-aside">

            @if ( auth()->user()->hasRole('admin') || auth()->user()->hasRole('seller') )
                <li class="{{ (request()->is('admin/dashboard*')) ? 'active' : '' }} menu-item" >
                    <a class="menu-link" href="/admin/dashboard">
                        <i class="icon material-icons md-home"></i>
                        <span class="text">Dashboard</span>
                    </a>
                </li>
            @endif

            @if (  auth()->user()->hasRole('admin') || auth()->user()->hasRole('seller')  )
                <li class="{{ (request()->is('admin/products*')) ? 'active' : '' }} menu-item" >
                    <a class="menu-link" href="/admin/products">
                        <i class="icon material-icons md-shopping_bag"></i>
                        <span class="text">Products</span>
                    </a>
                </li>
            @endif

            @if (auth()->user()->hasRole('admin'))
                <li class="{{ (request()->is('admin/vendors*')) ? 'active' : '' }} menu-item">
                    <a class="menu-link" href="/admin/vendors">
                        <i class="icon material-icons md-person"></i>
                        <span class="text">Vendors</span>
                    </a>
                </li>
            @endif

            @if ( auth()->user()->hasRole('admin') || auth()->user()->hasRole('seller') )
                <li class="{{ (request()->is('admin/orders*')) ? 'active' : '' }} menu-item">
                    <a class="menu-link" href="/admin/orders">
                        <i class="icon material-icons md-shopping_cart"></i>
                        <span class="text">Orders</span>
                    </a>
                </li>
            @endif

            @if (auth()->user()->hasRole('seller')  )
                <li class="{{ (request()->is('admin/questionnaire*')) ? 'active' : '' }} menu-item" >
                    <a class="menu-link" href="/admin/questionnaire">
                        <i class="icon material-icons md-help_outline"></i>
                        <span class="text">Questionnaire</span>
                    </a>
                </li>
            @endif

            @if ( auth()->user()->hasRole('seller') )

                <li class="{{ (request()->is('admin/subscription*')) ? 'active' : '' }} menu-item">
                    <a class="menu-link" href="/admin/subscription">
                        <i class="icon material-icons md-list_alt"></i>
                        <span class="text">Subscription</span>
                    </a>
                </li>
            @endif

            @if (auth()->user()->hasRole('admin'))
                <li class="{{ (request()->is('admin/users*')) ? 'active' : '' }} menu-item">
                    <a class="menu-link" href="/admin/users">
                        <i class="icon material-icons md-group"></i>
                        <span class="text">Users</span>
                    </a>
                </li>

                <li class="{{ (request()->is('admin/vendor/questionnaire*')) ? 'active' : '' }} menu-item" >
                    <a class="menu-link" href="/admin/vendor/questionnaire">
                        <i class="icon material-icons md-help_outline"></i>
                        <span class="text">Vendor Questionnaire</span>
                    </a>
                </li>

                <li class="{{ (request()->is('admin/vendor/approved-questionnaire*')) ? 'active' : '' }} menu-item" >
                    <a class="menu-link" href="/admin/vendor/approved-questionnaire">
                        <i class="icon material-icons md-help_outline"></i>
                        <span class="text">Approved Vendors</span>
                    </a>
                </li>
            @endif

            @if (auth()->user()->hasRole('admin'))
                <li class="{{ (request()->is('admin/top-products*')) ? 'active' : '' }} menu-item">
                    <a class="menu-link" href="/admin/top-products">
                        <i class="icon material-icons md-pie_chart"></i>
                        <span class="text">Top Products</span>
                    </a>
                </li>
            @endif

            @if (auth()->user()->hasRole('seller'))
                <li class="{{ (request()->is('admin/brand-subscribers*')) ? 'active' : '' }} menu-item">
                    <a class="menu-link" href="/admin/brand-subscribers">
                        <i class="icon material-icons md-monetization_on"></i>
                        <span class="text">Subscribers</span>
                    </a>
                </li>
            @endif
        </ul>
        <hr />
        <ul class="menu-aside">
            @if (auth()->user()->hasRole('admin'))
                <li class="{{ (request()->is('admin/category')) ? 'active' : '' }} menu-item" >
                    <a class="menu-link" href="/admin/category">
                        <i class="icon material-icons md-list_alt"></i>
                        <span class="text">Categories</span>
                    </a>
                </li>
            @endif
        </ul>
        <br />
        <br />
    </nav>
</aside>
