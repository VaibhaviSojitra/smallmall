import { defineStore } from 'pinia'

export const useUserStore = defineStore({
    id: 'user',
    state: () => ({
        users: [],
        meta: {},
        searchQuery: '',
    }),

    actions: {
        fetchUsers(params) {
            return new Promise((resolve, reject) => {
                axios.get("/api/admin/users", { params }).then((response) => {
                    this.users = response.data.data
                    this.meta = response.data.meta
                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        },

        blockUser(id) {
            return new Promise((resolve, reject) => {
                axios.post(`/api/admin/users/${id}/block`).then((response) => {
                    let index = this.users.findIndex((_c) => _c.id === id)
                    if(index > -1)
                        Object.assign(this.users[index], response.data.data)

                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        },

        unblockUser(id) {
            return new Promise((resolve, reject) => {
                axios.post(`/api/admin/users/${id}/unblock`).then((response) => {
                    let index = this.users.findIndex((_c) => _c.id === id)
                    if(index > -1) Object.assign(this.users[index], response.data.data)

                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        },


        fetchSubscribedUsers(params) {
            return new Promise((resolve, reject) => {
                axios.get("/api/admin/subscribed-users", { params }).then((response) => {
                    this.users = response.data.data
                    this.meta = response.data.meta
                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        },
    },
})
