import { defineStore } from 'pinia'
import productStub from '@/js/stubs/productStub'

export const useOrderStore = defineStore({
    id: 'product',
    state: () => ({
        orders: [],
        productImages: [],
        meta: {},
        orderStatus: '',
        searchQuery: '',
        currentProduct: {
            ...productStub()
        },
        isEdit: false,
    }),

    actions: {
        resetProduct() {
            this.currentProduct = {
                ...productStub()
            }
        },

        fetchOrders(params) {
            return new Promise((resolve, reject) => {
                axios.get("/api/admin/orders", { params }).then((response) => {
                    // console.log(response.data.data);
                    this.orders = response.data.data
                    this.meta = response.data.meta
                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        },

        fetchOrder(id) {
            return new Promise((resolve, reject) => {
                axios.get(`/api/admin/orders/${id}`).then((response) => {
                    this.currentProduct = response.data.data
                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        },

        changeOrderStatus(params) {

            return new Promise((resolve, reject) => {
                // console.log(params);
                axios.post("/api/admin/orders/change_status", {params})
                .then((response) => {
                    // this.productImages = response.data.data
                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        },

    },
})
