import { defineStore } from 'pinia'
import profileStub from '@/js/stubs/profileStub'

export const useProfileStore = defineStore({
    id: 'product',
    state: () => ({
        profile: [],
        productImages: [],
        currentProfile: {
            ...profileStub()
        }
    }),

    actions: {
        resetProfile() {
            this.currentProfile = {
                ...profileStub()
            }
        },

        fetchOrders(params) {
            return new Promise((resolve, reject) => {
                axios.get("/api/admin/orders", { params }).then((response) => {
                    // console.log(response.data.data);
                    this.orders = response.data.data
                    this.meta = response.data.meta
                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        },

        submitForm(params) {
            return new Promise((resolve, reject) => {
                axios.put(`/api/admin/profile`).then((response) => {
                    this.currentProfile = response.data.data
                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        },

        changeOrderStatus(params) {

            return new Promise((resolve, reject) => {
                // console.log(params);
                axios.post("/api/admin/orders/change_status", {params})
                .then((response) => {
                    // this.productImages = response.data.data
                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        },

    },
})
