import { defineStore } from 'pinia'

export const useVendorsStore = defineStore({
    id: 'vendor',
    state: () => ({
        vendor: [],
        paginationData: {
            total: 0,
            per_page: 2,
            from: 1,
            to: 0,
            current_page: 1
        },
        offset: 4,
        totalVendors: 0,
    }),

    actions: {

        fetchVendors() {
            return new Promise((resolve, reject) => {
                axios.get("/api/seller?page="+this.paginationData.current_page).then((response) => {
                    this.vendor = response.data.data;
                    this.paginationData = response.data.meta;
                    this.totalVendors = response.data.data.length;
                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            })

        },


        handleSubscription(seller_id) {

            return new Promise((resolve, reject) => {
                axios.get(`/api/seller/${seller_id}/toggle-subscription`).then((response) => {
                    this.vendor = response.data.data;
                    this.paginationData = response.data.meta;
                    this.totalVendors = response.data.data.length;

                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            })

        },
    },
})
