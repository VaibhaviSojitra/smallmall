import { defineStore } from 'pinia'

export const useCartStore = defineStore({
    id: 'cart',
    state: () => ({
        carts: [],
        totalAmount: 0,
        selected: [],
        allSelected: false,
        cartIds: []
    }),

    actions: {

        fetchCart() {
            return new Promise((resolve, reject) => {
                axios.post("/api/cart").then((response) => {
                    this.carts = response.data.data.cart.data;
                    this.totalAmount = response.data.data.total_amount;
                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        },

        removeItemFromCart(itemId) {
            const body = { data: [{ id: itemId }] }
            return new Promise((resolve, reject) => {
                axios.post('/api/cart/delete', body).then((response) => {
                    this.fetchCart();
                    this.updateTotalAmount()
                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            }) 
        },

        updateTotalAmount() {
            this.totalAmount = 0;
            this.carts.forEach((item) => {
                this.totalAmount += (item.product.selling_price *  item.quantity);
            });
        },

        updateItemToCart() {
            let data=[];
            this.carts.forEach((item) => {
                data.push({
                    id: item.id,
                    quantity: item.quantity,
                })
            });
            const body = { data: data }

            return new Promise((resolve, reject) => {
                axios.post('/api/cart/update', body).then((response) => {
                    this.updateTotalAmount()
                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            }) 
        },

        removeAllItemFromCart() {
            let data=[];
            this.carts.forEach((item) => {
                data.push({
                    id: item.id,
                })
            });
            const body = { data: data }
            return new Promise((resolve, reject) => {
                axios.post('/api/cart/delete', body).then((response) => {
                    this.fetchCart();
                    this.updateTotalAmount()
                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            }) 
        },
    },
})
