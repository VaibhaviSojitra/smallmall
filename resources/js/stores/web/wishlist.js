import { defineStore } from 'pinia'

export const useWishlistStore = defineStore({
    id: 'wishlist',
    state: () => ({
        wishlists: [],
        totalAmount: 0,
    }),

    actions: {

        fetchWishlist() {
            return new Promise((resolve, reject) => {
                axios.post("/api/wishlist").then((response) => {
                    this.wishlists = response.data.data;
                    this.totalAmount = response.data.data.length;
                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        },

        updateTotalAmount() {
            this.totalAmount = 0;
            this.wishlists.forEach((item) => {
                this.totalAmount += (item.product.selling_price *  item.quantity);
            });
        },
    },
})
