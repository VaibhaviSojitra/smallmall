import { defineStore } from 'pinia'

export const useProductsStore = defineStore({
    id: 'products',
    state: () => ({
        products: [],
        paginationData: {
            total: 0,
            per_page: 2,
            from: 1,
            to: 0,
            current_page: 1
        },
        offset: 4,
        totalProducts: 0,
        is_show:false,
        is_limit_show:false,
        sort_name:50,
        type_name:"All",
        active_sort_1:0,
        active_sort_2:0,
        category_idd:'',
        search_keyword:'',
        seller_id:''
    }),

    actions: {

        fetchProducts(category_id,seller_id) {
            if(category_id){
                const body = { user_id: this.user_id, sort_by:this.active_sort_2 }
                return new Promise((resolve, reject) => {
                    axios.post("/api/category/"+category_id+"/products",body).then((response) => {
                        this.products = response.data.data;
                        this.paginationData = response.data.meta;
                        this.totalProducts = response.data.data.length;
                        resolve(response)
                    })
                    .catch((err) => {
                        reject(err)
                    })
                })
            }else if(seller_id){
                const body = { user_id: this.user_id, sort_by:this.active_sort_2  }
                return new Promise((resolve, reject) => {
                    axios.post("/api/seller/"+seller_id+"/products",body).then((response) => {
                        this.products = response.data.data;
                        this.paginationData = response.data.meta;
                        this.totalProducts = response.data.data.length;
                        resolve(response)
                    })
                    .catch((err) => {
                        reject(err)
                    })
                })
            }else{
                const urlParams = new URLSearchParams(window.location.search);
                const category_slug = urlParams.get('category');
                if(category_slug){
                    const response1 = axios.get("/api/category/"+category_slug+"/slug");
                    if (response1.data.success) {
                        this.category_idd = response1.data.data.id;
                    } 
                }
            
                this.search_keyword = urlParams.get('search');
                if(this.category_idd || this.search_keyword){
                    const body = { user_id: this.user_id, sort_by:this.active_sort_2 , search_keyword:this.search_keyword, category_id: this.category_idd}
                    return new Promise((resolve, reject) => {
                        axios.post("/api/products/search/list?page="+this.paginationData.current_page,body).then((response) => {
                            this.products = response.data.data;
                            this.paginationData = response.data.meta;
                            this.totalProducts = response.data.data.length;
                            resolve(response)
                        })
                        .catch((err) => {
                            reject(err)
                        })
                    })
                }else{
                    const body = { user_id: this.user_id }

                    return new Promise((resolve, reject) => {
                        axios.post("/api/products?page="+this.paginationData.current_page,body).then((response) => {
                            this.products = response.data.data;
                            this.paginationData = response.data.meta;
                            this.totalProducts = response.data.data.length;
                            resolve(response)
                        })
                        .catch((err) => {
                            reject(err)
                        })
                    })
                }
            }
            
        },

      

    },
})
