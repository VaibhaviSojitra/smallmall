import { defineStore } from 'pinia'

export const useSubscriberStore = defineStore({
    id: 'subscriber',
    state: () => ({
        subscriber: [],
        meta: {},
        searchQuery: '',
    }),

    actions: {
        fetchSubscriber(params) {
            return new Promise((resolve, reject) => {
                axios.get("/api/admin/vendors/subscriber", { params }).then((response) => {
                    console.log(response)
                    this.subscriber = response.data.data
                    this.meta = response.data.meta
                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        }
        
    },
})
