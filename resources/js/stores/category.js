import { defineStore } from 'pinia'

export const useCategoryStore = defineStore({
    id: 'category',
    state: () => ({
        categories: [],
        searchQuery: '',
        currentCategory: {
            id: '',
            name: '',
            icon: ''
        },
        isEdit: false,
    }),

    actions: {
        resetCategory() {
            this.currentCategory = {
                id: '',
                name: '',
                icon: ''
            }
        },

        fetchCategories() {
            return new Promise((resolve, reject) => {
                axios.get("/api/category").then((response) => {
                    this.categories = response.data.data
                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        },

        addCategory(formData) {
            return new Promise((resolve, reject) => {
                axios.post('/api/admin/categories', formData).then((response) => {
                    this.categories.push(response.data.data)
                    this.resetCategory()
                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        },

        updateCategory(id, formData) {
            return new Promise((resolve, reject) => {
                axios.put(`/api/admin/categories/${id}`, formData).then((response) => {
                    this.categories.push(response.data.data)
                    this.resetCategory()
                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        },

        deleteCategory(id) {
            return new Promise((resolve, reject) => {
                axios.delete(`/api/admin/categories/${id}`).then((response) => {
                    let index = this.categories.findIndex((_c) => _c.id === id)
                    if(index > -1) this.categories.splice(index, 1)

                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        },
    },
})
