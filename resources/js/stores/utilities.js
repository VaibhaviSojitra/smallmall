import { defineStore } from 'pinia'
import isArray from 'lodash/isArray'


export const useUtilitiesStore = defineStore({
    id: 'utilities',

    actions: {
        toFormData(object) {
            const formData = new FormData()

            Object.keys(object).forEach((key) => {
              if (object[key] && object[key].length && object[key][0] instanceof File) {
                for (let index = 0; index < object[key].length; index++)
                  formData.append(`${key}[]`, object[key][index])
              }
              else if (isArray(object[key])) {
                formData.append(key, JSON.stringify(object[key]))
              }
              else {
                // Convert null to empty strings (because formData does not support null values and converts it to string)
                if (object[key] === null)
                  object[key] = ''

                formData.append(key, object[key])
              }
            })

            return formData
          },
      },
})
