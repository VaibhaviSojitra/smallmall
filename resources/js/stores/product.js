import { defineStore } from 'pinia'
import productStub from '@/js/stubs/productStub'

export const useProductStore = defineStore({
    id: 'product',
    state: () => ({
        products: [],
        productImages: [],
        meta: {},
        searchQuery: '',
        currentProduct: {
            ...productStub()
        },
        isEdit: false,
    }),

    actions: {
        resetProduct() {
            this.currentProduct = {
                ...productStub()
            }
        },

        fetchProducts(params) {
            return new Promise((resolve, reject) => {
                axios.get("/api/admin/products", { params }).then((response) => {
                    this.products = response.data.data
                    this.meta = response.data.meta
                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        },

        fetchVendorProducts(params) {
            return new Promise((resolve, reject) => {
                axios.get("/api/admin/vendors", { params }).then((response) => {
                    this.products = response.data.data
                    this.meta = response.data.links
                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        },

        fetchProduct(id) {
            return new Promise((resolve, reject) => {
                axios.get(`/api/admin/products/${id}`).then((response) => {
                    this.currentProduct = response.data.data
                    // this.products = response.data.data
                    // this.meta = response.data.meta
                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        },

        fetchProductImages(id) {
            return new Promise((resolve, reject) => {
                axios.get(`/api/admin/products/${id}/medias`).then((response) => {
                    this.productImages = response.data.data
                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        },

        uploadImages(id, formData) {
            return new Promise((resolve, reject) => {
                axios.post(`/api/admin/products/${id}/medias`, formData).then((response) => {
                    this.productImages.push({...response.data.data})
                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        },

        deleteProductImage(p_id, m_id) {
            return new Promise((resolve, reject) => {
                axios.delete(`/api/admin/products/${p_id}/medias/${m_id}`).then((response) => {
                    let index = this.productImages.findIndex((_p) => _p.id === m_id)
                    if(index > -1)
                        this.productImages.splice(index, 1)
                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        },

        addProduct(formData) {
            return new Promise((resolve, reject) => {
                axios.post('/api/admin/products', formData).then((response) => {
                    this.products.push(response.data.data)
                    this.resetProduct()
                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        },

        updateProduct(id, formData) {
            return new Promise((resolve, reject) => {
                axios.put(`/api/admin/products/${id}`, formData).then((response) => {
                    // this.products.push(response.data.data)
                    this.resetProduct()
                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        },

        // deleteProduct(id) {
        //     return new Promise((resolve, reject) => {
        //         axios.delete(`/api/admin/products/${id}`).then((response) => {
        //             let index = this.products.findIndex((_c) => _c.id === id)
        //             if(index > -1) this.products.splice(index, 1)

        //             resolve(response)
        //         })
        //         .catch((err) => {
        //             reject(err)
        //         })
        //     })
        // },

        approveProduct(id) {
            return new Promise((resolve, reject) => {
                axios.put(`/api/admin/products/${id}/approve`).then((response) => {
                    let index = this.products.findIndex((_p) => _p.id === response.data.data.id)
                    if(index > -1)
                        Object.assign(this.products[index], response.data.data)

                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        },

        rejectProduct(id) {
            return new Promise((resolve, reject) => {
                axios.put(`/api/admin/products/${id}/reject`).then((response) => {
                    let index = this.products.findIndex((_p) => _p.id === response.data.data.id)
                    if(index > -1)
                        Object.assign(this.products[index], response.data.data)

                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        },

        deleteProduct(id) {
            return new Promise((resolve, reject) => {
                axios.put(`/api/admin/products/${id}/delete`).then((response) => {
                    let index = this.products.findIndex((_p) => _p.id === id)
                    if(index > -1)
                        this.products.splice(index, 1)

                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        },

        changePopular(id) {
            return new Promise((resolve, reject) => {
                axios.put(`/api/admin/products/${id}/popular`).then((response) => {
                    let index = this.products.findIndex((_p) => _p.id === response.data.data.id)
                    if(index > -1)
                        Object.assign(this.products[index], response.data.data)

                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        },

        changeFeatured(id) {
            return new Promise((resolve, reject) => {
                axios.put(`/api/admin/products/${id}/featured`).then((response) => {
                    let index = this.products.findIndex((_p) => _p.id === response.data.data.id)
                    if(index > -1)
                        Object.assign(this.products[index], response.data.data)

                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        },

        fetchTopProducts(params) {
            return new Promise((resolve, reject) => {
                axios.get("/api/admin/top-products", { params }).then((response) => {
                    this.products = response.data.data
                    // this.meta = response.data.meta
                    console.log(this.products);
                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        },
    },
})
