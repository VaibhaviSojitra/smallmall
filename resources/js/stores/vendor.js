import { defineStore } from 'pinia'
import vendorQusStub from '@/js/stubs/vendorQusStub'

export const useVendorStore = defineStore({
    id: 'vendor',
    state: () => ({
        questionnaire: [],
        currentQuestionnaire:[],
        meta: {},
        searchQuery: '',
        // currentQuestionnaire: {
        //     ...vendorQusStub()
        // },
        max_date:  new Date().toISOString().split('T')[0]
    }),

    actions: {
        resetVendor() {
            this.currentQuestionnaire = {
                ...vendorQusStub()
            }
        },

        fetchQuestionnaire() {
            return new Promise((resolve, reject) => {
                axios.get("/api/admin/vendor/questionnaire").then((response) => {
                    this.currentQuestionnaire = response.data.data
                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        },

        fetchVendorsQuestionnaire() {
            return new Promise((resolve, reject) => {
                axios.get("/api/admin/vendor/all/questionnaire").then((response) => {
                    this.questionnaire = response.data.data
                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        },

        fetchApprovedVendorsQuestionnaire() {
            return new Promise((resolve, reject) => {
                axios.get("/api/admin/vendor/all/questionnaire").then((response) => {
                    this.questionnaire = response.data.data
                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        },

        updateQuestionnaire(formData) {
            console.log(formData);

            return new Promise((resolve, reject) => {
                axios.post('/api/admin/vendor/add-questionnaire', formData).then((response) => {
                    this.currentQuestionnaire = response.data.data;
                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        },
        changeQuestionnaireStatus(params) {

            return new Promise((resolve, reject) => {
                // console.log(params);
                axios.post("/api/admin/vendor/questionnaire/change_status", {params})
                .then((response) => {
                    // this.productImages = response.data.data
                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        },

    },
})
