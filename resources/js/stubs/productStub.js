export default function () {
    return {
        title: '',
        subtitle: '',
        category_id: '',
        used_material: '',
        manufactured_in: '',
        manufactured_by: '',
        sustainability_statement: '',
        description: '',
        short_description: '',
        specification: [{key: '', value: ''}],
        media: []
    }
}
