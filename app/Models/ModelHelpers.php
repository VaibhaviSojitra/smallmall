<?php

namespace App\Models;

trait ModelHelpers {
    public static function getFillables() {
        return (new static())->getFillable();
    }

    public static function getInstance($data)
    {
        if (gettype($data) === "string" || gettype($data) === "integer") {
            $data = self::findOrFail($data);
        }
        return $data;
    }
}