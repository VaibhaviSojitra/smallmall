<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Razorpay\Api\Subscription;
use Carbon\Carbon;

class UserSubscription extends Model
{
    const PENDING = 0;
    const SUCCESS = 1; // This should also be used for ACTIVE status
    const FAILED = 2;
    const COMPLETED = 3;
    const HALTED = 4;
    const PAUSED = 5;
    const CANCELLED = 6;
    const SCHEDULED_FOR_CANCELLATION = 7;

    protected $fillable = [
        'user_id',
        'plan_id',
        'pg_plan_id',
        'provider',
        'subscription_id',
        'status'
    ];

    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d h:i:s');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d h:i:s');
    }

    public function plan()
    {
        return $this->belongsTo(SubscriptionPlan::class, "plan_id");
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
