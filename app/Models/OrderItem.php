<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class OrderItem extends Model
{
    use HasFactory;

    protected $appends = ['status_name'];

    const ORDERED = 0;
    const CONFIRMED = 1;
    const PROCESSING = 2;
    const SHIPPED = 3;
    const ON_HOLD = 4;
    const COMPLETED = 5;
    const CANCELLED = 6;

    public static function statusToText($number) {
        switch ($number) {
            case static::ORDERED:
                return 'Ordered';
            case static::CONFIRMED:
                return 'Confirmed';
            case static::PROCESSING:
                return 'Processing';
            case static::SHIPPED:
                return 'Shipped';
            case static::ON_HOLD:
                return 'On Hold';
            case static::COMPLETED:
                return 'Completed';
            case static::CANCELLED:
                return 'Cancelled';
        }
    }

    public function getStatusNameAttribute()
    {
        switch ($this->status) {
            case 0:
                return 'Ordered';
            case 1:
                return 'Confirmed';
            case 2:
                return 'Processing';
            case 3:
                return 'Shipped';
            case 4:
                return 'On Hold';
            case 5:
                return 'Completed';
            case 6:
                return 'Cancelled';
        }
    }

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class,'order_id','order_id');
    }
}
