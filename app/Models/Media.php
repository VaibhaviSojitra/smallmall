<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Storage;

class Media extends Model
{
    use HasFactory;

    protected $fillable = ["type", "path", "ownerable_type", "ownerable_id"];

    protected $appends = ['full_url'];
    
    public function ownerable()
    {
        return $this->morphTo();
    }

    /**
     * Get the user's first name.
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    protected function fullUrl(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => Storage::disk('product')->url($this->path),
        );
    }
}
