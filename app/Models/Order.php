<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;


class Order extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $appends = ['payment_type'];

    public static function generateRandomOrderId($field_code='order_id')
	{
        $random_unique  =  'SM'.mt_rand(1000000000, 9999999999);

        $order = Order::where('order_id', '=', $random_unique)->first();
        if ($order != null) {
            self::generateRandomOrderId();
        }
        return $random_unique;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function order_items()
    {
        
        if(auth()->check()){
            if(auth()->user()->hasRole('seller')){
                return $this->hasMany(OrderItem::class,'order_id','order_id')->whereHas('product', function($q) {
                    $q->where('seller_id', auth()->id());
                });
            }
        }
        return $this->hasMany(OrderItem::class,'order_id','order_id');

    }

    public function sellerOrderItems(){
        return $this->order_items()->whereHas('product', function($q) {
            $q->where('seller_id', auth()->id());
        });
    }

    public function billing_address()
    {
        return $this->belongsTo(Address::class,'billing_address_id','id');
    }

    public function shipping_address()
    {
        return $this->belongsTo(Address::class,'shipping_address_id','id');
    }

    protected function paymentType(): Attribute
    {
        return Attribute::make(
            get: function ($value) {
                if($value == 1)
                {
                    return 'COD';
                }
                if($value == 2)
                {
                    return 'Card Payment';
                }
                if($value == 3)
                {
                    return 'Paypal';
                }
            }
        );
    }
}
