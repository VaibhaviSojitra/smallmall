<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory, ModelHelpers;

    protected $fillable = [
        'title', 'subtitle', 'slug', 'selling_price', 'display_price', 'short_description', 'description', 'specification', 'category_id', 'seller_id',  'used_material', 'manufactured_in', 'manufactured_by','sustainability_statement'
    ];

    protected $casts = [
        'specification' => 'json',
        'is_approved' => 'boolean'
    ];

    protected $appends = ['average_rating'];

    public function medias()
    {
        return $this->morphMany(Media::class, 'ownerable');
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function reviews()
    {
        return $this->hasMany(ProductReview::class);
    }

    public function seller()
    {
        return $this->belongsTo(User::class, 'seller_id');
    }

    public function scopeActiveSellerProducts($query)
    {
        return $query->whereHas('seller', fn ($q) => $q->where('status', 'UNBLOCK')->orWhere('status', null) );
    }

    public static function commonProductsData($products,$user_id="",$session_id="")
    {
        foreach($products as $key => $value){
            $products[$key]->cart_id = "";
            $products[$key]->is_cart = false;
            $products[$key]->quantity = 1;
            $CartData = $WishlistData = [];
            if($user_id){
                $CartData =  Cart::where('user_id',$user_id)->where('product_id',$value->id)->first();
            }else{
                if($session_id){
                    $CartData =  Cart::where('session_id',$session_id)->where('product_id',$value->id)->first();
                }
            }
            if($CartData){
                $products[$key]->cart_id = $CartData['id'];
                $products[$key]->is_cart = true;
                $products[$key]->quantity = $CartData['quantity'];
            }
            $products[$key]->wishlist_id = "";
            $products[$key]->is_wishlist = false;
            if($user_id){
                $WishlistData =  Wishlist::where('user_id',$user_id)->where('product_id',$value->id)->first();
            }else{
                if($session_id){
                    $WishlistData =  Wishlist::where('session_id',$session_id)->where('product_id',$value->id)->first();                }
            }
            if($WishlistData){
                $products[$key]->wishlist_id = $WishlistData['id'];
                $products[$key]->is_wishlist = true;
            }
            $products[$key]->avg_rating = 0;
            if ($value->reviews()->count() > 0 ){
                $products[$key]->avg_rating = $value->reviews()->avg('rating') . '';
            }
        }
        return $products;
    }

    public function getAverageRatingAttribute()
    {
        return $this->reviews->avg('rating');
    }
}
