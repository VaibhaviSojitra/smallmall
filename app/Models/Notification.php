<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use HasFactory;
    
    public function sendNotification($title,$message,$device_type="",$device_token="")
    {

        if($device_type == 1)
        {
            $customData =  array("message" =>$message);
            
            $url = 'https://fcm.googleapis.com/fcm/send';

            $api_key = env('FCM_TOKEN');

            $body = $message;
            $notification = array('title' => $title , 'body' => $body, 'sound' => 'default', 'badge' => '1');
            $fields = array('to' => $device_token, 'notification' => $notification,'priority'=>'high');

            $headers = array(
                'Content-Type:application/json',
                'Authorization:key='.$api_key
            );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            if ($result === FALSE) {
                die('FCM Send Error: ' . curl_error($ch));
            }
            curl_close($ch);
            
            return $result;
        }
        else
        {
            $url = 'https://fcm.googleapis.com/fcm/send';

            $api_key = env('FCM_TOKEN');

            $msg = array ( 'title' => $title, 'body' => $message);

            $data = [
                "registration_ids" => $device_token,
                "notification" => [
                    "title" => $title,
                    "body" => $message,  
                    "sound" => "default"
                ]
            ];

            $headers = array(
                'Content-Type:application/json',
                'Authorization:key='.$api_key
            );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
            $result = curl_exec($ch);
            if ($result === FALSE) {
                die('FCM Send Error: ' . curl_error($ch));
            }
            curl_close($ch);
            return $result;
        }
        // dd($notification);
        // $message = $notification['message'];
        // $title = $notification['title'];
        // $notification['type'] = $notification['segment'];
        
        // $user = User::find(1);
        // // $user->notify(new AllUserNotification($notification));

        // $content = array(
        //     "en" => $message
        // );

        // $fields = array(
        //     'app_id' => config('services.onesignal.app_id'),
           
        //     'filters' => array(array("field" => "tag", "key" => "user_type", "relation" => "=", "value" => $notification['segment'])),
           

        //     'included_segments' => array('Active Users'),
        //     'contents' => $content
        // );

        // $fields = json_encode($fields);

        // $ch = curl_init();
        // curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        // curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        //     'Content-Type: application/json; charset=utf-8',
        //     'Authorization: Basic '. config('services.onesignal.rest_api_key')
        // ));
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        // curl_setopt($ch, CURLOPT_HEADER, FALSE);
        // curl_setopt($ch, CURLOPT_POST, TRUE);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        // $response = curl_exec($ch);
        // // print_r($response);
        // curl_close($ch);


    }
}