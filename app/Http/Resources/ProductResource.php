<?php

namespace App\Http\Resources;

use App\Models\ProductReview;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    use CommonResponseFormat;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "title" => $this->title,
            "subtitle" => $this->subtitle,
            "slug" => $this->slug,
            "description" => $this->description,
            "short_description" => $this->short_description,
            "used_material" => $this->used_material,
            "manufactured_in" => $this->manufactured_in,
            "manufactured_by" => $this->manufactured_by,
            "sustainability_statement" => $this->sustainability_statement,
            "selling_price" => $this->selling_price,
            "display_price" => $this->display_price,
            "specification" => $this->specification ?
                (is_string($this->specification) ? json_decode($this->specification,true) : $this->specification) :
                [],
            "is_featured" => $this->is_featured,
            "is_popular" => $this->is_popular,
            "is_approved" => $this->is_approved,
            "cart_id" => $this->cart_id,
            "is_cart" => $this->is_cart,
            "quantity" => $this->quantity,
            "wishlist_id" => $this->wishlist_id,
            "is_wishlist" => $this->is_wishlist,
            "category_id" => $this->category_id,
            "avg_rating" =>  sprintf("%.1f",$this->avg_rating),
            "average_rating" =>  sprintf("%.1f",$this->average_rating),
            "seller" => ProductSellerResource::make($this->whenLoaded('seller')),
            "medias" => MediaResource::collection($this->whenLoaded('medias')),
            "reviews" => ProductReviewResource::collection($this->whenLoaded('reviews')),
            "category" => CategoryResource::make($this->whenLoaded('category'))
        ];
    }
}
