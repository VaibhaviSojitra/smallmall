<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class PublicUserResource extends JsonResource
{
    use CommonResponseFormat;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "email" => Str::mask($this->email, "*", 3, -4),
            "profile_image" => $this->profile_image ? config('app.url') . Storage::url('users/' . $this->profile_image) : '',
        ];
    }
}
