<?php

namespace App\Http\Resources;

use App\Models\OrderItem;
use App\Models\ProductReview;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderItemResource extends JsonResource
{
    use CommonResponseFormat;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {


        return [
            "id" => $this->id,
            "order_id" => $this->order_id,
            "quantity" => $this->quantity,
            "status" => $this->status,
            "status_title" => OrderItem::statusToText($this->status),
            "ordered_date" => $this->created_at ? \Carbon\Carbon::parse($this->created_at)->format('M d, Y') : null,
            "confirmed_date" => $this->confirmed_at ? \Carbon\Carbon::parse($this->confirmed_at)->format('M d, Y') : null,
            "processing_date" =>  $this->assigned_at ? \Carbon\Carbon::parse($this->assigned_at)->format('M d, Y') : null,
            "shipped_date" =>  $this->shipped_date ? \Carbon\Carbon::parse($this->shipped_date)->format('M d, Y') : null,
            "completed_date" =>  $this->compledted_at ? \Carbon\Carbon::parse($this->compledted_at)->format('M d, Y') : null,
            "cancelled_date" =>  $this->cancelled_at ? \Carbon\Carbon::parse($this->cancelled_at)->format('M d, Y') : null,
            "user" => UserResource::make($this->whenLoaded('user')),
            "product" => ProductResource::make($this->whenLoaded('product')),
            "seller" => ProductSellerResource::make($this->whenLoaded('product.seller')),
            "order" => OrderResource::make($this->whenLoaded('order')),
        ];
    }
}
