<?php

namespace App\Http\Resources;

use App\Models\ProductReview;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    use CommonResponseFormat;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

        return [
            "id" => $this->id,
            "order_id" => $this->order_id,
            "total_amount" => $this->total_amount,
            "payment_type" => $this->payment_type,
            "coupon_discount" => $this->coupon_discount,
            "shipping_charge" => $this->shipping_charge,
            "transaction_id" => $this->transaction_id,
            "transaction_status" => $this->transaction_status,
            "ordered_at" => \Carbon\Carbon::parse($this->ordered_at)->format('M d, Y'),
            "user" => UserResource::make($this->whenLoaded('user')),
            "order_items" => OrderItemResource::collection($this->whenLoaded('order_items')),
            "billing_address" => AddressResource::make($this->whenLoaded('billing_address')),
            "shipping_address" => AddressResource::make($this->whenLoaded('shipping_address'))
        ];
    }
}
