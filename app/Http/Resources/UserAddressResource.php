<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserAddressResource extends JsonResource
{
    use CommonResponseFormat;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "full_name" => $this->full_name,
            "mobile" => $this->mobile,
            "add_1" => $this->add_1,
            "add_2" => $this->add_2,
            "city" => $this->city,
            "state" => $this->state,
            "zip" => $this->zip,
        ];
    }
}
