<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class ProductReviewResource extends JsonResource
{
    use CommonResponseFormat;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "rating" => $this->rating,
            "review" => $this->review,
            "created_at" => Carbon::parse($this->created_at)->format('Y-m-d h:i:s'),
            "user" => PublicUserResource::make($this->whenLoaded("user")),
            "product" => ProductResource::make($this->whenLoaded("product"))
        ];
    }
}
