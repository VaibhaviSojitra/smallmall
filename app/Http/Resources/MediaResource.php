<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class MediaResource extends JsonResource
{
   
    use CommonResponseFormat;


    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "media_type" => $this->type == 0 ? "image" : "video",
            "path" => Storage::disk('product')->url($this->path),
            "full_url" => $this->full_url
        ];
    }
}
