<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;
use Auth; 

class UserResource extends JsonResource
{
    use CommonResponseFormat;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "email" => $this->email,
            "mobile" => $this->mobile,
            "gender" => $this->gender,
            "dob" => $this->dob,
            "status" => $this->status,
            "browser_token" => $this->browser_token,
            "customer_id" => $this->customer_id,
            "payment_method_id" => $this->payment_method_id,
            "profile_image" => $this->profile_image ? config('app.url') . Storage::url('users/' . $this->profile_image) : '',
            "address" => UserAddressResource::collection($this->whenLoaded('addresses')),
            "is_subscribed" => $this->subscribedBrands->count() > 0 ? 1 : 0,
            "total_product" => $this->products_count,
            "is_verified" => $this->is_verified,
            "is_subscriber" => ($this->subscriber && $this->subscriber->user_id == Auth::user()->id) ? 1 : 0,
        ];
    }
}
