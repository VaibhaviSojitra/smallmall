<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class VendorQuestionResource extends JsonResource
{
    use CommonResponseFormat;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "user_id" => $this->user_id,
            "business_name" => $this->business_name,
            "business_location" => $this->business_location,
            "business_type" => $this->business_type,
            "comapany_register_number" => $this->comapany_register_number,
            "business_add_1" => $this->business_add_1,
            "business_add_2" => $this->business_add_2,
            "business_city" => $this->business_city,
            "business_state" => $this->business_state,
            "business_zip" => $this->business_zip,
            "firstname" => $this->firstname,
            "middlename" => $this->middlename,
            "lastname" => $this->lastname,
            "contact_number" => $this->contact_number,
            "country_citizenship" => $this->country_citizenship,
            "dob" => $this->dob,
            "add_1" => $this->add_1,
            "add_2" => $this->add_2,
            "city" => $this->city,
            "state" => $this->state,
            "zip" => $this->zip,
            "is_primary_contact" => $this->is_primary_contact,
            "bank_acc_holder_name" => $this->bank_acc_holder_name,
            "bank_name" => $this->bank_name,
            "bank_acc_number" => $this->bank_acc_number,
            "bank_ifsc" => $this->bank_ifsc,
            "brand_name" => $this->brand_name,
            "sell_products_type" => $this->sell_products_type,
            "product_produce" => $this->product_produce,
            "product_sustainably" => $this->product_sustainably,
            "raw_materials_sourced" => $this->raw_materials_sourced,
            "trademark" => $this->trademark,
            "identity_card" => $this->identity_card ? Storage::disk('vendor')->url($this->identity_card) : "",
            "address_proof" => $this->address_proof ? Storage::disk('vendor')->url($this->address_proof) : "",
            "status" => $this->status,
        ];
    }
}
