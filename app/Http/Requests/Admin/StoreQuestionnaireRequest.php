<?php

namespace App\Http\Requests\Admin;

use Exception;
use Illuminate\Foundation\Http\FormRequest;

class StoreQuestionnaireRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'user_id' => 'nullable',
            'business_name' => 'required|string',
            'business_location' => 'required|string',
            'business_type' => 'required',
            'comapany_register_number' => 'required',
            'business_add_1' => 'required|string',
            'business_add_2' => 'required|string',
            'business_city' => 'required|string',
            'business_state' => 'required|string',
            'business_zip' => 'required|string',
            'firstname' => 'required|string',
            'middlename' => 'required|string',
            'lastname' => 'required|string',
            'contact_number' => 'required',
            'dob' => 'required',
            'add_1' => 'required|string',
            'add_2' => 'required|string',
            'city' => 'required|string',
            'state' => 'required|string',
            'zip' => 'required|string',
            'is_primary_contact' => 'required',
            'bank_acc_holder_name' => 'required|string',
            'bank_name' => 'required|string',
            'bank_acc_number' => 'required|string',
            'bank_ifsc' => 'required|string',
            'brand_name' => 'required|string',
            'sell_products_type' => 'required|string',
            'product_produce' => 'required|string',
            'product_sustainably' => 'required|string',
            'raw_materials_sourced' => 'required|string',
            'trademark' => 'required|string',
            'identity_card' => 'required|image',
            'address_proof' => 'required|image',

        ];
    }

}
