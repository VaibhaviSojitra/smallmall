<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Traits\AuthorizesAfterValidation;
use Exception;
use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{
    use AuthorizesAfterValidation;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorizeValidated()
    {
        //TODO: Check for permission
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title' => 'required|string',
            'subtitle' => 'nullable|string',
            'used_material' => 'required|string',
            'manufactured_in' => 'required|string',
            'manufactured_by' => 'required|string',
            'sustainability_statement' => 'required|string',
            'display_price' => 'nullable',
            'selling_price' => 'nullable',
            'category_id' => 'required|exists:categories,id',
            'description' => 'required|string',
            'short_description' => 'nullable|string',
            'specification' => 'nullable|array',
            'specification.*' => 'array:key,value'
        ];
    }

    public function prepareForValidation()
    {
        try {
            if (is_string($this->specification)) {
                $this->merge([
                    'specification' => json_decode($this->specification, true)  
                ]);
            }
        }catch (Exception $e) {
            // Do nothing, validation will send 422
        }
    }
}
