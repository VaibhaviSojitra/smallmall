<?php

namespace App\Http\Requests\Order;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'total_amount' => 'required',
            'billing_address_id' => 'required',
            'is_shipping_address' => 'required',
            'shipping_address_id' => 'required_if:is_shipping_address,==,1',
            'payment_type' => 'required',
            //'transaction_id' => 'required_if:payment_type,==,2,3'
        ];
    }
}
