<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class DashboardController extends Controller
{
    public function __invoke(Request $request)
    {
        $user = auth()->user();
        if( $user->hasRole('seller') )
        {
            $totalOrders = Order::with(['user', 'order_items.product.seller', 'shipping_address'])
                            ->whereHas('order_items.product', fn($q) => $q->where('seller_id', $user->id) )
                            ->paginate();
            $totalProducts = Product::where('seller_id', $user->id)->count();
            $categories = Category::count();

            $dailySalesGraph = Order::select(DB::raw("(SUM(total_amount)) as total_amount"),DB::raw("HOUR(created_at) as dayname"))
                                ->where('created_at', Carbon::now()->subHours(24))
                                ->whereHas('order_items.product', fn ($q) => $q->where('seller_id', $user->id) )
                                ->groupBy('dayname')
                                ->latest()->get()->reverse();

            $weeklySalesGraph = Order::select(DB::raw("(SUM(total_amount)) as total_amount"),DB::raw("DAYNAME(created_at) as weekname"))
                                ->whereBetween('created_at', [Carbon::now()->subWeek(), Carbon::now()])
                                ->whereHas('order_items.product', fn ($q) => $q->where('seller_id', $user->id) )
                                ->groupBy('weekname')
                                ->latest()->get()->reverse();

            $monthlySalesGraph = Order::select(DB::raw("(SUM(total_amount)) as total_amount"),DB::raw("MONTHNAME(created_at) as monthname"))
                                ->whereBetween('created_at', [Carbon::now()->subYears(1), Carbon::now()])
                                ->whereHas('order_items.product', fn ($q) => $q->where('seller_id', $user->id) )
                                ->groupBy('monthname')
                                ->latest()->get()->reverse();


            $dailyProductsGraph = Product::select(DB::raw("(COUNT(*)) as total"),DB::raw("HOUR(created_at) as dayname"))
                                ->where('created_at', Carbon::now()->subHours(24))
                                ->where('seller_id', $user)
                                ->groupBy('dayname')
                                ->latest()->get()->reverse();

            $weeklyProductsGraph = Product::select(DB::raw("(COUNT(*)) as total"),DB::raw("DAYNAME(created_at) as weekname"))
                                ->whereBetween('created_at', [Carbon::now()->subWeek(), Carbon::now()])
                                ->where('seller_id', $user)
                                ->groupBy('weekname')
                                ->latest()->get()->reverse();

            $monthlyProductsGraph = Product::select(DB::raw("(COUNT(*)) as total"),DB::raw("MONTHNAME(created_at) as monthname"))
                                ->whereBetween('created_at', [Carbon::now()->subYears(1), Carbon::now()])
                                ->where('seller_id', $user)
                                ->groupBy('monthname')
                                ->latest()->get()->reverse();

            $monthlyEarning = 0;

            return view('admin.dashboard', compact('totalOrders', 'totalProducts', 'monthlyEarning', 'categories', 'dailySalesGraph',
                                'weeklySalesGraph', 'monthlySalesGraph', 'dailyProductsGraph', 'weeklyProductsGraph', 'monthlyProductsGraph'));
        }

        $totalOrders = Order::with(['user', 'order_items.product', 'order_items.product.seller', 'shipping_address'])->paginate();
        $totalProducts = Product::count();
        $categories = Category::count();

        $dailySalesGraph = Order::select(DB::raw("(SUM(total_amount)) as total_amount"),DB::raw("HOUR(created_at) as dayname"))
                            ->where('created_at', Carbon::now()->subHours(24))
                            ->groupBy('dayname')
                            ->latest()->get()->reverse();
        $weeklySalesGraph = Order::select(DB::raw("(SUM(total_amount)) as total_amount"),DB::raw("DAYNAME(created_at) as weekname"))
                            ->whereBetween('created_at', [Carbon::now()->subWeek(), Carbon::now()])
                            ->groupBy('weekname')
                            ->latest()->get()->reverse();
        $monthlySalesGraph = Order::select(DB::raw("(SUM(total_amount)) as total_amount"),DB::raw("MONTHNAME(created_at) as monthname"))
                            ->whereBetween('created_at', [Carbon::now()->subYears(1), Carbon::now()])
                            ->groupBy('monthname')
                            ->latest()->get()->reverse();

        $dailyProductsGraph = Product::select(DB::raw("(COUNT(*)) as total"),DB::raw("HOUR(created_at) as dayname"))
                                ->where('created_at', Carbon::now()->subHours(24))
                                ->groupBy('dayname')
                                ->latest()->get()->reverse();
        $weeklyProductsGraph = Product::select(DB::raw("(COUNT(*)) as total"),DB::raw("DAYNAME(created_at) as weekname"))
                                ->whereBetween('created_at', [Carbon::now()->subWeek(), Carbon::now()])
                                ->groupBy('weekname')
                                ->latest()->get()->reverse();
        $monthlyProductsGraph = Product::select(DB::raw("(COUNT(*)) as total"),DB::raw("MONTHNAME(created_at) as monthname"))
                                ->whereBetween('created_at', [Carbon::now()->subYears(1), Carbon::now()])
                                ->groupBy('monthname')
                                ->latest()->get()->reverse();

        return view('admin.dashboard', compact('totalOrders', 'totalProducts', 'categories', 'dailySalesGraph', 'weeklySalesGraph',
                                'monthlySalesGraph', 'dailyProductsGraph', 'weeklyProductsGraph', 'monthlyProductsGraph'));
    }

}
