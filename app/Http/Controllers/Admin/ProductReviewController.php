<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductReviewCollection;
use App\Models\Product;
use App\Models\ProductReview;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;

class ProductReviewController extends Controller
{

    protected $productRepo;

    public function __construct()
    {
        $this->productRepo = new ProductRepository();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Product $product)
    {
        return ProductReviewCollection::make(
            $this->productRepo->reviews($product)
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  ProductReview $productReview
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product, ProductReview $review)
    {
        return $this->respondWith(
            $review->delete()
        );
    }
}
