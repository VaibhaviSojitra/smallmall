<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AddMediaRequest;
use App\Http\Resources\MediaCollection;
use App\Http\Resources\MediaResource;
use App\Models\Media;
use App\Models\Product;
use App\Repositories\ProductMediaRepository;
use Illuminate\Http\Request;

class ProductMediaController extends Controller
{

    protected $productMediaRepository;
    public function __construct()
    {
        $this->productMediaRepository = new ProductMediaRepository();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Product $product)
    {
        return MediaCollection::make($product->medias()->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddMediaRequest $request, Product $product)
    {
        $input = $request->validated();
        return MediaResource::make(
            $this->productMediaRepository->store($product, $input['media'])
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product, Media $medias)
    {
        return MediaResource::make($medias);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product $product, \App\Models\Media $media
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product, $media_id)
    {
        $medias = Media::find($media_id);
        return $this->respondWith($medias->delete());
    }
}
