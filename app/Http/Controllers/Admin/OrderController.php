<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreProductRequest;
use App\Http\Requests\Admin\UpdateProductRequest;
use App\Http\Resources\OrderCollection;
use App\Http\Resources\ProductResource;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use App\Repositories\OrderRepository;
use Illuminate\Http\Request;

class OrderController extends Controller
{

    protected $orderRepository;

    public function __construct()
    {
        $this->orderRepository = new OrderRepository();
    }

    public function getStatusField($index){
        $statusFields = [
            'updated_at',
            'confirmed_at',
            'assigned_at',
            'shipped_date',
            'updated_at',
            'completed_at',
            'cancelled_at',
        ];
        return $statusFields[$index];
    }

    public function index()
    {
        return OrderCollection::make(
            $this->orderRepository->allSellerOrders()
        );
    }

    public function show(Product $product)
    {
        return ProductResource::make($product);
    }

    public function changeOrderStatus(Request $request)
    {
        $id = $request->params['orderItem'];
        $status = $request->params['status'];
        $timestamp = \Carbon\Carbon::now();
        $key = $this->getStatusField($status);
        OrderItem::where('id', $id)->update([
            'status'=> $status,
            $key => $timestamp
        ]);
    }

    public function orderDetail(Order $order){
        $user = auth()->user();

        $order = Order::where('id',$order->id)->with(['user', 'order_items', 'order_items.product.seller','billing_address','shipping_address', 'order_items.product.medias']);

        if($user->hasRole('seller')){
            $order = $order->whereHas('order_items.product', function($q) use ($user){
                $q->where('seller_id', $user->id);
            });
        }

        $order = $order->first();
        return view('admin.order.details', compact('order'));
    }

    public function editOrder(Order $order){

        $user = auth()->user();

        $order = Order::where('id',$order->id)->with(['user', 'order_items', 'order_items.product.seller','billing_address','shipping_address', 'order_items.product.medias'])->first();

        return view('admin.order.edit', compact('order'));
    }

}
