<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CreateCategoryRequest;
use App\Http\Requests\Admin\DeleteCategoryRequest;
use App\Http\Requests\Admin\UpdateCategoryRequest;
use App\Http\Resources\CategoryCollection;
use App\Http\Resources\CategoryResource;
use App\Models\Category;
use App\Repositories\CategoryRepository;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    protected $categoryRepository;
    public function __construct()
    {
        $this->categoryRepository = new CategoryRepository();
    }

    public function index()
    {
        return CategoryCollection::make(
            $this->categoryRepository->index()
        );
    }

    public function store(CreateCategoryRequest $request)
    {
        return CategoryResource::make(
            $this->categoryRepository->create($request)
        );
    }

    public function update(Category $category, UpdateCategoryRequest $request)
    {
        return CategoryResource::make(
            $this->categoryRepository->update($category, $request)
        );
    }

    public function destroy(Category $category, DeleteCategoryRequest $request)
    {
        return $this->respondWith(
            $this->categoryRepository->delete($category)
        );
    }
}
