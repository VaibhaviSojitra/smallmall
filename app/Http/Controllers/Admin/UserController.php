<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use App\Http\Resources\VendorQuestionCollection;
use App\Http\Resources\VendorQuestionResource;
use App\Http\Requests\Admin\StoreQuestionnaireRequest;
use App\Http\Resources\UserSubscriberResource;
use App\Http\Resources\UserSubscriberCollection;
use App\Factories\PaymentProviderFactory;
use App\Models\User;
use App\Models\SubscriptionPlan;
use App\Models\VendorQuestionnaire;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    protected $userRepository;

    public function __construct()
    {
        $this->provider = 'stripe';
        $this->userRepository = new UserRepository();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $role = $request->role;
        return UserCollection::make(
            $this->userRepository->index($role)
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $user->loadMissing(['addresses', 'latestOrder']);
        return UserResource::make($user);
    }

    /**
     * Block the specified resource.
     *
     * @param  User $user
     * @return \Illuminate\Http\Response
     */
    public function block(User $user)
    {
        $user->status = 'BLOCK';
        $user->save();
        return UserResource::make($user);
    }

    /**
     * UnBlock the specified resource.
     *
     * @param  User $user
     * @return \Illuminate\Http\Response
     */
    public function unblock(User $user)
    {
        $user->status = 'UNBLOCK';
        $user->save();
        return UserResource::make($user);
    }

    public function vendorQuestionnaire()
    {
        $user_id = Auth::user()->id;

        $vendorQuestionnaire =  $this->userRepository->getVendorQuestionnaire($user_id);
        if($vendorQuestionnaire){
            return VendorQuestionResource::make( $vendorQuestionnaire );
        }else{
            return VendorQuestionResource::make( new VendorQuestionnaire() );
        }
    }

    public function vendorAllQuestionnaire()
    {
        $vendorQuestionnaire =  $this->userRepository->getVendorQuestionnaire();
        if($vendorQuestionnaire){
            return VendorQuestionCollection::make( $vendorQuestionnaire );
        }else{
            return [];
        }
    }

    public function approvedVendorQuestionnaire()
    {
        $vendorQuestionnaire =  $this->userRepository->getApprovedVendorQuestionnaire();
        if($vendorQuestionnaire){
            return VendorQuestionCollection::make( $vendorQuestionnaire );
        }else{
            return [];
        }
    }

    public function vendorAddQuestionnaire(StoreQuestionnaireRequest $request)
    {
        return VendorQuestionResource::make(
            $this->userRepository->addQuestionnaire($request));
    }

    public function subscribedUsers(Request $request)
    {
        $role = $request->role;
        return UserCollection::make(
            $this->userRepository->subscribedUsers($role)
        );
    }


    public function vendorSubscribe(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'plan_id' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors(['error' => $validator->errors()]);
        }
        $user = User::where('id', Auth::user()->id)->first();

        $plan = SubscriptionPlan::where('id',$request->plan_id)->first();

        $paymentProvider = PaymentProviderFactory::get($this->provider);
        $paymentProvider->initiateSubscription($request, $plan, $user);

        return redirect()->to('admin/subscription')->with(['message' => 'Successfully Subscribe with selected plan.']);

    }

    public function changeVendorQuestionnaireStatus(Request $request)
    {
        $id = $request->params['questionnaire_id'];
        $status = $request->params['status'];
        VendorQuestionnaire::where('id', $id)->update([
            'status'=> $status,
        ]);
    }
}
