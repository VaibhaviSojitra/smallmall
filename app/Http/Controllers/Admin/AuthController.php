<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Auth\forgotPasswordRequest;
use App\Http\Requests\Auth\resetPasswordRequest;
use App\Http\Requests\DashboardLoginRequest;
use App\Models\User;
use App\Models\PasswordReset;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\ForgotPassword;
use Carbon\Carbon;
use Session;

class AuthController extends Controller
{
    /**
     * Login The User
     * @param Request $request
     * @return User
     */
    public function loginUser(DashboardLoginRequest $request)
    {
       
        try {

            $user = User::where('email', $request->email)->first();
            if (!$user->hasRole(['seller', 'admin'])) {
                Auth::logout();
                return redirect()->to('admin/login')->withErrors(['email' => 'User name and password do not match.']);
            }
            if( $user->hasRole('seller') ){
                if($user->status == 'BLOCK'){
                    return redirect()->to('admin/login')->withErrors(['error' => 'Oppes! Your account is blocked by admin.']); 
                }
                if($user->is_verified == 0){
                    return redirect()->to('admin/login')->withErrors(['error' => 'Oppes! Your account is not verified.']); 
                }
                
                if(!Auth::attempt($request->only('email', 'password'), $request->remember ?? false))
                {
                    return redirect()->to('admin/login')->withErrors(['email' => 'User name and password do not match']);
                }
    
                return redirect()->to('admin/questionnaire');
            }else{
                if(!Auth::attempt($request->only('email', 'password'), $request->remember ?? false))
                {
                    return redirect()->to('admin/login')->withErrors(['email' => 'User name and password do not match']);
                }
                return redirect()->to('admin/dashboard');
            }

        } catch (\Throwable $th) {
            return redirect()->to('admin/login')->withException($th);
        }
    }
        
    public function forgotPassword(forgotPasswordRequest $request)
    {
    
        $user = User::where('email', $request->get('email'))->first();

        if (!empty($user)) {
            PasswordReset::where('email', $request->get('email'))->delete();
            $passwordReset = new PasswordReset;
            $passwordReset->email = $request->get('email');
            $passwordReset->token = mt_rand(100000, 999999);
            $passwordReset->created_at = Carbon::now();
            $passwordReset->save();

            Mail::to($request->get('email'))->send(new ForgotPassword($user, $passwordReset));
            return $this->respondWith([], "An Email Containing Code has been Sent to Your Verified Email Address.", 200, true);

        } else {
            return $this->respondWith([], "User not found", 200, true);
        }
   
    }

    public function resetPassword(resetPasswordRequest $request)
    {

        $check = PasswordReset::where('email', $request->get('email'))->where('token', $request->get('otp'))->first();
        if (!empty($check)) {
            $user = User::where('email', $request->get('email'))->first();
            $user->password = bcrypt($request->get('password'));
            $user->save();
            PasswordReset::where('email', $request->get('email'))->where('token', $request->get('otp'))->delete();
            return $this->respondWith([], "Password changed successfully.", 200, true);
        } else {
            return $this->respondWith([], "Please enter valid OTP.", 404, true);
        }

    }

}
