<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreProductRequest;
use App\Http\Requests\Admin\UpdateProductRequest;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\ProductResource;
use App\Models\Cart;
use App\Models\Product;
use App\Models\VendorQuestionnaire;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

class ProductController extends Controller
{

    protected $productRepository;

    public function __construct()
    {
        $this->productRepository = new ProductRepository();
    }

    public function index()
    {
        return ProductCollection::make(
            $this->productRepository->getAdminProducts()
        );

    }

    public function show(Product $product)
    {
        return ProductResource::make($product);
    }

    public function store(StoreProductRequest $request)
    {
        return ProductResource::make(
            $this->productRepository->store($request->validated())
        );
    }

    public function update(UpdateProductRequest $request, Product $product)
    {
        $input = $request->validated();
        $this->productRepository->update($product, $input);
        $product = Product::find($product->id);
        return ProductResource::make($product);
    }

    public function approve(Product $product)
    {
        $product->is_approved = 1;
        $product->save();

        return ProductResource::make($product);
    }

    public function reject(Product $product)
    {
        $product->is_approved = 0;
        $product->save();

        Cart::where('product_id',$product->id)->delete();

        return ProductResource::make($product);
    }

    public function destroy(Product $product)
    {
        return $this->respondWith(
            $this->productRepository->delete($product)
        );
    }

    public function vendorProducts(Request $request)
    {
        // dd($request->seller_id['userId']);
        $products = Product::where('seller_id', $request->seller_id['userId'])->with([
            'seller', 'medias', 'reviews', 'category', 'category.products'
        ])->paginate();

        return $products;
    }

    public function changePopular(Product $product)
    {
        $product->is_popular = $product->is_popular == 1 ? 0 : 1;
        $product->save();

        return ProductResource::make($product);
    }

    public function changeFeatured(Product $product)
    {
        $product->is_featured = $product->is_featured == 1 ? 0 : 1;
        $product->save();

        return ProductResource::make($product);
    }

    public function topProducts()
    {
        $products = Product::with([
            'seller', 'medias', 'reviews', 'category', 'category.products'
        ])->get();

        $newProducts = collect($products)->sortByDesc('average_rating')->values()->take(30)->all();

        return ProductCollection::make($newProducts );
    }



    public function pagination($items, $perPage = 15, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        // $items = $items instanceof Collection ? $items : Collection::make($items);

        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
}
