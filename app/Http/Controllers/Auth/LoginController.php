<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Repositories\CartRepository;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    private $cartRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->cartRepository = new CartRepository();

        $this->middleware('guest')->except('logout');
    }

     /**
     * Show the application's login form.
     *
     * @return \Illuminate\View\View
     */
    public function showLoginForm()
    {
        return view('web.auth.login');
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);
 

        $user = User::where('email', $request->email)->first();
        if ($user->hasRole(['seller', 'admin'])) {
            Auth::logout();
            return redirect()->back()->withErrors(['error' => 'Oppes! You are seller. You are not Allowed to access website']); 
        }  
        if($user->status == 'BLOCK'){
            return redirect()->back()->withErrors(['error' => 'Oppes! Your account is blocked by admin.']); 
        }
        if($user->is_verified == 0){
            return redirect()->back()->withErrors(['error' => 'Oppes! Your account is not verified.']); 
        }
  
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            $user = User::where('email', $request->email)->first();
            if($user->status == 'BLOCK'){
                return redirect()->back()->withErrors(['error' => 'Oppes! Your account is blocked by admin.']); 
            }
            if($user->is_verified == 0){
                return redirect()->back()->withErrors(['error' => 'Oppes! Your account is not verified.']); 
            }
            $session_id = Session::get('session_id');
            if($session_id){
               Cart::where('session_id',$session_id)->update([
                'session_id' => '',
                'user_id' => $user->id
               ]);
            }
            if( $user->hasRole('seller') )
            {
                return redirect()->to('admin/vendor/questionnaire');
            }else{
                return redirect()->to('my-account');
            }
        }

        $session_id = Session::get('session_id');
        if($session_id){
            Cart::where('session_id',$session_id)->update([
            'session_id' => '',
            'user_id' => $user->id
            ]);
        }
        return redirect()->route('my-account');
        
    }
}
