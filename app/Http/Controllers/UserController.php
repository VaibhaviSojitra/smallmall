<?php

namespace App\Http\Controllers;

use App\Mail\ForgotPassword;
use App\Mail\ForgotPasswordMail;
use App\Models\PasswordReset;
use App\Repositories\UserRepository;
use App\Repositories\CartRepository;
use App\Repositories\WishlistRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Models\User;
use App\Models\UserAddress;
use App\Models\VerifyUser;
use Session;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    private $userRepository;
    private $cartRepository;
    private $wishlistRepository;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
        $this->cartRepository = new CartRepository();
        $this->wishlistRepository = new WishlistRepository();
    }

    public function verifyEmail(Request $request,$token)
    {
        $verifyUser = VerifyUser::where('token', $token)->first();

        if (!empty($verifyUser)) {

            $user = User::where('id',$verifyUser->user_id)->first();
            if($user->is_verified == 0) {
                $user->is_verified = 1;
                $user->save();

                return redirect()->to('/login')->with(['message' => 'Your Email has been activated successfully. ']);
            } else {
			    return redirect()->to('/login')->withErrors(['error' => 'Your Email Already Activate. Please Login']);;
            }
        } else {
			return redirect()->to('/login')->withErrors(['error' => 'You have no permission to access this link']);;
        }
    }

    public function logout()
	{
        Session::flush();
        return redirect()->to('/login');
	}

    public function myaccount()
    {
        return view('web.pages.my-account');
    }

    public function addUserAddress(Request $request)
	{
        $data['user_id'] = Auth::user()->id;
        $data['full_name'] = $request->full_name;
        $data['mobile'] = $request->mobile;
        $data['add_1'] = $request->add_1;
        $data['add_2'] = $request->add_2;
        $data['city'] = $request->city;
        $data['state'] = $request->state;
        $data['zip'] = $request->zip;
        $response['billing_address'] = $this->userRepository->addUserAddress($data);
        if($request->is_shipping_address){
            $sdata['user_id'] = Auth::user()->id;
            $sdata['full_name'] = $request->s_full_name;
            $sdata['mobile'] = $request->s_mobile;
            $sdata['add_1'] = $request->s_add_1;
            $sdata['add_2'] = $request->s_add_2;
            $sdata['city'] = $request->s_city;
            $sdata['state'] = $request->s_state;
            $sdata['zip'] = $request->s_zip;
            $response['shipping_address'] = $this->userRepository->addUserAddress($sdata);
        }
        return response()->json(['data'=>$response,'success' => 1]);
	}

    public function showForgotPassword()
    {
        return view('web.auth.forgot-password');
    }

    public function sendForgotOtp(Request $request)
    {
        $request->validate([
            'email' => 'required|exists:users,email',
        ]);

        $email = $request->email;

        $check = PasswordReset::where('email', $email)->first();
        try
        {
            if(!$check)
            {
                $this->generateOTP($email);
                return view('web.auth.verify-otp')->with('email', $request->email)->with('message', 'Please enter the verification code sent on your mail id');
            }
            if($check->created_at <= Carbon::now()->subMinutes(5))
            {
                $this->generateOTP($email);
            }
            return view('web.auth.verify-otp')->with('email', $request->email)->with('message', 'Please enter the verification code sent on your mail id');
        }
        catch(\Exception $e)
        {
            \Log::info($e->getMessage());
            return redirect()->back()->with('message', 'Something went wrong while processing your request!');
        }

    }

    public function verifyForgotOtp(Request $request)
    {
        $request->validate([
            'email' => 'required|exists:users,email',
            'security_code' => 'required',
        ]);

        $email = $request->email;
        $security_code = $request->security_code;

        $check = DB::table('password_resets')->where(['email'=> $email, 'token'=> $security_code])->first();

        if(!$check)
            return back()->with('message', 'Invalid security code');

        return view('web.auth.change-password')->with(['message'=> 'Security code verified successfully!', 'email'=> $email]);
    }

    public function changeForgotPassword(Request $request)
    {
        $request->validate([
            'email' => 'required|exists:users,email',
            'password' => 'required|same:confirm_password',
            'confirm_password' => 'required',
        ]);

        $email = $request->email;
        $password = $request->password;

        DB::table('users')->where('email', $email)->update(['password'=> Hash::make($password)]);
        $request->session()->put('message', 'Password updated successfully');
        return view('web.auth.login');
    }

    protected function generateOTP($email)
    {
        $user = User::where('email', $email)->first();
        PasswordReset::where('email', $email)->delete();
        $passwordReset = new PasswordReset;
        $passwordReset->email = $email;
        $passwordReset->token = mt_rand(100000, 999999);
        $passwordReset->created_at = Carbon::now();
        $passwordReset->save();
        Mail::to($email)->send(new ForgotPassword($user, $passwordReset));

        return true;
    }

}
