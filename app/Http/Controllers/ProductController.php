<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\CategoryRepository;
use App\Repositories\ProductRepository;
use App\Models\Product;
use Auth;
use Session;

class ProductController extends Controller
{
    private $categoryRepository;
    private $productRepository;

    public function __construct()
    {
        $this->categoryRepository = new CategoryRepository();
        $this->productRepository = new ProductRepository();
    } 

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function shop()
    {
        return view('web.pages.shop');
    }

    public function index($slug)
    {
        $product = $this->productRepository->detailBySlug($slug);
        return view('web.pages.product-details',compact('product'));
    }

    public function productByCategory($slug)
    {
        $category = $this->productRepository->categoryBySlug($slug);
        return view('web.pages.shop',compact('category'));
    }

    public function productBySeller($seller_id)
    {
        return view('web.pages.shop',compact('seller_id'));
    }

}
