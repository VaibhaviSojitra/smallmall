<?php

namespace App\Http\Controllers;

use App\Providers\RouteServiceProvider;
use App\Repositories\UserRepository;
use App\Factories\PaymentProviderFactory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Models\User;
use App\Models\VerifyUser;
use App\Mail\VerifyEmail;
use App\Models\SubscriptionPlan;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Session;

class SellerController extends Controller
{
    private $userRepository;

    public function __construct()
    {
        $this->provider = 'stripe';
        $this->userRepository = new UserRepository();
    }

    public function register()
    {     
        $plans = SubscriptionPlan::get();
        return view('web.seller.register',compact('plans'));
    }

    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'mobile' => $data['mobile'],
            'password' => Hash::make($data['password']),
        ]);

    }

    public function doregister(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required_with:password_confirmation|same:password_confirmation',
            'password_confirmation' => 'required',
            'mobile' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors(['error' => $validator->errors()]);
        }

        // event(new Registered($user = $this->create($request->all())));

        $user = $this->create($request->all());
        $user->assignRole('seller');

        $verifyUser = VerifyUser::create([
            'user_id' => $user->id,
            'token' => sha1(time())
        ]);
        $user['verifyUser'] = $verifyUser;
        \Mail::to($user->email)->send(new VerifyEmail($user));

        return redirect()->to('admin/login')->with(['message' => 'Successfully Registerd. We have sent Email. Please Verify your Account. ']);;

    }

    public function vendorsList()
    {
        return view('web.pages.vendors-list');
    }
}