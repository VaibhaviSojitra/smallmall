<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\UserAddressCollection;
use App\Repositories\UserRepository;
use App\Repositories\BannerRepository;
use App\Models\Address;
use App\Models\Banner;
use Auth;

class HomeController extends Controller
{
    private $userRepository;
    private $bannerRepository;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
        $this->bannerRepository = new BannerRepository();
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $banner = $this->bannerRepository->index();
        return view('web.pages.index',compact('banner'));
    }

    public function about()
    {
        return view('web.pages.about');
    }

    public function contact()
    {
        return view('web.pages.contact');
    }

    public function privacypolicy()
    {
        return view('web.pages.privacy-policy');
    }

    public function terms()
    {
        return view('web.pages.terms');
    }

}
