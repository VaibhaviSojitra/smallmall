<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserCollection;
use App\Http\Resources\ProductResource;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\ProductReviewCollection;
use App\Http\Resources\CategoryCollection;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\ProductSellerResource;
use App\Repositories\CategoryRepository;
use App\Repositories\ProductRepository;
use App\Repositories\UserRepository;
use App\Http\Requests\Review\StoreRequest;
use App\Models\Product;
use App\Models\ProductReview;
use App\Models\SubscribedBrand;
use App\Models\User;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{

    private $categoryRepository;
    private $productRepository;
    private $userRepository;

    public function __construct()
    {
        $this->categoryRepository = new CategoryRepository();
        $this->productRepository = new ProductRepository();
        $this->userRepository = new UserRepository();
    }

    public function index(Request $request)
    {
        $user_id = Auth::check() ? Auth::id() : null;
        $session_id = $request->session_id ? $request->session_id : Session::get('session_id');
        return ProductCollection::make($this->productRepository->index($user_id,$session_id));
    }

    public function show(Product $product,Request $request)
    {
        $user_id = Auth::check() ? Auth::id() : null;
        $session_id = $request->session_id ? $request->session_id : Session::get('session_id');
        return ProductResource::make($this->productRepository->detail($product,$user_id,$session_id));
    }

    public function reviews(Product $product)
    {
        $reviews = $this->productRepository->reviews($product);
        return ProductReviewCollection::make($reviews);
    }

    public function reviewsAdd(Product $product,StoreRequest $request)
    {
        $reviews = $this->productRepository->reviewsAdd($product,$request);
        return ProductReviewCollection::make($reviews);
    }

    public function reviewsUpdate(Product $product,ProductReview $review,StoreRequest $request)
    {
        $reviews = $this->productRepository->reviewsUpdate($product,$review,$request);
        return ProductReviewCollection::make($reviews);
    }

    public function reviewsDelete(Product $product,ProductReview $review)
    {
        $reviews = $this->productRepository->reviewsDelete($product,$review);
        return ProductReviewCollection::make($reviews);
    }

    public function categories(Request $request)
    {
        return CategoryCollection::make($this->categoryRepository->index());
    }

    public function categoryBySlug(Request $request,$slug)
    {
        return CategoryResource::make($this->categoryRepository->categoryBySlug($slug));
    }

    public function productsByCategory(Request $request,$category_id)
    {
        $user_id = Auth::check() ? Auth::id() : null;
        $session_id = $request->session_id ? $request->session_id : Session::get('session_id');
        $sort_by = $request->sort_by ? $request->sort_by : "";
        return ProductCollection::make($this->productRepository->productsByCategory($category_id,$user_id,$session_id,$sort_by));
    }

    public function sellers(Request $request)
    {
        return UserCollection::make($this->userRepository->index('seller'));
    }

    public function productsBySeller(Request $request,$seller_id)
    {
        $user_id = Auth::check() ? Auth::id() : null;
        $session_id = $request->session_id ? $request->session_id : Session::get('session_id');
        $sort_by = $request->sort_by ? $request->sort_by : "";
        return ProductCollection::make($this->productRepository->productsBySeller($seller_id,$user_id,$session_id,$sort_by));
    }

    public function relatedProductsByCategory($product_id,$category_id)
    {
        $user_id = Auth::check() ? Auth::id() : null;
        $session_id = Session::get('session_id');
        return ProductCollection::make($this->productRepository->relatedProductsByCategory($product_id,$category_id,$user_id,$session_id));
    }

    // 1 - Price Low to High
    // 2 - Price High to Low
    // 3 - Featured
    // 4 - Date
    // 5 - Avg Rating

    public function search(Request $request)
    {
        $search = $request->get('search_keyword');
        $sort_by = $request->get('sort_by');
        $min_price = $request->get('min_price') ? $request->get('min_price') : 0;
        $max_price = $request->get('max_price');
        $avg_rating = $request->get('avg_rating') ? $request->get('avg_rating') : 0;
        $category_id = $request->get('category_id');
        $user_id = Auth::check() ? Auth::id() : null;
        $session_id = $request->session_id ? $request->session_id : Session::get('session_id');

        $products = $this->productRepository->search($search,$sort_by,$min_price,$max_price,$avg_rating,$category_id,$user_id,$session_id);
        return ProductCollection::make($products);
    }

    public function reviewSummary(Product $product)
    {
        return $this->respondWith(
            $this->productRepository->reviewSummary($product)
        );
    }

    public function toggleBrandSubscription(User $seller)
    {
        $user_id = auth()->user()->id;
        $data = SubscribedBrand::where(['user_id'=> $user_id, 'seller_id'=> $seller->id])->first();

        if($data)
            $data->delete();
        else
            SubscribedBrand::create(['user_id'=> $user_id, 'seller_id'=> $seller->id]);

        return UserCollection::make($this->userRepository->index('seller'));
    }
}
