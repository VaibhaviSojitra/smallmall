<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Http\Resources\UserCollection;
use App\Http\Resources\UserAddressCollection;
use App\Http\Resources\UserAddressResource;
use App\Http\Resources\NotificationCollection;
use App\Http\Resources\NotificationResource;
use App\Repositories\UserRepository;
use App\Http\Requests\UserAddress\StoreRequest;
use App\Http\Requests\UserAddress\UpdateRequest;
use App\Http\Requests\UserAddress\DeleteRequest;
use App\Http\Requests\Auth\UserMobileRequest;
use App\Http\Requests\User\UserProfileRequest;
use App\Http\Requests\Auth\VerifyOtpRequest;
use App\Http\Requests\Auth\changePasswordRequest;
use Illuminate\Support\Facades\Storage;
use App\Models\User;
use App\Models\Address;
use App\Models\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use File;

class UserController extends Controller
{

    private $userRepository;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
    }

    public function index()
    {
        $user = Auth::user();
        return UserResource::make($this->userRepository->details($user));
    }

    public function update(Request $request)
    {
        $user = Auth::user();
        $data['name'] = $request->name;
        $data['mobile'] = $request->mobile_no;
        $data['gender'] = $request->gender;
        $data['dob'] = Carbon::parse($request->dob)->format('Y-m-d');
        if($request->password){
            $data['password'] = bcrypt($request->password);
        }
        $user = $this->userRepository->update($user->id,$data);        
        return $this->respondWith(UserResource::make($user), "Profile Update Successfully", 200, true);
    }

    public function updateUserMobile(UserMobileRequest $request)
    {
        $user = Auth::user();
        $checkMobileNumber = $this->userRepository->get($user->id,$request->mobile);        
        if($checkMobileNumber){
            return $this->respondWith([], "Mobile Number already exist.", 401, false);
        }
        $user->mobile = $request->mobile;        
        $user->save();
        
        $verificationCode = $this->userRepository->getOTPByIDs($user->id);        
        $now = Carbon::now();
        if($verificationCode && $now->isBefore($verificationCode->expire_at)){
            return UserResource::make($user);
        }else{
            $data['status'] = 0;
            $this->userRepository->updateOTP($user->id,$data);
            $adddata['user_id'] = $user->id;       
            $adddata['otp'] = rand(111111,999999);       
            $adddata['expire_at'] = Carbon::now()->addMinutes(20);       
            $adddata['status'] = 1;       
            $this->userRepository->addOTP($adddata);
            return UserResource::make($user);
        }
    }

    public function updateProfile(UserProfileRequest $request)
    {
        $user = Auth::user();

        if ($request->hasFile('profile_image')) {
            $file = $request->file('profile_image');
            $filename = $file->getClientOriginalName();
            $filename = str_replace(' ','_',$filename);
            $imageFileName='user_' . rand(111,999) . '.' . $file->getClientOriginalExtension();
            Storage::disk('user')->put($imageFileName, File::get($file));
            $user->profile_image = $imageFileName;
            $user->save();
        }
        
        return UserResource::make($user);
    }

    public function verifyOTP(VerifyOtpRequest $request)
    {
        $user = Auth::user();
        $userExist = $this->userRepository->get($user->id,request()->input('mobile_no'));        
        if($userExist){
            $verificationCode = $this->userRepository->getOTPByIDs($user->id, request()->input('otp'));        

            if($verificationCode){
                $data['is_verified'] = 1;
                $this->userRepository->update($user->id,$data);        
                return $this->respondWith([], "Verified OTP Successfully", 200, true);
            }else{
                return $this->respondWith([], "OTP Does not exist", 401, false);
            }
        }
        else{
            return $this->respondWith([], "User Does not exist with this mobile number", 401, false);
        }
    }

    public function getUserAddress($id="")
    {
        if($id){
            return UserAddressResource::make($this->userRepository->getUserAddress($id));
        }else{
            return UserAddressCollection::make($this->userRepository->getUserAddress());
        }
    }

    public function addUserAddress(StoreRequest $request)
    {
        $data['user_id'] = Auth::user()->id;
        $data['full_name'] = $request->full_name;
        $data['mobile'] = $request->mobile;
        $data['add_1'] = $request->add_1;
        $data['add_2'] = $request->add_2;
        $data['city'] = $request->city;
        $data['state'] = $request->state;
        $data['zip'] = $request->zip;
        return UserAddressResource::make($this->userRepository->addUserAddress($data));
    }

    public function updateUserAddress(UpdateRequest $request)
    {
        $data['user_id'] = Auth::user()->id;
        $data['full_name'] = $request->full_name;
        $data['mobile'] = $request->mobile;
        $data['add_1'] = $request->add_1; 
        $data['add_2'] = $request->add_2;
        $data['city'] = $request->city;
        $data['state'] = $request->state;
        $data['zip'] = $request->zip;
        return UserAddressResource::make($this->userRepository->updateUserAddress($data,Auth::user()->id,$request->id));
    }

    public function deleteUserAddress(DeleteRequest $request)
    {
        $this->userRepository->deleteUserAddress(Auth::user()->id,$request->id);
        return $this->respondWith([], "Address Deleted Successfully", 200, true);

    }

    public function changePassword(changePasswordRequest $request)
    {
        $user = $this->userRepository->getUserByID(Auth::user()->id);
        $user->password = bcrypt($request->get('password'));
        $user->save();
        return $this->respondWith([], "Password changed successfully.", 200, true);
      
    }

    public function deviceToken(Request $request)
    {
        $user = Auth::user();
        $data['browser_token'] = $request->browser_token;
        $user = $this->userRepository->update($user->id,$data);    
        return $this->respondWith(UserResource::make($user), "Token update successfully.", 200, true);
    }

    public function getNotification(Request $request)
    {
        $user = Auth::user();
        $notification = $this->userRepository->notification($user->id,);    
        return NotificationCollection::make($notification);
    }

}
 