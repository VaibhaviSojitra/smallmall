<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CartCollection;
use App\Http\Resources\ProductResource;
use App\Http\Resources\ProductCollection;
use App\Repositories\CartRepository;
use App\Repositories\ProductRepository;
use App\Http\Requests\Cart\StoreRequest;
use App\Http\Resources\CartItemResource;
use App\Models\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class CartController extends Controller
{

    private $cartRepository;

    public function __construct()
    {
        $this->cartRepository = new CartRepository();
        $this->productRepository = new ProductRepository();

    }

    public function index(Request $request)
    {

        $user_id = Auth::check() ? Auth::id() : null;
       
        if(empty($user_id)){
            $session_id = $request->session_id ? $request->session_id : Session::get('session_id');
            $data = [];
            if($session_id){
                $data = $this->cartRepository->index($session_id);
            }
        }else{
            $data = $this->cartRepository->index($user_id);
        }
        if($data){
            $response['total_amount'] = $data['total'];
            $response['cart']  = CartCollection::make($data['cart']);
        }else{
            $response['total_amount'] = 0;
            $response['cart']['data']  = [];
        }

        return $this->respondWith($response, "Cart get Successfully", 200, true);
    }

    public function getSessionId(Request $request)
    {
        $session_id = session()->getId();
        Session::put('session_id', $session_id);
        $response['session_id'] = $session_id;
        return $this->respondWith($response, "Session ID get Successfully", 200, true);
    }

    public function store(StoreRequest $request)
    {
        $input = $request->validated();
        $user_id = Auth::check() ? Auth::id() : null;
        $session_id = $request->session_id;
        if(empty($user_id)){
            if($session_id){
                $session_id = $request->session_id;
            }else{
                $session_id = session()->getId();
                Session::put('session_id', $session_id);
            }
            $data['session_id'] = $session_id;
            $checkCart = $this->cartRepository->getData($session_id, $input,1);
        }else{
            $data['user_id'] = $user_id;
            $checkCart = $this->cartRepository->getData($user_id, $input);
        }
              
        if(empty($checkCart)){ 
            $data['product_id'] = $input['product_id'];
            $data['quantity'] = $input['quantity'];
            $this->cartRepository->create($data);
            $product = $this->productRepository->detailByID($data['product_id']);
            $productList = $this->productRepository->detail($product,$user_id,$session_id);
            return $this->respondWith(ProductResource::make($productList), "Product added into Cart Successfully", 200, true);
        } else {
            return $this->respondWith([], "Product already into Cart", 200, true);
        }
    }

    public function update(Request $request)
    {
        $input = $request->all();
        $cartlist = [];
        $session_id = "";
        if($input && $input['data']){
            $user_id = Auth::check() ? Auth::id() : null;
            if(empty($user_id)){
                $session_id = $request->session_id ? $request->session_id : Session::get('session_id');
                if($session_id){
                    $cartlist =  $this->cartRepository->update($input,$session_id);
                }
            }else{
                $cartlist =  $this->cartRepository->update($input,$user_id);
            }
            $product = $this->productRepository->detailByID($cartlist['product_id']);
            $productList = $this->productRepository->detail($product,$user_id,$session_id);
            return $this->respondWith(ProductResource::make($productList), "Product update into Cart Successfully", 200, true);

        }else{
            return $this->respondWith([], "Data Input Required", 200, true);
        }
    }

    public function delete(Request $request)
    {
        $input = $request->all();
        if($input && $input['data']){
            $user_id = Auth::check() ? Auth::id() : null;
            if(empty($user_id)){
                $session_id = $request->session_id ? $request->session_id : Session::get('session_id');
                if($session_id){
                    $this->cartRepository->delete($input,$session_id);
                }
            }else{
                $this->cartRepository->delete($input,$user_id);
            }
            return $this->respondWith([], "Product delete from Cart Successfully", 200, true);
        }else{
            return $this->respondWith([], "Data Input Required", 200, true);
        }
    }
}
