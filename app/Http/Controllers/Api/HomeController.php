<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductResource;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\ProductReviewCollection;
use App\Http\Resources\CategoryCollection;
use App\Http\Resources\BannerCollection;
use App\Repositories\CategoryRepository;
use App\Repositories\ProductRepository;
use App\Repositories\BannerRepository;
use App\Models\Product;
use App\Models\Banner;
use Illuminate\Http\Request;
use Session;
use Auth;

class HomeController extends Controller
{

    private $categoryRepository;
    private $productRepository;
    private $bannerRepository;

    public function __construct()
    {
        $this->categoryRepository = new CategoryRepository();
        $this->productRepository = new ProductRepository();
        $this->bannerRepository = new BannerRepository();
    }

    public function index(Request $request)
    {
        $user_id = Auth::check() ? Auth::id() : null;
        $session_id = $request->session_id ? $request->session_id : Session::get('session_id');

        $data['banner'] = BannerCollection::make($this->bannerRepository->index());
        $data['categories'] = CategoryCollection::make($this->categoryRepository->index());
        $data['featured_products'] = ProductCollection::make($this->productRepository->filter_products($user_id,$session_id,1,1));
        $data['popular_products'] = ProductCollection::make($this->productRepository->filter_products($user_id,$session_id,1,2));
        $data['recently_added'] = ProductCollection::make($this->productRepository->filter_products($user_id,$session_id,1,3));
        $data['top_selling'] = ProductCollection::make($this->productRepository->filter_products($user_id,$session_id,2));
        $data['daily_best_sell_featured'] = ProductCollection::make($this->productRepository->filter_products($user_id,$session_id,3,1));
        $data['daily_best_sell_popular'] = ProductCollection::make($this->productRepository->filter_products($user_id,$session_id,3,2));
        $data['daily_best_sell_new'] = ProductCollection::make($this->productRepository->filter_products($user_id,$session_id,3,3));
        $products = ProductCollection::make($this->productRepository->index($user_id,$session_id));
        $productsFiltered = $products->filter(function($product){
            return $product->avg_rating == 5;
        });
        $data['top_rated'] = $productsFiltered;
        return $this->respondWith($data, "data get Successfully.", 200, true);
    }

}
