<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\OrderCollection;
use App\Http\Resources\OrderResource;
use App\Repositories\OrderRepository;
use App\Repositories\CartRepository;
use App\Http\Requests\Order\StoreRequest;
use App\Factories\PaymentProviderFactory;
use App\Models\User;
use App\Models\Cart;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\URL;
use App\Notifications\FirebaseNotification;

class OrderController extends Controller
{

    private $orderRepository;
    private $cartRepository;

    public function __construct()
    {
        $this->provider = 'stripe';
        $this->orderRepository = new OrderRepository();
        $this->cartRepository = new CartRepository();
    }

    public function index(Request $request)
    {
        return OrderCollection::make($this->orderRepository->index());
    }

    public function show(Order $order)
    {
        return OrderResource::make($this->orderRepository->detail($order));
    }

    public function store(StoreRequest $request)
    {
        $user = Auth::user();
        $getCart = $this->cartRepository->getData($user->id);
        if(!empty($getCart)){
            $order = $this->orderRepository->create($getCart,$request);
           
            return $this->respondWith(OrderResource::make($order), "Place Order Successfully", 200, true);
        }else{
            return $this->respondWith([], "Cart is empty", 200, true);
        }
    }

    public function payment(Request $request, Order $order)
	{
        \Log::info($request->all());
        $user = Auth::user();

        if ($order) {
            $order->transaction_id = $request->payment_id ?? '';
            $order->transaction_status = $request->status ?? '';
            $order->save();
        }
        OrderItem::where('order_id', $order->order_id)->update(['confirmed_at'=> Carbon::now()->toDateTimeString()]);
        $user->customer_id = $request->customer_id ?? '';
        $user->payment_method_id = $request->payment_method_id ?? '';
        $user->update();
        
        $user = User::where('id',$order->user_id)->first();
        $order_id = $order->order_id;
        $data = [
            'title'=>'Order Confirmed',
            'description'=>'Hey '.$user->first_name.' '.$user->last_name.', Your Order('.$order_id.') is Confirmed. Please check your order status for more details.'
        ];
        $user->notify(new FirebaseNotification($data));
        $notificationdata = array(
            'user_id'=>$order->user_id,
            'title'=>'Your Order Confirmed',
            'message'=>'Hey '.$user->first_name.' '.$user->last_name.', Your Order('.$order_id.') is Confirmed. Please check your order status for more details.',
        );

        Notification::insert($notificationdata);
        return $this->respondWith([], "Payment Successfully", 200, true);
	}

    public function cancel(OrderItem $order)
    {
        if($order->status != 5){
            $order->status = 6;
            $order->cancelled_at = Carbon::now()->format('Y-m-d h:i:s');
            $order->save();

            $user = User::where('id',$order->user_id)->first();
            $order_id = $order->order_id;
            $data = [
                'title'=>'Order Cancelled',
                'description'=>'Hey '.$user->first_name.' '.$user->last_name.', Your Order('.$order_id.') is Cancelled. Please check your order status for more details'
            ];
            $user->notify(new FirebaseNotification($data));
            $notificationdata = array(
                    'user_id'=>$order->user_id,
                    'title'=>'Your Order Cancelled',
                    'message'=>'Hey '.$user->first_name.' '.$user->last_name.', Your Order('.$order_id.') is Cancelled. Please check your order status for more details',
                );

            Notification::insert($notificationdata);

            return $this->respondWith([], "Order Cancelled Successfully.", 200, true);
        }else{
            return $this->respondWith([], "You can not cancelled order.", 200, true);
        }
    }

    public function invoice($order_item_id)
    {
        $order = $this->orderRepository->orderItemByID($order_item_id);

        return $order;
    }


    public function viewInvoice($order_item_id)
    {
        $url = URL::temporarySignedRoute('show-invoice', now()->addMinutes(20), ['id' => $order_item_id ]);
        return $this->respondWith($url);
    }
}
