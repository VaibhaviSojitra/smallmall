<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\WishlistCollection;
use App\Http\Resources\WishlistResource;
use App\Repositories\WishlistRepository;
use App\Http\Requests\Wishlist\StoreRequest;
use App\Models\Wishlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class WishlistController extends Controller
{

    private $wishlistRepository;

    public function __construct()
    {
        $this->wishlistRepository = new WishlistRepository();
    }

    public function index(Request $request)
    {
        $user_id = Auth::check() ? Auth::id() : null;
        if(empty($user_id)){
            $session_id = $request->session_id ? $request->session_id : Session::get('session_id');
            $data = [];
            if($session_id){
                $data = $this->wishlistRepository->index($session_id);
            }
        }else{
            $data = $this->wishlistRepository->index($user_id);
        }
        return WishlistCollection::make($data);

    }

    public function store(StoreRequest $request)
    {
        $input = $request->all();
        $user_id = Auth::check() ? Auth::id() : null;
        $session_id = $request->session_id;
        if(empty($user_id)){
            if($session_id){
                $session_id = $request->session_id;
            }else{
                $session_id = session()->getId();
                Session::put('session_id', $session_id);
            }
            $data['session_id'] = $session_id;
            $checkWishlist = Wishlist::where('session_id',$session_id)->where('product_id',$input['product_id'])->first();
        }else{
            $data['user_id'] = $user_id;
            $checkWishlist = Wishlist::where('user_id',$user_id)->where('product_id',$input['product_id'])->first();
        }
        if(empty($checkWishlist)){
            $data['product_id'] = $input['product_id'];
            $wishlist = $this->wishlistRepository->create($data);
            
            return $this->respondWith(WishlistResource::make($wishlist), "Product Add into Wishlist Successfully", 200, true);
        }else{
            return $this->respondWith([], "Product already into Wishlist", 200, true);
        }
        
    }

    public function delete(Request $request)
    {
        $input = $request->all();
        if($input && $input['data']){
            $user_id = Auth::check() ? Auth::id() : null;
            if(empty($user_id)){
                $session_id = $request->session_id ? $request->session_id : Session::get('session_id');
                if($session_id){
                    $this->wishlistRepository->delete($input,$session_id);
                }
            }else{
                $this->wishlistRepository->delete($input,$user_id);
            }
            return $this->respondWith([], "Product delete from Wishlist Successfully", 200, true);
        }else{
            return $this->respondWith([], "Data Input Required", 200, true);
        }
    }
}
