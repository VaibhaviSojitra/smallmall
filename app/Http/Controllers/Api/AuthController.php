<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Auth\forgotPasswordRequest;
use App\Http\Requests\Auth\resetPasswordRequest;
use App\Http\Resources\UserResource;
use App\Repositories\CartRepository;
use App\Repositories\WishlistRepository;
use App\Models\User;
use App\Models\PasswordReset;
use App\Models\VerifyUser;
use App\Mail\VerifyEmail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\ForgotPassword;
use Carbon\Carbon;
use Session;

class AuthController extends Controller
{
    private $cartRepository;
    private $wishlistRepository;

    public function __construct()
    {
        $this->cartRepository = new CartRepository();
        $this->wishlistRepository = new WishlistRepository();

    }
    /**
     * Create User
     * @param Request $request
     * @return User 
     */
    public function createUser(Request $request)
    {
        try {
            //Validated
            $validateUser = Validator::make($request->all(), 
            [
                'name' => 'required',
                'email' => 'required|email|unique:users,email',
                'password' => 'required',
                'mobile' => 'required'
            ]);

            if($validateUser->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }

            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'mobile' => $request->mobile,
                'password' => Hash::make($request->password)
            ]);
            $session_id = $request->session_id;
            if($session_id){

                $cartData = $this->cartRepository->index($session_id);
                if(count($cartData['cart'])>0){
                    $input = $storeData = [];
                    foreach($cartData['cart'] as $key => $value){
                        $input['id'] = $value['id'];
                        $storeData['product_id'] = $value['product_id'];
                        $storeData['quantity'] = $value['quantity'];
                        $storeData['user_id'] = $user->id;
                        $checkCart = $this->cartRepository->getData($user->id,$value);
                        if($checkCart){
                            $this->cartRepository->update($storeData,$user->id,1);
                        }else{
                            $cartList = $this->cartRepository->create($storeData);
                        }
                        $this->cartRepository->delete($input,$session_id,1);
                    }
                }
            }
            $user->assignRole('customer');

            $verifyUser = VerifyUser::create([
                'user_id' => $user->id,
                'token' => sha1(time())
            ]);
            $user['verifyUser'] = $verifyUser;
            \Mail::to($user->email)->send(new VerifyEmail($user));
            unset($user['verifyUser']);
            return response()->json([
                'status' => true,
                'message' => 'User Created Successfully. We have sent verification mail. please activate account.',
                'user' => $user,
                'token' => $user->createToken("API TOKEN")->plainTextToken
            ], 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }

    /**
     * Login The User
     * @param Request $request
     * @return User
     */
    public function loginUser(Request $request)
    {
        try {
            $validateUser = Validator::make($request->all(), 
            [
                'email' => 'required|email',
                'password' => 'required'
            ]);

            if($validateUser->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }

            // if(!Auth::attempt($request->only(['email', 'password']))){
            $user = User::where('email', $request->email)->first();
            if (!$user || !Hash::check($request->password, $user->password)) {
                return response()->json([
                    'status' => false,
                    'message' => 'Email & Password does not match with our record.',
                ], 401);
            }
            if($user->status == 'BLOCK'){
                return response()->json([
                    'status' => false,
                    'message' => 'Oppes! Your account is blocked by admin.',
                ], 401);
            }
            if(!$user->hasRole('customer')){
                return response()->json([
                    'status' => false,
                    'message' => 'Oppes! Your account is not a user account.',
                ], 401);
            }

            $session_id = $request->session_id;
            if($session_id){

                $cartData = $this->cartRepository->index($session_id);
                if(count($cartData['cart'])>0){
                    $input = $storeData = [];
                    foreach($cartData['cart'] as $key => $value){
                        $input['id'] = $value['id'];
                        $storeData['product_id'] = $value['product_id'];
                        $storeData['quantity'] = $value['quantity'];
                        $storeData['user_id'] = $user->id;
                        $checkCart = $this->cartRepository->getData($user->id,$value);
                        if($checkCart){
                            $this->cartRepository->update($storeData,$user->id,1);
                        }else{
                            $cartList = $this->cartRepository->create($storeData);
                        }
                        $this->cartRepository->delete($input,$session_id,1);
                    }
                }
            }

            return response()->json([
                'status' => true,
                'message' => 'User Logged In Successfully',
                'user' => UserResource::make($user),
                'token' => $user->createToken("API TOKEN")->plainTextToken
            ], 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }

        
    public function forgotPassword(forgotPasswordRequest $request)
    {
    
        $user = User::where('email', $request->get('email'))->first();

        if (!empty($user)) {
            PasswordReset::where('email', $request->get('email'))->delete();
            $passwordReset = new PasswordReset;
            $passwordReset->email = $request->get('email');
            $passwordReset->token = mt_rand(100000, 999999);
            $passwordReset->created_at = Carbon::now();
            $passwordReset->save();

            Mail::to($request->get('email'))->send(new ForgotPassword($user, $passwordReset));
            return $this->respondWith([], "An Email Containing Code has been Sent to Your Verified Email Address.", 200, true);

        } else {
            return $this->respondWith([], "User not found", 200, true);
        }
   
    }

    public function resetPassword(resetPasswordRequest $request)
    {

        $check = PasswordReset::where('email', $request->get('email'))->where('token', $request->get('otp'))->first();
        if (!empty($check)) {
            $user = User::where('email', $request->get('email'))->first();
            $user->password = bcrypt($request->get('password'));
            $user->save();
            PasswordReset::where('email', $request->get('email'))->where('token', $request->get('otp'))->delete();
            return $this->respondWith([], "Password changed successfully.", 200, true);
        } else {
            return $this->respondWith([], "Please enter valid OTP.", 404, true);
        }

    }

}
