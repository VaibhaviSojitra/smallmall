<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\UserAddressCollection;
use App\Repositories\UserRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\ProductRepository;
use App\Repositories\OrderRepository;
use App\Repositories\CartRepository;
use App\Factories\PaymentProviderFactory;
use App\Models\Address;
use App\Models\Order;
use App\Models\OrderItem;
use Auth;

class OrderController extends Controller
{
    private $userRepository;
    private $categoryRepository;
    private $productRepository;
    private $orderRepository;
    private $cartRepository;

    protected $provider;

    public function __construct()
    {
        $this->provider = 'stripe';

        $this->userRepository = new UserRepository();
        $this->categoryRepository = new CategoryRepository();
        $this->productRepository = new ProductRepository();
        $this->orderRepository = new OrderRepository();
        $this->cartRepository = new CartRepository();
    }

    public function cart()
    {
        return view('web.pages.cart');
    }

    public function wishlist()
    {
        return view('web.pages.wishlist');
    }

    public function checkout()
    {
        if(Auth::user()){
            $user = Auth::user();
            $userAddress = $this->userRepository->getUserAddress();
            $cart = $this->cartRepository->index($user->id);
            return view('web.pages.checkout',compact('userAddress','cart'));
        }else{
            return redirect()->to('/login');
        }
    }

    public function getCards()
    {
        $paymentProvider = PaymentProviderFactory::get($this->provider);
        $cards = $paymentProvider->getCards(Auth::user());
        return response()->json(['message' => '', 'success' => true, 'data' => $cards]);
    }

    public function placeOrder(Request $request)
    {
        $user = Auth::user();
        $getCart = $this->cartRepository->getData($user->id);
        $cart = $this->cartRepository->index($user->id);
        if(!empty($getCart)){
            $order = $this->orderRepository->create($getCart,$request,$cart['total']);
        }
        return response()->json(['order_id' => $order->order_id, 'success' => true, 'message' => 'Order Created']);
    }

    public function payment(Request $request)
	{
        $user = Auth::user();
        $transactionData = [];
        $userAddress = $this->userRepository->getUserAddress($request->billing_address_id);
       $paymentProvider = PaymentProviderFactory::get($this->provider);
        $data = $paymentProvider->createPayment($request, $userAddress, Auth::user()); 
        $transactionData['transactionId'] = $data->id;
        $transactionData['status'] = $data->status;

        $orderData = Order::where('order_id', $request->order_id)->where('transaction_status', '!=', '1')->first();
        if (!empty($orderData)) {
            $orderData->transaction_id = $transactionData['transactionId'];
            $orderData->transaction_status = $transactionData['status'];
            $orderData->save();
        }

        return response()->json(['success' => true, 'data' => $data, 'message' => 'Payment Successfully']);
	}


    public function viewOrderDetails($order_id)
    {
        if(Auth::user()){
            $order = $this->orderRepository->detailByOrderID($order_id);
            return view('web.pages.order-details',compact('order'));
        }else{
            return redirect()->to('/login');
        }
    }


    public function viewInvoice($order_item_id)
    {
        $order = $this->orderRepository->orderItemByID($order_item_id);
        return view('web.pages.invoice',compact('order'));
    }


    public function viewSignedInvoice(Request $request, $order_item_id)
    {
        if (! $request->hasValidSignature()) {
            abort(401);
        }

        $order = $this->orderRepository->orderItemByID($order_item_id);
        return view('web.pages.invoice',compact('order'));
    }

}
