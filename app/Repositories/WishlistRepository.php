<?php

namespace App\Repositories;

use App\Models\Wishlist;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class WishlistRepository
{

    public function index($userIdOrSessionId)
    {
        $wishlist = Wishlist::where(function($query) use ($userIdOrSessionId) {
                $query->where('user_id', $userIdOrSessionId);
                $query->orWhere('session_id', $userIdOrSessionId);
            })->get()->loadMissing([
            'product','product.medias','product.seller'
        ]);
        return $wishlist;
    }

    public function create($data)
    {
        $wishlist = Wishlist::create($data);      
        return $wishlist;
    }


    public function update($input,$userIdOrSessionId)
    {
        foreach($input['data'] as $value){
            $checkWishlist = Wishlist::where(function($query) use ($userIdOrSessionId) {
                $query->where('user_id', $userIdOrSessionId);
                $query->orWhere('session_id', $userIdOrSessionId);
            })->where('id',$value['id'])->first();
            if(!empty($checkWishlist)){
                Wishlist::where(function($query) use ($userIdOrSessionId) {
                $query->where('user_id', $userIdOrSessionId);
                $query->orWhere('session_id', $userIdOrSessionId);
            })->where('id',$value['id'])->update($data);
            }
        }
      
        return true;
    }

    public function delete($input,$userIdOrSessionId)
    {
        foreach($input['data'] as $value){
            $checkWishlist = Wishlist::where(function($query) use ($userIdOrSessionId) {
                $query->where('user_id', $userIdOrSessionId);
                $query->orWhere('session_id', $userIdOrSessionId);
            })->where('id',$value['id'])->first();
            if(!empty($checkWishlist)){
                Wishlist::where(function($query) use ($userIdOrSessionId) {
                $query->where('user_id', $userIdOrSessionId);
                $query->orWhere('session_id', $userIdOrSessionId);
            })->where('id',$value['id'])->delete();
            }
        }
      
        return true;
    }
}
