<?php

namespace App\Repositories;

use App\Models\Banner;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BannerRepository
{

    public function index()
    {
        return Banner::where('status',1)->get();

    }

}