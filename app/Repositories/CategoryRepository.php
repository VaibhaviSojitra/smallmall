<?php

namespace App\Repositories;

use App\Models\Product;
use App\Models\Category;
use Database\Seeders\CategorySeeder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CategoryRepository
{
    public function index()
    {
        $category = Category::get()->loadMissing([
            'products'
        ]);
        return $category;
    }

    public function categoryBySlug($slug)
    {
        $category = Category::where('slug',$slug)->first();
        return $category;
    }

    public function create($request)
    {
        $input = $request->validated();
        if ($request->hasFile('icon')) {
            $path = Storage::disk('category')->putFile('/', $request->icon);
            $input['icon'] = $path;
        }
        return Category::create($input);
    }

    public function update(Category $category, $request)
    {
        $input = $request->validated();
        if ($request->hasFile('icon')) {
            $path = Storage::disk('category')->putFile('/', $request->icon);
            $input['icon'] = $path;
        }
        $category->update($input);
        $category->refresh();

        return $category;
    }

    public function delete(Category $category)
    {
        return $category->delete();
    }
}
