<?php

namespace App\Repositories;

use App\Models\User;
use App\Models\Address;
use App\Models\Otp;
use App\Models\VendorQuestionnaire;
use App\Models\SubscribedBrand;
use App\Models\SubscribeSeller;
use App\Models\Notification;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class UserRepository
{
    public function get($user_id, $mobile)
    {
        $user = User::where('id',$user_id)->where('mobile', $mobile)->first();
        return $user;
    }

    public function index($role)
    {
        if($role == 'seller'){
            return auth()->check() ?
                User::role($role)->with(['subscribedBrands' => fn($q) => $q->where('user_id', auth()->user()->id) ])->withCount('products')->paginate()
                : User::role($role)->withCount('products')->paginate();
        }else{
            return User::role($role)->paginate();
        }
    }

    public function details($user)
    {
        $user->loadMissing([
            'addresses'
        ]);
        return $user;
    }

    public function getUserByID($user_id)
    {
        $user =  User::where('id',$user_id)->first();
        return $user;
    }

    public function update($user_id,$data)
    {
        User::where('id',$user_id)->update($data);
        $user =  User::where('id',$user_id)->first();
        return $user;
    }

    public function getOTPByIDs($user_id,$id="")
    {
        if($id){
            $otp = Otp::where('otp', $id)->where('user_id', $user_id)->where('status',1)->latest()->first();
        }else{
            $otp = Otp::where('user_id', $user_id)->latest()->first();
        }
        return $otp;
    }

    public function addOTP($data)
    {
        Otp::create($data);
        return true;
    }

    public function updateOTP($user_id,$data)
    {
        Otp::where('user_id', $user_id)->update(['status'=>0]);
        return true;
    }

    public function getUserAddress($id="")
    {
        $user = Auth::user();
        if($id){
            $useraddress = Address::where('id',$id)->where('user_id',$user->id)->first();
        }else{
            $useraddress = Address::where('user_id',$user->id)->get();
        }
        return $useraddress;
    }

    public function addUserAddress($data)
    {
        $storeAddress = Address::create($data);
        $address = Address::where('id',$storeAddress->id)->first();
        return $address;
    }

    public function updateUserAddress($data,$user_id,$id="")
    {
        if($id){
           Address::where('id',$id)->where('user_id',$user_id)->update($data);
            $address = Address::where('user_id',$user_id)->where('id',$id)->first();
            return $address;
        }else{
            Address::where('user_id',$user_id)->update($data);
            return true;
        }
    }

    public function deleteUserAddress($user_id,$id)
    {
        Address::where('id',$id)->where('user_id',$user_id)->delete();
        return true;
    }

    public function getVendorQuestionnaire($user_id="")
    {
        if($user_id){
            $vendorQuestionnaire = VendorQuestionnaire::where('user_id',$user_id)->first();
        }else{
            $vendorQuestionnaire = VendorQuestionnaire::whereNot('status', 1)->paginate();
        }
        return $vendorQuestionnaire;
    }

    public function getApprovedVendorQuestionnaire()
    {
        $vendorQuestionnaire = VendorQuestionnaire::where('status', 1)->paginate();
        return $vendorQuestionnaire;
    }

    public function addQuestionnaire($request)
    {
        $user_id = Auth::user()->id;
        $input = $request->validated();
        if ($request->hasFile('identity_card')) {
            $path = Storage::disk('vendor')->putFile('/', $request->identity_card);
            $input['identity_card'] = $path;
        }
        if ($request->hasFile('address_proof')) {
            $path = Storage::disk('vendor')->putFile('/', $request->address_proof);
            $input['address_proof'] = $path;
        }
        $input['user_id'] = $user_id;
        $checkVendorQuestionnaire = VendorQuestionnaire::where('user_id',$user_id)->first();
        if($checkVendorQuestionnaire){
            $vendorQuestionnaire = VendorQuestionnaire::where('user_id',$user_id)->update(
                Arr::only($input, VendorQuestionnaire::getFillables())
            );
        }else{
            $vendorQuestionnaire = VendorQuestionnaire::Create(
                Arr::only($input, VendorQuestionnaire::getFillables())
            );
            $vendorQuestionnaire->refresh();
        }
        $vendorQuestionnaire = VendorQuestionnaire::where('user_id',$user_id)->first();

        return $vendorQuestionnaire;
    }
    public function subscribedUsers($role)
    {
        if($role == 'seller'){
            return User::role('customer')->whereIn('id', SubscribedBrand::where('seller_id', auth()->user()->id)->pluck('user_id') )->withCount('products')->paginate();
        }else{
            return User::role($role)->paginate();
        }

    }
    public function notification($user_id)
    {
        $notification = Notification::where('user_id',$user_id)->paginate();
        return $notification;
    }
}
