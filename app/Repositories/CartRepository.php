<?php

namespace App\Repositories;

use App\Models\Cart;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CartRepository
{

    public function index($userIdOrSessionId)
    {
        $cart = Cart::where(function($query) use ($userIdOrSessionId) {
            $query->where('user_id', $userIdOrSessionId);
            $query->orWhere('session_id', $userIdOrSessionId);
        })->get()->loadMissing([
            'product','product.medias','product.seller'
        ]);
        $total = 0;
        if (count($cart) > 0) {
            foreach($cart as $k => $value){
                $total += ((int)$value['product']['selling_price']*(int)$value['quantity']);    
                $cart[$k]['product']['avg_rating'] = $value['product']->reviews()->avg('rating') . '';
               }
        }
        $Cartlist['cart'] = $cart;
        $Cartlist['total'] = $total;
        return $Cartlist;
    }

    public function getData($userIdOrSessionId, $input = null)
    {
        if ($input) {
            $cart =  Cart::where(function($query) use ($userIdOrSessionId) {
                $query->where('user_id', $userIdOrSessionId);
                $query->orWhere('session_id', $userIdOrSessionId);
            })->where('product_id', $input['product_id'])->first();
        } else {
            $cart = Cart::where(function($query) use ($userIdOrSessionId) {
                $query->where('user_id', $userIdOrSessionId);
                $query->orWhere('session_id', $userIdOrSessionId);
            })->get();
        }
        return $cart;
    }

    public function create($data)
    {
        return Cart::create($data);    
    }
    
    public function update($input,$userIdOrSessionId,$flag=0)
    {
        if($flag == 1){
            $data['quantity'] = $input['quantity'];
            Cart::where(function($query) use ($userIdOrSessionId) {
            $query->where('user_id', $userIdOrSessionId);
            $query->orWhere('session_id', $userIdOrSessionId);
            })->where('product_id',$input['product_id'])->update($data);
           
            return true;
        }else{
            foreach($input['data'] as $value){
                $checkCart = Cart::where(function($query) use ($userIdOrSessionId) {
                    $query->where('user_id', $userIdOrSessionId);
                    $query->orWhere('session_id', $userIdOrSessionId);
                })->where('id',$value['id'])->first();

                if(!empty($checkCart)){
                    $data['quantity'] = $value['quantity'];
                    Cart::where(function($query) use ($userIdOrSessionId) {
                        $query->where('user_id', $userIdOrSessionId);
                        $query->orWhere('session_id', $userIdOrSessionId);
                    })->where('id',$value['id'])->update($data);
                }
            }
            return $checkCart;
        }
    
    }

    public function delete($input,$userIdOrSessionId,$flag=0)
    {
        if($flag == 1){
            Cart::where(function($query) use ($userIdOrSessionId) {
                $query->where('user_id', $userIdOrSessionId);
                $query->orWhere('session_id', $userIdOrSessionId);
            })->where('id',$input['id'])->delete();

        }else{
            foreach($input['data'] as $value){
                $checkCart = Cart::where(function($query) use ($userIdOrSessionId) {
                    $query->where('user_id', $userIdOrSessionId);
                    $query->orWhere('session_id', $userIdOrSessionId);
                })->where('id',$value['id'])->first();
                if(!empty($checkCart)){
                    Cart::where(function($query) use ($userIdOrSessionId) {
                    $query->where('user_id', $userIdOrSessionId);
                    $query->orWhere('session_id', $userIdOrSessionId);
                })->where('id',$value['id'])->delete();
                }
            }

        }
      
        return true;
    }
}
