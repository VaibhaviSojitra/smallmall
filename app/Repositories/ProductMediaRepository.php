<?php

namespace App\Repositories;

use App\Models\Product;
use Illuminate\Support\Facades\Storage;

class ProductMediaRepository {


    public function store(Product $product, $media) 
    {
        $media = $product->medias()->create([
            'type' => 0,
            'path' => Storage::disk('product')->putFile("", $media)
        ]);
        return $media;
    }



}