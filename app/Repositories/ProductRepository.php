<?php

namespace App\Repositories;

use App\Models\Product;
use App\Models\Category;
use App\Models\Cart;
use App\Models\Wishlist;
use App\Models\ProductReview;
use App\Models\ProductVariant;
use App\Models\ProductSeller;
use App\Models\Order;
use App\Models\OrderItem;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Storage;
use Carbon\Carbon;
use Session;
use Str;

class ProductRepository
{
    public function index($user_id="",$session_id="")
    {
        $products = Product::with([
            'seller', 'medias', 'reviews', 'category', 'category.products'
        ])->where('is_approved',1)->activeSellerProducts()->paginate();
        $products = Product::commonProductsData($products,$user_id,$session_id);
        return $products;
    }

    public function getAdminProducts(){
        $products = Product::with([
            'seller', 'medias', 'reviews', 'category', 'category.products'
        ]);
        if(auth()->user()->hasRole('seller')){
            $products = $products->where('seller_id', auth()->user()->id);
        }
        $products = $products->activeSellerProducts()->paginate();
        $products = Product::commonProductsData($products,auth()->user()->id ?? '');
        return $products;
    }

    public function detailBySlug($slug)
    {
       return Product::where('slug',$slug)->where('is_approved',1)->first();
    }

    public function detailByID($id)
    {
       return Product::where('id',$id)->where('is_approved',1)->first();
    }

    public function categoryBySlug($slug)
    {
       return Category::where('slug',$slug)->first();
    }

    public function detail($product,$user_id="",$session_id="")
    {
        $product->loadMissing([
            'seller', 'medias', 'reviews','reviews.user', 'category', 'category.products'
        ]);

        $product->cart_id = "";
        $product->is_cart = false;
        $product->quantity = 1;

        $CartData = $WishlistData = [];

        if($user_id){
            $CartData =  Cart::where('user_id',$user_id)->where('product_id',$product->id)->first();
        }else{
            if($session_id){
                $CartData =  Cart::where('session_id',$session_id)->where('product_id',$product->id)->first();
            }
        }
        if($CartData){
            $product->cart_id = $CartData['id'];
            $product->is_cart = true;
            $product->quantity = $CartData['quantity'];
        }
        $product->wishlist_id = "";
        $product->is_wishlist = false;
        if($user_id){
            $WishlistData =  Wishlist::where('user_id',$user_id)->where('product_id',$product->id)->first();
        }else{
            if($session_id){
                $WishlistData =  Wishlist::where('session_id',$session_id)->where('product_id',$product->id)->first();
            }
        }
        if($WishlistData){
            $product->wishlist_id = $WishlistData['id'];
            $product->is_wishlist = true;
        }
        $product->avg_rating = 0;
        if ($product->reviews()->count() > 0 ){
            $product->avg_rating = $product->reviews()->avg('rating') . '';
        }

        return $product;
    }

    public function productsByCategory($category_id,$user_id = "",$session_id = "",$sort_by="")
    {
        $query = Product::with([
            'seller', 'medias', 'reviews', 'category', 'category.products'
        ])->where('category_id',$category_id)->where('is_approved',1);

        if($sort_by){
            if($sort_by == 1){
                $query->orderBy('selling_price','ASC');
            }
            if($sort_by == 2){
                $query->orderBy('selling_price','DESC');
            }
            if($sort_by == 3){
                $query->where('is_featured',1);
            }
            if($sort_by == 4){
                $query->orderBy('created_at','DESC');
            }
        }

        $products = $query->activeSellerProducts()->paginate();
        $products = Product::commonProductsData($products,$user_id,$session_id);

        return $products;
    }

    public function productsBySeller($seller_id,$user_id = "",$session_id = "",$sort_by="")
    {
        $query = Product::with([
            'seller', 'medias', 'reviews', 'category', 'category.products'
        ])->where('seller_id',$seller_id)->where('is_approved',1);

        if($sort_by){
            if($sort_by == 1){
                $query->orderBy('selling_price','ASC');
            }
            if($sort_by == 2){
                $query->orderBy('selling_price','DESC');
            }
            if($sort_by == 3){
                $query->where('is_featured',1);
            }
            if($sort_by == 4){
                $query->orderBy('created_at','DESC');
            }
        }

        $products = $query->activeSellerProducts()->paginate();
        $products = Product::commonProductsData($products,$user_id,$session_id);

        return $products;
    }

    public function relatedProductsByCategory($product_id,$category_id,$user_id="",$session_id="")
    {
        $products = Product::with([
            'seller', 'medias', 'reviews', 'category', 'category.products'
        ])->where('id','!=',$product_id)->where('category_id',$category_id)->where('is_approved',1)->take(4)->get();

        $products = Product::commonProductsData($products,$user_id,$session_id);

        return $products;
    }

    public function filter_products($user_id="",$session_id = "", $flag, $secondflag=0)
    {
        if($flag == 1){
            $query = Product::with([
                'seller', 'medias', 'reviews', 'category', 'category.products'
            ]);
            if($secondflag == 1){
                $query->where('is_featured',1);
            }
            if($secondflag == 2){
                $query->where('is_popular',1);
            }
            if($secondflag == 3){
                $query->orderBy('created_at','DESC');
            }
            $products = $query->activeSellerProducts()->paginate();
        }
        if($flag == 2){
            $products = Product::with([
                'seller', 'medias', 'reviews', 'category', 'category.products'
            ])->select([
                'products.*',
                DB::raw('SUM(order_items.quantity) as total_sales'),
                DB::raw('SUM(IFNULL(orders.total_amount, products.selling_price) * order_items.quantity) AS total_price'),
            ])
            ->join('order_items', 'order_items.product_id', '=', 'products.id')
            ->join('orders', 'order_items.order_id', '=', 'orders.order_id')
            ->where('products.is_approved',1)
            ->activeSellerProducts()
            ->groupBy('products.id')
            ->orderByDesc('total_sales')
            ->paginate();
        }
        if($flag == 3){
            $date = \Carbon\Carbon::today()->subDays(30);

            // $max=OrderItem::join('orders','orders.order_id', '=','order_items.order_id')
            //     ->select('order_items.product_id')
            //     ->whereDate('orders.ordered_at','>=', $date)
            //     ->groupBy('order_items.order_id')
            //     ->max('order_items.quantity');

            $daily_sell=Order::join('order_items','orders.order_id', '=','order_items.order_id')
                // ->where('order_items.quantity','=',$max)
                ->whereDate('orders.ordered_at','>=', $date)
                ->groupBy('order_items.order_id')
                ->pluck('order_items.product_id');

            $query = Product::with([
                'seller', 'medias', 'reviews', 'category', 'category.products'
            ])->whereIn('id',$daily_sell);

            if($secondflag == 1){
                $query->where('is_featured',1);
            }
            if($secondflag == 2){
                $query->where('is_popular',1);
            }
            if($secondflag == 3){
                $query->orderBy('created_at','DESC');
            }
            $products = $query->activeSellerProducts()->paginate();
        }
        $products = $products->filter(function($product){
            return $product->is_approved == 1;
        })->values();
        $products = Product::commonProductsData($products,$user_id,$session_id);
        return $products;
    }

    public function reviews(Product $product)
    {
        return $product->reviews()->with('user')->paginate();
    }

    public function reviewsAdd(Product $product,$items)
    {
        $data['product_id'] = $product->id;
        $data['user_id'] = Auth::user()->id;
        $data['rating'] = $items->rating;
        $data['review'] = $items->review;
        ProductReview::insert($data);
        return $product->reviews()->with('user')->paginate();
    }

    public function reviewsUpdate(Product $product,ProductReview $review,$items)
    {
        $review->rating = $items->rating;
        $review->review = $items->review;
        $review->save();
        return $product->reviews()->with('user')->paginate();
    }

    public function reviewsDelete(Product $product,ProductReview $review)
    {
        $review->delete();
        return $product->reviews()->with('user')->paginate();
    }

    public function reviewSummary(Product $product)
    {
        $totalCount = $product->reviews()->count();
        $reviews = $product->reviews()->select(
            'rating',
            DB::raw('count(*) as total'),
            DB::raw("count(*) / $totalCount * 100 AS percentage")
        )->groupBy('rating')->orderBy('rating')->get()->toArray();
        $availableRatings = [];
        $summary = array_fill(0, 5, []);
        foreach ($reviews as $review) {
            $summary[(int)$review['rating']-1] = $review;
            $availableRatings [] = $review['rating'];
        }
        for ($i=1; $i<=5; $i++) {
            if (!in_array($i, $availableRatings)) {
                $summary[$i-1] = [
                    "rating" => $i,
                    "total" => 0,
                    "percentage" => 0
                ];
            }
        }

        return [
            "total" => $totalCount,
            "summary" => ($summary)
        ];
    }

    public function store(array $input)
    {
        DB::beginTransaction();
        //TODO: remove 1 once auth setup is working.
        $input['seller_id'] = Auth::id() ?? 1;
        $input['slug'] = Str::slug($input['title']);
        $product = Product::Create(
            Arr::only($input, Product::getFillables())
        );

        foreach ($input['media'] as $media) {
            $product->medias()->create([
                'type' => 0,
                'path' => Storage::disk('product')->putFile("", $media)
            ]);
        }

        DB::commit();
        $product->refresh();
        return $product;
    }

    public function update(Product $product, array $input)
    {
        $input['slug'] = Str::slug($input['title']);
        $product->update(
            Arr::only($input, Product::getFillables())
        );
        return true;
    }

    public function search($search="",$sort_by="",$min_price="",$max_price="",$avg_rating="",$category_id="",$user_id="",$session_id = "")
    {
        $query = Product::select('products.*')->with([
            'seller', 'medias', 'reviews', 'category', 'category.products'
        ])->leftjoin('categories as C', 'products.category_id', 'C.id');

        if($sort_by){
            if($sort_by == 1){
                $query->orderBy('products.selling_price','ASC');
            }
            if($sort_by == 2){
                $query->orderBy('products.selling_price','DESC');
            }
            if($sort_by == 3){
                $query->where('products.is_featured',1);
            }
            if($sort_by == 4){
                $query->orderBy('products.created_at','DESC');
            }
        }

        if (!empty($min_price) && !empty($max_price)) {
            $query->whereBetween('products.selling_price', [$min_price, $max_price]);
        } elseif (!empty($min_price)) {
            $query->where('products.selling_price', '>=', $min_price);
        } elseif (!empty($max_price)) {
            $query->where('products.selling_price', '<=', $max_price);
        }

        if($avg_rating > 0){
            $query->selectRaw('AVG(PR.rating) AS average_rating')->leftjoin('product_reviews as PR', 'products.id', 'PR.product_id')->havingRaw('AVG(PR.rating) >= ?', [$avg_rating]);
            // ->where('average_rating >= ', $avg_rating);
        }
        if(!empty($search) && !empty($category_id)){
            $query->where(function ($query) use($search) {
                $query->where('products.title', 'LIKE',"%{$search}%")->orWhere('C.name', 'LIKE',"%{$search}%");
            });
            $query->where('products.category_id', $category_id);
        }else if(empty($search) && !empty($category_id)){
            $query->where('products.category_id', $category_id);
        }else if(!empty($search) && empty($category_id)){
            $query->where(function ($query) use($search) {
                $query->where('products.title', 'LIKE',"%{$search}%")->orWhere('C.name', 'LIKE',"%{$search}%");
            });
        }
        // if($avg_rating > 0){
        //     $query = $query->filter(function($product)use($avg_rating){
        //         return $product->is_approved == 1 && $product->reviews->avg('rating') >= $avg_rating;
        //     })->values();
        // }else{
        //     $query = $query->filter(function($product){
        //         return $product->is_approved == 1;
        //     })->values();
        // }
        $products = $query->where('products.is_approved',1)->groupBy('products.id')->activeSellerProducts()->paginate();



        $products = Product::commonProductsData($products,$user_id,$session_id);

        return $products;
    }

    public function delete(Product $product)
    {
        return $product->delete();
    }

}
