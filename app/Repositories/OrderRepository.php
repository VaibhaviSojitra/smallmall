<?php

namespace App\Repositories;

use App\Models\User;
use App\Models\Product;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Cart;
use App\Models\Wishlist;
use App\Models\Notification;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Notifications\FirebaseNotification;

class OrderRepository
{

    public function index()
    {
        $user = Auth::user();
        $order = Order::where('user_id',$user->id)->get()->loadMissing([
            'user', 'order_items', 'order_items.product','order_items.product.seller','billing_address','shipping_address', 'order_items.product.medias'
        ]);
        return $order;
    }

    public function detail($order)
    {
        $order->loadMissing([
            'user', 'order_items', 'order_items.product','order_items.product.seller','billing_address','shipping_address', 'order_items.product.medias'
        ]);
        return $order;
    }

    public function detailByOrderID($order_id)
    {
        return Order::with([
            'user', 'order_items', 'order_items.product','order_items.product.seller','billing_address','shipping_address', 'order_items.product.medias'
        ])->where('order_id',$order_id)->first();
    }

    public function create($cart,$request,$total_amount=0)
    {
        if($request->get('payment_type') == 'on'){
            $payment_type = 1;
        }else if($request->get('payment_type')){
            $payment_type = $request->get('payment_type');
        }else{
            $payment_type = 0;
        }

        $total_amount = $request->get('total_amount') ? $request->get('total_amount'): $total_amount;
        $shipping_charge = $request->get('shipping_charge') ?  $request->get('shipping_charge') : 0;
        $billing_address_id = $request->get('billing_address_id');
        $is_shipping_address = $request->get('is_shipping_address');
        $shipping_address_id = $request->get('shipping_address_id');

        $user = Auth::user();
        $order_id = Order::generateRandomOrderId();
        $data['order_id'] = $order_id;
        $data['total_amount'] = $total_amount;
        $data['shipping_charge'] = $shipping_charge ? $shipping_charge : 0;
        $data['payment_type'] = $payment_type;
        $data['billing_address_id'] = $billing_address_id;
        $data['shipping_address_id'] = $shipping_address_id;
        $data['transaction_id'] = null;
        $data['transaction_status'] = 'pending';
        $data['ordered_at'] = Carbon::now()->format('Y-m-d h:i:s');
        $data['user_id'] = $user->id;
        $order = Order::create($data);

        foreach($cart as $value){
            $product = Product::where('id',$value->product_id)->first();
            $orderItem['user_id'] = $value->user_id;
            $orderItem['order_id'] = $order_id;
            $orderItem['product_id'] = $value->product_id;
            $orderItem['quantity'] = $value->quantity;
            $orderItem['price'] = $product->selling_price;
            $orderItem['status'] = 0;
            OrderItem::create($orderItem);
            Wishlist::where('user_id',$user->id)->where('product_id',$value->product_id)->delete();
        }

        Cart::where('user_id',$user->id)->delete();
        $user = User::where('id',$user->id)->first();
        $data = [
            'title'=>'Order Placed',
            'description'=>'Hey '.$user->first_name.' '.$user->last_name.', Your Order('.$order_id.') is Placed Successfully. Please check your order status for more details.'
        ];
        $user->notify(new FirebaseNotification($data));
        $notificationdata = array(
            'user_id'=>$user->id,
            'title'=>'Your Order Placed',
            'message'=>'Hey '.$user->first_name.' '.$user->last_name.', Your Order('.$order_id.') is Placed Successfully. Please check your order status for more details.',
        );

        Notification::insert($notificationdata);
        return $order;
    }


    public function allSellerOrders()
    {
        $user = auth()->user();

        if($user->hasRole('seller'))
        {
            $order = Order::with(['user', 'order_items', 'order_items.product.seller','billing_address','shipping_address', 'order_items.product.medias'])
                        ->whereHas('order_items.product', function($q) use ($user){
                            $q->where('seller_id', $user->id);
                        })
                        ->paginate();
            return $order;
        }

        $order = Order::with(['user', 'order_items', 'order_items.product.seller','billing_address','shipping_address', 'order_items.product.medias', 'order_items.product'])->paginate();

        return $order;
    }

    public function orderItemByID($order_item_id){
        $order = OrderItem::with([
            'user', 'product','product.seller','order','order.billing_address','order.shipping_address', 'product.medias'
        ])->where('id',$order_item_id)->first();
        return $order;
    }
}
