<?php 

namespace App\Factories;

use App\Services\PaymentProvider\StripePay;
use Illuminate\Support\Str;

use Exception;

class PaymentProviderFactory {

    /**
     * @param string $provider
     * @return App\Services\PaymentProvider\StripePay;
     */
    public static function get($provider) {
        $provider = Str::lower($provider);
        switch($provider) {
            case 'stripe': 
                return new StripePay();
                break;
        }
        throw new Exception("Unsupported Payment Method");
    }
}
