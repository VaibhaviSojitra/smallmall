<?php

namespace App\Services\PaymentProvider;

use App\Models\Order;
use App\Models\SubscriptionPlan;
use App\Models\User;
use App\Models\Address;
use App\Models\UserSubscription;

abstract class Base {

    abstract public function getProviderName();
    abstract public function createPayment($request,Address $address, User $user);
    abstract public function initiateSubscription($request, SubscriptionPlan $plan, User $user);
    abstract public function handleResponse(Request $request);
    abstract public function handleWebhook(Request $request);

    public function createUserSubscription(SubscriptionPlan $plan, User $user, string $subscription_id)
    {
        $provider = $this->getProviderName();
        $planArray = $plan->toArray();
        return UserSubscription::create([
            'user_id' => $user->id,
            'plan_id' => $plan->id,
            'pg_plan_id' => $planArray["plan_id"],
            'provider' => $provider,
            'status' => UserSubscription::SUCCESS,
            'subscription_id' => $subscription_id
        ]);
    }
}