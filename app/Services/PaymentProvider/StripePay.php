<?php

namespace App\Services\PaymentProvider;

use App\Models\SubscriptionPlan;
use App\Models\User;
use App\Models\UserSubscription;
use App\Models\Order;
use App\Models\Address;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Stripe\Stripe;

class StripePay extends Base
{

    public function __construct()
    {
        \Stripe\Stripe::setApiKey( config('stripe.api_keys.secret_key'));

    }

    public function getProviderName()
    {
        return 'stripe';
    }


    public function getCards(User $user)
    {
        $user = Auth::user();
        \Stripe\Stripe::setApiKey(config('services.stripe.secret_key'));
        $cards = [];

        if ($user->customer_id) {
            $paymentMethod = \Stripe\PaymentMethod::all([
                'customer' => $user->customer_id,
                'type' => 'card',
            ]);
            $cards = $paymentMethod['data'];

        }
        return $cards;
    }

    public function createPayment($input, Address $address, User $user)
    {
        $stripe = new \Stripe\StripeClient(
            config('stripe.api_keys.secret_key')
        );

        if ($input->use_saved_card) {
            $stripe = \Stripe\PaymentIntent::create([
                'amount' => round($input->total_amount * 100, 2),
                'currency' => config('services.stripe.currency'),
                'customer' => $user->customer_id,
                'payment_method' => $input->payment_method_id,
                'off_session' => true,
                'confirm' => true,
            ]);
        } else {

            $payment_method_id = null;
            $payment_method_id = $input->save_card ? $input->payment_method_id : null;

            if (!$user->customer_id) {

                //creating new customer
                $customer = \Stripe\Customer::create([
                    "email" => $user->email,
                    "name" =>  $user->name,
                    "source" => $input->payment_id,
                    'shipping' => [
                        'name' => $user->name,
                        'address' => [
                            'line1' => $address->add_1 ?? '',
                            'line2' => $address->add_2 ?? '',
                            'postal_code' => $address->zip,
                            'city' => $address->city,
                            'state' => $address->state,
                        ],
                    ],
                ]);

                if ($payment_method_id) {
                    $payment_method = \Stripe\PaymentMethod::retrieve($payment_method_id);
                    $payment_method->attach(['customer' => $customer->id]);
                }

                $user->customer_id = $customer->id;
                $user->payment_method_id = $payment_method_id;
                $user->update();
            }

            $stripe = \Stripe\Charge::create([
                "amount" => round($input->total_amount * 100, 2),
                "currency" => config('services.stripe.currency'),
                "customer" => $user->customer_id,
                "description" => 'Order amount of ' . config('services.stripe.currency') . ' ' . $input->total_amount,
            ]);
        }

        return $stripe;

    }

    public function initiateSubscription($request, SubscriptionPlan $plan, User $user)
    {
        $stripe = new \Stripe\StripeClient(
            config('stripe.api_keys.secret_key')
        );

        $token =  $request->stripeToken;

        if (!$user->customer_id) {
            //creating new customer
            $customer = \Stripe\Customer::create([
                "email" => $user->email,
                "name" =>  $user->name,
            ]);

            $user->customer_id = $customer->id;
            $user->update();
        }

        \Stripe\Customer::createSource(
            $user->customer_id,
            ['source' => $token]
        );

        $subscription = $stripe->subscriptions->create([
            'customer' => $user->customer_id,
            'items' => [
              ['price' => $plan->plan_id],
            ],
          ]);

        $this->createUserSubscription($plan, $user, $subscription['id']);

        return [
            "plan_id" => $plan->plan_id,
            "custom_id" => "".$subscription->id
        ];
    }

    public function handleResponse(Request $request)
    {
        $payload = @file_get_contents('php://input');
        $event = null;

        Stripe\Stripe::setApiKey(config('stripe.api_secret'));

        // dd('123');
        try {
            $event = \Stripe\Event::constructFrom(
                json_decode($payload, true)
            );
        } catch (\UnexpectedValueException $e) {
            // Invalid payload
            http_response_code(400);
            exit();
        }

        $response = null;
        // Handle the event
        switch ($event->type) {
            case 'payment_intent.succeeded':
                \DB::beginTransaction();

                $paymentIntent = $event->data->object; // contains a StripePaymentIntent
                //TODO: handlePaymentIntentSucceeded($paymentIntent);
                $data = $paymentIntent->metadata;
                $transaction = Order::where('order_id', $data->order_id)->where('transaction_status', null)->first();
                if ($transaction == null) {
                    return $response;
                }
                $transaction->transaction_id = $paymentIntent->id;
                // $transaction->response = json_encode($paymentIntent);
                $transaction->transaction_status = 'succeeded';
                $transaction->update();

                // $user = User::find($transaction->user_id);
                // $balance = $user->balance;


                // $user->balance = $balance + $transaction->amount;
                // $user->update();

                DB::commit();
                //   $this->sendMail($transaction);
                $response = $transaction;
                break;
            case 'payment_method.attached':
                $paymentMethod = $event->data->object; // contains a StripePaymentMethod
                //TODO: handlePaymentMethodAttached($paymentMethod);
                dd($paymentMethod);
                break;
            case 'payment_intent.payment_failed':
                $paymentIntent = $event->data->object; // contains a StripePaymentIntent
                //TODO: handlePaymentIntentSucceeded($paymentIntent);
                $data = $paymentIntent->metadata;
                $transaction = Order::where('order_id', $data->order_id)->where('transaction_status', null)->first();
                if ($transaction == null) {
                    return 'Order not found';
                }

                $transaction->transaction_id = $paymentIntent->id;
                // $transaction->response = json_encode($paymentIntent);
                $transaction->status = 'FAILED';
                $transaction->update();
                // ... TODO: handle other event types
                // no break
            default:
                // Unexpected event type
                http_response_code(400);
                exit();
        }
        return $response;
    }

    public function handleWebhook(Request $request)
    {

    }
}
